package cn.cellcom.agent.pojo;

/**
 * TAgentSession entity.
 * 
 * @author MyEclipse Persistence Tools
 */

public class TAgentSession implements java.io.Serializable {

	// Fields

	private String id;
	private String org;
	private String pid;
	private String channel;
	private String session;
	private String groupId;
	private String agent;
	private String agentNickname;
	private Long joinTime;
	private String joinType;
	private Long endTime;
	private String endType;
	private Long firstResponseTime;
	private Integer messageCount;

	// Constructors

	/** default constructor */
	public TAgentSession() {
	}

	/** minimal constructor */
	public TAgentSession(String id, String org, String channel, String session, String groupId, String agent, String agentNickname, Long joinTime,
			String joinType) {
		this.id = id;
		this.org = org;
		this.channel = channel;
		this.session = session;
		this.groupId = groupId;
		this.agent = agent;
		this.agentNickname = agentNickname;
		this.joinTime = joinTime;
		this.joinType = joinType;
	}

	/** full constructor */
	public TAgentSession(String id, String org, String pid, String channel, String session, String groupId, String agent, String agentNickname,
			Long joinTime, String joinType, Long endTime, String endType, Long firstResponseTime, Integer messageCount) {
		this.id = id;
		this.org = org;
		this.pid = pid;
		this.channel = channel;
		this.session = session;
		this.groupId = groupId;
		this.agent = agent;
		this.agentNickname = agentNickname;
		this.joinTime = joinTime;
		this.joinType = joinType;
		this.endTime = endTime;
		this.endType = endType;
		this.firstResponseTime = firstResponseTime;
		this.messageCount = messageCount;
	}

	// Property accessors

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getOrg() {
		return this.org;
	}

	public void setOrg(String org) {
		this.org = org;
	}

	public String getPid() {
		return this.pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public String getChannel() {
		return this.channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getSession() {
		return this.session;
	}

	public void setSession(String session) {
		this.session = session;
	}

	public String getGroupId() {
		return this.groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public String getAgent() {
		return this.agent;
	}

	public void setAgent(String agent) {
		this.agent = agent;
	}

	public String getAgentNickname() {
		return this.agentNickname;
	}

	public void setAgentNickname(String agentNickname) {
		this.agentNickname = agentNickname;
	}

	public Long getJoinTime() {
		return this.joinTime;
	}

	public void setJoinTime(Long joinTime) {
		this.joinTime = joinTime;
	}

	public String getJoinType() {
		return this.joinType;
	}

	public void setJoinType(String joinType) {
		this.joinType = joinType;
	}

	public Long getEndTime() {
		return this.endTime;
	}

	public void setEndTime(Long endTime) {
		this.endTime = endTime;
	}

	public String getEndType() {
		return this.endType;
	}

	public void setEndType(String endType) {
		this.endType = endType;
	}

	public Long getFirstResponseTime() {
		return this.firstResponseTime;
	}

	public void setFirstResponseTime(Long firstResponseTime) {
		this.firstResponseTime = firstResponseTime;
	}

	public Integer getMessageCount() {
		return this.messageCount;
	}

	public void setMessageCount(Integer messageCount) {
		this.messageCount = messageCount;
	}

}