package cn.cellcom.agent.pojo;

/**
 * TButton entity.
 * 
 * @author MyEclipse Persistence Tools
 */

public class TButton implements java.io.Serializable {

	// Fields

	private String id;
	private String org;
	private String pid;
	private Integer seq;
	private String img;
	private String href;
	private String name;
	private String title;
	private String memo;
	private Integer level;
	private String subject;

	// Constructors

	/** default constructor */
	public TButton() {
	}

	/** minimal constructor */
	public TButton(String id, String org, String pid) {
		this.id = id;
		this.org = org;
		this.pid = pid;
	}

	/** full constructor */
	public TButton(String id, String org, String pid, Integer seq, String img, String href, String name, String title, String memo, Integer level,
			String subject) {
		this.id = id;
		this.org = org;
		this.pid = pid;
		this.seq = seq;
		this.img = img;
		this.href = href;
		this.name = name;
		this.title = title;
		this.memo = memo;
		this.level = level;
		this.subject = subject;
	}

	// Property accessors

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getOrg() {
		return this.org;
	}

	public void setOrg(String org) {
		this.org = org;
	}

	public String getPid() {
		return this.pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public Integer getSeq() {
		return this.seq;
	}

	public void setSeq(Integer seq) {
		this.seq = seq;
	}

	public String getImg() {
		return this.img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public String getHref() {
		return this.href;
	}

	public void setHref(String href) {
		this.href = href;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getMemo() {
		return this.memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public Integer getLevel() {
		return this.level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public String getSubject() {
		return this.subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

}
