package cn.cellcom.agent.pojo;

/**
 * TSetting entity.
 * 
 * @author MyEclipse Persistence Tools
 */

public class TSetting implements java.io.Serializable {

	// Fields

	private String id;
	private String org;
	private String pid;
	private String subject;
	private String k;
	private String v;
	private Integer level;

	// Constructors

	/** default constructor */
	public TSetting() {
	}

	/** minimal constructor */
	public TSetting(String id, String org, String pid, String k, String v, Integer level) {
		this.id = id;
		this.org = org;
		this.pid = pid;
		this.k = k;
		this.v = v;
		this.level = level;
	}

	/** full constructor */
	public TSetting(String id, String org, String pid, String subject, String k, String v, Integer level) {
		this.id = id;
		this.org = org;
		this.pid = pid;
		this.subject = subject;
		this.k = k;
		this.v = v;
		this.level = level;
	}

	// Property accessors

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getOrg() {
		return this.org;
	}

	public void setOrg(String org) {
		this.org = org;
	}

	public String getPid() {
		return this.pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public String getSubject() {
		return this.subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getK() {
		return this.k;
	}

	public void setK(String k) {
		this.k = k;
	}

	public String getV() {
		return this.v;
	}

	public void setV(String v) {
		this.v = v;
	}

	public Integer getLevel() {
		return this.level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

}
