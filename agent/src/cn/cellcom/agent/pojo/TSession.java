package cn.cellcom.agent.pojo;

/**
 * TSession entity.
 * 
 * @author MyEclipse Persistence Tools
 */

public class TSession implements java.io.Serializable {

	// Fields

	private String id;
	private String org;
	private String pid;
	private String channel;
	private String crm;
	private Long requestTime;
	private Long robotTime;
	private Long queueTime;
	private Long agentTime;
	private Long endTime;
	private Long leftTime;
	private String agent;
	private String agentName;
	private String endType;
	private Long firstAgentResponseTime;
	private String requestGroup;
	private Integer messageCount;
	private Integer messageCountWhenAgent;
	private Integer agentMessageCount;
	private Integer systemMessageCount;
	private Short satisfyScore;
	private String satisfyText;
	private String step;
	private String tag1;
	private String tag2;
	private String tag3;
	private String tag4;
	private String ip;
	private String trace;
	private String province;
	private String city;
	private String extend1;
	private String extend2;
	private String extend3;
	private String extend4;
	private String extend5;
	private String extend6;
	private String extend7;
	private String extend8;
	private String extend9;
	private String extend10;
	private String type;

	// Constructors

	/** default constructor */
	public TSession() {
	}

	/** minimal constructor */
	public TSession(String id, String org, String channel, String crm, Long requestTime) {
		this.id = id;
		this.org = org;
		this.channel = channel;
		this.crm = crm;
		this.requestTime = requestTime;
	}

	/** full constructor */
	public TSession(String id, String org, String pid, String channel, String crm, Long requestTime, Long robotTime, Long queueTime, Long agentTime,
			Long endTime, Long leftTime, String agent, String agentName, String endType, Long firstAgentResponseTime, String requestGroup,
			Integer messageCount, Integer messageCountWhenAgent, Integer agentMessageCount, Integer systemMessageCount, Short satisfyScore,
			String satisfyText, String step, String tag1, String tag2, String tag3, String tag4, String ip, String trace, String province,
			String city, String extend1, String extend2, String extend3, String extend4, String extend5, String extend6, String extend7,
			String extend8, String extend9, String extend10, String type) {
		this.id = id;
		this.org = org;
		this.pid = pid;
		this.channel = channel;
		this.crm = crm;
		this.requestTime = requestTime;
		this.robotTime = robotTime;
		this.queueTime = queueTime;
		this.agentTime = agentTime;
		this.endTime = endTime;
		this.leftTime = leftTime;
		this.agent = agent;
		this.agentName = agentName;
		this.endType = endType;
		this.firstAgentResponseTime = firstAgentResponseTime;
		this.requestGroup = requestGroup;
		this.messageCount = messageCount;
		this.messageCountWhenAgent = messageCountWhenAgent;
		this.agentMessageCount = agentMessageCount;
		this.systemMessageCount = systemMessageCount;
		this.satisfyScore = satisfyScore;
		this.satisfyText = satisfyText;
		this.step = step;
		this.tag1 = tag1;
		this.tag2 = tag2;
		this.tag3 = tag3;
		this.tag4 = tag4;
		this.ip = ip;
		this.trace = trace;
		this.province = province;
		this.city = city;
		this.extend1 = extend1;
		this.extend2 = extend2;
		this.extend3 = extend3;
		this.extend4 = extend4;
		this.extend5 = extend5;
		this.extend6 = extend6;
		this.extend7 = extend7;
		this.extend8 = extend8;
		this.extend9 = extend9;
		this.extend10 = extend10;
		this.type = type;
	}

	// Property accessors

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getOrg() {
		return this.org;
	}

	public void setOrg(String org) {
		this.org = org;
	}

	public String getPid() {
		return this.pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public String getChannel() {
		return this.channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getCrm() {
		return this.crm;
	}

	public void setCrm(String crm) {
		this.crm = crm;
	}

	public Long getRequestTime() {
		return this.requestTime;
	}

	public void setRequestTime(Long requestTime) {
		this.requestTime = requestTime;
	}

	public Long getRobotTime() {
		return this.robotTime;
	}

	public void setRobotTime(Long robotTime) {
		this.robotTime = robotTime;
	}

	public Long getQueueTime() {
		return this.queueTime;
	}

	public void setQueueTime(Long queueTime) {
		this.queueTime = queueTime;
	}

	public Long getAgentTime() {
		return this.agentTime;
	}

	public void setAgentTime(Long agentTime) {
		this.agentTime = agentTime;
	}

	public Long getEndTime() {
		return this.endTime;
	}

	public void setEndTime(Long endTime) {
		this.endTime = endTime;
	}

	public Long getLeftTime() {
		return this.leftTime;
	}

	public void setLeftTime(Long leftTime) {
		this.leftTime = leftTime;
	}

	public String getAgent() {
		return this.agent;
	}

	public void setAgent(String agent) {
		this.agent = agent;
	}

	public String getAgentName() {
		return this.agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public String getEndType() {
		return this.endType;
	}

	public void setEndType(String endType) {
		this.endType = endType;
	}

	public Long getFirstAgentResponseTime() {
		return this.firstAgentResponseTime;
	}

	public void setFirstAgentResponseTime(Long firstAgentResponseTime) {
		this.firstAgentResponseTime = firstAgentResponseTime;
	}

	public String getRequestGroup() {
		return this.requestGroup;
	}

	public void setRequestGroup(String requestGroup) {
		this.requestGroup = requestGroup;
	}

	public Integer getMessageCount() {
		return this.messageCount;
	}

	public void setMessageCount(Integer messageCount) {
		this.messageCount = messageCount;
	}

	public Integer getMessageCountWhenAgent() {
		return this.messageCountWhenAgent;
	}

	public void setMessageCountWhenAgent(Integer messageCountWhenAgent) {
		this.messageCountWhenAgent = messageCountWhenAgent;
	}

	public Integer getAgentMessageCount() {
		return this.agentMessageCount;
	}

	public void setAgentMessageCount(Integer agentMessageCount) {
		this.agentMessageCount = agentMessageCount;
	}

	public Integer getSystemMessageCount() {
		return this.systemMessageCount;
	}

	public void setSystemMessageCount(Integer systemMessageCount) {
		this.systemMessageCount = systemMessageCount;
	}

	public Short getSatisfyScore() {
		return this.satisfyScore;
	}

	public void setSatisfyScore(Short satisfyScore) {
		this.satisfyScore = satisfyScore;
	}

	public String getSatisfyText() {
		return this.satisfyText;
	}

	public void setSatisfyText(String satisfyText) {
		this.satisfyText = satisfyText;
	}

	public String getStep() {
		return this.step;
	}

	public void setStep(String step) {
		this.step = step;
	}

	public String getTag1() {
		return this.tag1;
	}

	public void setTag1(String tag1) {
		this.tag1 = tag1;
	}

	public String getTag2() {
		return this.tag2;
	}

	public void setTag2(String tag2) {
		this.tag2 = tag2;
	}

	public String getTag3() {
		return this.tag3;
	}

	public void setTag3(String tag3) {
		this.tag3 = tag3;
	}

	public String getTag4() {
		return this.tag4;
	}

	public void setTag4(String tag4) {
		this.tag4 = tag4;
	}

	public String getIp() {
		return this.ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getTrace() {
		return this.trace;
	}

	public void setTrace(String trace) {
		this.trace = trace;
	}

	public String getProvince() {
		return this.province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getExtend1() {
		return this.extend1;
	}

	public void setExtend1(String extend1) {
		this.extend1 = extend1;
	}

	public String getExtend2() {
		return this.extend2;
	}

	public void setExtend2(String extend2) {
		this.extend2 = extend2;
	}

	public String getExtend3() {
		return this.extend3;
	}

	public void setExtend3(String extend3) {
		this.extend3 = extend3;
	}

	public String getExtend4() {
		return this.extend4;
	}

	public void setExtend4(String extend4) {
		this.extend4 = extend4;
	}

	public String getExtend5() {
		return this.extend5;
	}

	public void setExtend5(String extend5) {
		this.extend5 = extend5;
	}

	public String getExtend6() {
		return this.extend6;
	}

	public void setExtend6(String extend6) {
		this.extend6 = extend6;
	}

	public String getExtend7() {
		return this.extend7;
	}

	public void setExtend7(String extend7) {
		this.extend7 = extend7;
	}

	public String getExtend8() {
		return this.extend8;
	}

	public void setExtend8(String extend8) {
		this.extend8 = extend8;
	}

	public String getExtend9() {
		return this.extend9;
	}

	public void setExtend9(String extend9) {
		this.extend9 = extend9;
	}

	public String getExtend10() {
		return this.extend10;
	}

	public void setExtend10(String extend10) {
		this.extend10 = extend10;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

}