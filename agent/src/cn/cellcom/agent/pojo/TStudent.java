package cn.cellcom.agent.pojo;

/**
 * TStudent entity.
 * 
 * @author MyEclipse Persistence Tools
 */

public class TStudent implements java.io.Serializable {

	// Fields

	private String id;
	private String name;
	private Integer age;
	private String birthday;
	private String level;
	private String sex;
	private String t;

	// Constructors

	/** default constructor */
	public TStudent() {
	}

	/** minimal constructor */
	public TStudent(String id, String name) {
		this.id = id;
		this.name = name;
	}

	/** full constructor */
	public TStudent(String id, String name, Integer age, String birthday, String level, String sex, String t) {
		this.id = id;
		this.name = name;
		this.age = age;
		this.birthday = birthday;
		this.level = level;
		this.sex = sex;
		this.t = t;
	}

	// Property accessors

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getAge() {
		return this.age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public String getBirthday() {
		return this.birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getLevel() {
		return this.level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public String getSex() {
		return this.sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getT() {
		return this.t;
	}

	public void setT(String t) {
		this.t = t;
	}

}