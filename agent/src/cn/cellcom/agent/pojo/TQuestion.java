package cn.cellcom.agent.pojo;

import cn.cellcom.jar.mongo.IMongoPojo;

/**
 * TQuestion entity.
 * 
 * @author MyEclipse Persistence Tools
 */

public class TQuestion implements java.io.Serializable, IMongoPojo {

	// Fields

	private String id;
	private String org;
	private String pid;
	private String no;
	private String question;
	private String answer;
	private String insertTime;
	private Integer times;
	private String type;
	private Integer level;
	private String subject;

	// Constructors

	/** default constructor */
	public TQuestion() {
	}

	/** minimal constructor */
	public TQuestion(String id, String org, String pid, String no, String question, String answer, String insertTime) {
		this.id = id;
		this.org = org;
		this.pid = pid;
		this.no = no;
		this.question = question;
		this.answer = answer;
		this.insertTime = insertTime;
	}

	/** full constructor */
	public TQuestion(String id, String org, String pid, String no, String question, String answer, String insertTime, Integer times, String type,
			Integer level, String subject) {
		this.id = id;
		this.org = org;
		this.pid = pid;
		this.no = no;
		this.question = question;
		this.answer = answer;
		this.insertTime = insertTime;
		this.times = times;
		this.type = type;
		this.level = level;
		this.subject = subject;
	}

	// Property accessors

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getOrg() {
		return this.org;
	}

	public void setOrg(String org) {
		this.org = org;
	}

	public String getPid() {
		return this.pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public String getNo() {
		return this.no;
	}

	public void setNo(String no) {
		this.no = no;
	}

	public String getQuestion() {
		return this.question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getAnswer() {
		return this.answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public String getInsertTime() {
		return this.insertTime;
	}

	public void setInsertTime(String insertTime) {
		this.insertTime = insertTime;
	}

	public Integer getTimes() {
		return this.times;
	}

	public void setTimes(Integer times) {
		this.times = times;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Integer getLevel() {
		return this.level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public String getSubject() {
		return this.subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	@Override
	public String get_IdName() {
		return "id";
	}

}
