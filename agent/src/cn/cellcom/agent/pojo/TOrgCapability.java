package cn.cellcom.agent.pojo;

/**
 * TOrgCapability entity.
 * 
 * @author MyEclipse Persistence Tools
 */

public class TOrgCapability implements java.io.Serializable {

	// Fields

	private String id;
	private Integer startTime;
	private Integer endTime;
	private String type;
	private Integer capability;

	// Constructors

	/** default constructor */
	public TOrgCapability() {
	}

	/** full constructor */
	public TOrgCapability(String id, Integer startTime, Integer endTime, String type, Integer capability) {
		this.id = id;
		this.startTime = startTime;
		this.endTime = endTime;
		this.type = type;
		this.capability = capability;
	}

	// Property accessors

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Integer getStartTime() {
		return this.startTime;
	}

	public void setStartTime(Integer startTime) {
		this.startTime = startTime;
	}

	public Integer getEndTime() {
		return this.endTime;
	}

	public void setEndTime(Integer endTime) {
		this.endTime = endTime;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Integer getCapability() {
		return this.capability;
	}

	public void setCapability(Integer capability) {
		this.capability = capability;
	}

}
