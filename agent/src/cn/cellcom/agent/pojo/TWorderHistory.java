package cn.cellcom.agent.pojo;

/**
 * TWorderHistory entity.
 * 
 * @author MyEclipse Persistence Tools
 */

public class TWorderHistory implements java.io.Serializable {

	// Fields

	private String id;
	private String orderId;
	private String crm;
	private String operUser;
	private String operType;
	private String operContent;
	private String operTime;

	// Constructors

	/** default constructor */
	public TWorderHistory() {
	}

	/** minimal constructor */
	public TWorderHistory(String id, String orderId, String crm, String operType, String operContent, String operTime) {
		this.id = id;
		this.orderId = orderId;
		this.crm = crm;
		this.operType = operType;
		this.operContent = operContent;
		this.operTime = operTime;
	}

	/** full constructor */
	public TWorderHistory(String id, String orderId, String crm, String operUser, String operType, String operContent, String operTime) {
		this.id = id;
		this.orderId = orderId;
		this.crm = crm;
		this.operUser = operUser;
		this.operType = operType;
		this.operContent = operContent;
		this.operTime = operTime;
	}

	// Property accessors

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getOrderId() {
		return this.orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getCrm() {
		return this.crm;
	}

	public void setCrm(String crm) {
		this.crm = crm;
	}

	public String getOperUser() {
		return this.operUser;
	}

	public void setOperUser(String operUser) {
		this.operUser = operUser;
	}

	public String getOperType() {
		return this.operType;
	}

	public void setOperType(String operType) {
		this.operType = operType;
	}

	public String getOperContent() {
		return this.operContent;
	}

	public void setOperContent(String operContent) {
		this.operContent = operContent;
	}

	public String getOperTime() {
		return this.operTime;
	}

	public void setOperTime(String operTime) {
		this.operTime = operTime;
	}

}