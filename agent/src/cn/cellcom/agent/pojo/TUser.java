package cn.cellcom.agent.pojo;

import java.util.Date;

import cn.cellcom.agent.common.AgentEnv;
import cn.cellcom.agent.online.message.MessageConstant;
import cn.cellcom.jar.file.FU;
import cn.cellcom.jar.util.DT;

/**
 * TUser entity.
 * 
 * @author MyEclipse Persistence Tools
 */

public class TUser implements java.io.Serializable {

	// Fields

	private String username;
	private String org;
	private String pid;
	private String type;
	private String password;
	private Integer level;
	private String no;
	private String nickName;
	private String name;
	private String phone;
	private String email;
	private Date insertTime;
	private String status;
	private String tmp;

	// Constructors

	/** default constructor */
	public TUser() {
	}

	/** minimal constructor */
	public TUser(String username, String org, String pid, String type, String password, Integer level, String name, Date insertTime, String status) {
		this.username = username;
		this.org = org;
		this.pid = pid;
		this.type = type;
		this.password = password;
		this.level = level;
		this.name = name;
		this.insertTime = insertTime;
		this.status = status;
	}

	/** full constructor */
	public TUser(String username, String org, String pid, String type, String password, Integer level, String no, String nickName, String name,
			String phone, String email, Date insertTime, String status, String tmp) {
		this.username = username;
		this.org = org;
		this.pid = pid;
		this.type = type;
		this.password = password;
		this.level = level;
		this.no = no;
		this.nickName = nickName;
		this.name = name;
		this.phone = phone;
		this.email = email;
		this.insertTime = insertTime;
		this.status = status;
		this.tmp = tmp;
	}

	// Property accessors

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getOrg() {
		return this.org;
	}

	public void setOrg(String org) {
		this.org = org;
	}

	public String getPid() {
		return this.pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Integer getLevel() {
		return this.level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public String getNo() {
		return this.no;
	}

	public void setNo(String no) {
		this.no = no;
	}

	public String getNickName() {
		return this.nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getInsertTime() {
		return this.insertTime;
	}

	public void setInsertTime(Date insertTime) {
		this.insertTime = insertTime;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTmp() {
		return this.tmp;
	}

	public void setTmp(String tmp) {
		this.tmp = tmp;
	}
	public String[] getSettingFile(String file) {
		String fn = this.getPid() + "-" + this.getUsername() + "-" + FU.getSerialFileName(file);
		return new String[]{fn, AgentEnv.SETTING_FILE_PATH + fn};
	}

	public String[] getChatFile(String file, String multiType) {
		if(file.equals("blob")) {//截图
			if(MessageConstant.MULIT_TYPE.AUDIO.name().equals(multiType)) {
				file = "录音.amr";
			} else if(MessageConstant.MULIT_TYPE.IMG.name().equals(multiType)) {
				file = "截图.png";
			} else {
				file = "error.erro";
			}
		}
		String fn = this.getUsername() + "-" + FU.getSerialFileName(file);
		return new String[]{fn, AgentEnv.CHAT_FILE_PATH + this.getPid() + "/" + DT.getYYYYMMDD() + "/" + fn};
	}

	public String[] getSelfQuestionFile(String file) {
		String fn = this.getUsername() + "-" + FU.getSerialFileName(file);
		return new String[]{fn, AgentEnv.QUESTION_FILE_PATH + this.getPid() + "/" + fn};
	}
}
