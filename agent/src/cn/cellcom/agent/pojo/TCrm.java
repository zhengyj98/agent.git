package cn.cellcom.agent.pojo;

import cn.cellcom.agent.common.AgentConstant;
import cn.cellcom.agent.common.AgentEnv;
import cn.cellcom.agent.online.message.MessageConstant;
import cn.cellcom.jar.file.FU;
import cn.cellcom.jar.util.DT;

/**
 * TCrm entity.
 * 
 * @author MyEclipse Persistence Tools
 */

public class TCrm implements java.io.Serializable {

	// Fields

	private String id;
	private String org;
	private String name;
	private String phone;
	private String head;
	private String email;
	private Integer level;
	private Integer status;
	private String qq;
	private Integer sex;
	private String company;
	private String province;
	private String city;
	private String memo;
	private String tid;
	private Integer type;
	private Long createTime;
	private String extend1;
	private String extend2;
	private String extend3;
	private String extend4;
	private String extend5;
	private String extend6;
	private String extend7;
	private String extend8;
	private String extend9;
	private String extend10;

	// Constructors

	/** default constructor */
	public TCrm() {
	}

	/** minimal constructor */
	public TCrm(String id, String org, String name, Long createTime) {
		this.id = id;
		this.org = org;
		this.name = name;
		this.createTime = createTime;
	}

	/** full constructor */
	public TCrm(String id, String org, String name, String phone, String email, Integer level, Integer status, String qq, Integer sex, String company,
			String province, String city, String memo, String tid, Integer type, Long createTime, String extend1, String extend2, String extend3,
			String extend4, String extend5, String extend6, String extend7, String extend8, String extend9, String extend10) {
		this.id = id;
		this.org = org;
		this.name = name;
		this.phone = phone;
		this.email = email;
		this.level = level;
		this.status = status;
		this.qq = qq;
		this.sex = sex;
		this.company = company;
		this.province = province;
		this.city = city;
		this.memo = memo;
		this.tid = tid;
		this.type = type;
		this.createTime = createTime;
		this.extend1 = extend1;
		this.extend2 = extend2;
		this.extend3 = extend3;
		this.extend4 = extend4;
		this.extend5 = extend5;
		this.extend6 = extend6;
		this.extend7 = extend7;
		this.extend8 = extend8;
		this.extend9 = extend9;
		this.extend10 = extend10;
	}

	// Property accessors

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getOrg() {
		return this.org;
	}

	public void setOrg(String org) {
		this.org = org;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	public String getHead() {
		return head;
	}

	public void setHead(String head) {
		this.head = head;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getLevel() {
		return this.level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getQq() {
		return this.qq;
	}

	public void setQq(String qq) {
		this.qq = qq;
	}

	public Integer getSex() {
		return this.sex;
	}

	public void setSex(Integer sex) {
		this.sex = sex;
	}

	public String getCompany() {
		return this.company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getProvince() {
		return this.province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getMemo() {
		return this.memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public String getTid() {
		return this.tid;
	}

	public void setTid(String tid) {
		this.tid = tid;
	}

	public Integer getType() {
		return this.type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Long getCreateTime() {
		return this.createTime;
	}

	public void setCreateTime(Long createTime) {
		this.createTime = createTime;
	}

	public String getExtend1() {
		return this.extend1;
	}

	public void setExtend1(String extend1) {
		this.extend1 = extend1;
	}

	public String getExtend2() {
		return this.extend2;
	}

	public void setExtend2(String extend2) {
		this.extend2 = extend2;
	}

	public String getExtend3() {
		return this.extend3;
	}

	public void setExtend3(String extend3) {
		this.extend3 = extend3;
	}

	public String getExtend4() {
		return this.extend4;
	}

	public void setExtend4(String extend4) {
		this.extend4 = extend4;
	}

	public String getExtend5() {
		return this.extend5;
	}

	public void setExtend5(String extend5) {
		this.extend5 = extend5;
	}

	public String getExtend6() {
		return this.extend6;
	}

	public void setExtend6(String extend6) {
		this.extend6 = extend6;
	}

	public String getExtend7() {
		return this.extend7;
	}

	public void setExtend7(String extend7) {
		this.extend7 = extend7;
	}

	public String getExtend8() {
		return this.extend8;
	}

	public void setExtend8(String extend8) {
		this.extend8 = extend8;
	}

	public String getExtend9() {
		return this.extend9;
	}

	public void setExtend9(String extend9) {
		this.extend9 = extend9;
	}

	public String getExtend10() {
		return this.extend10;
	}

	public void setExtend10(String extend10) {
		this.extend10 = extend10;
	}

	public String[] getChatFile(String file, String multiType) {
		if(file.equals("blob")) {//截图
			if(MessageConstant.MULIT_TYPE.AUDIO.name().equals(multiType)) {
				file = "录音.amr";
			} else if(MessageConstant.MULIT_TYPE.IMG.name().equals(multiType)) {
				file = "截图.png";
			} else {
				file = "error.erro";
			}
		}
		String fn = this.getId() + "-" + FU.getSerialFileName(file);
		return new String[] { fn, AgentEnv.CHAT_FILE_PATH + this.getOrg() + "/" + DT.getYYYYMMDD() + "/" + fn };
	}

	@Override
	public String toString() {
		return this.id;
	}
}
