package cn.cellcom.agent.token;

/**
 * 
 * Description
 * 
 * 
 */
public class TokenEntity implements java.io.Serializable {

	private static final long serialVersionUID = 6471042724663229147L;

	public final static int NO_USER = 300;
	
	public final static int NO_AUTH = 400;

	public final static int ERROR_AUTH = 401;

	public static final int ERROR_USER = 402;

	public static final int AUTH_OK = 200;

	private int code;

	private Long timestamp;

	private Long validateTime;

	private String token;

	public Long getValidateTime() {
		return validateTime;
	}

	public void setValidateTime(Long validateTime) {
		this.validateTime = validateTime;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public Long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Long timestamp) {
		this.timestamp = timestamp;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

}
