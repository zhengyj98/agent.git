package cn.cellcom.agent.token;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import net.sf.json.JSONObject;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import cn.cellcom.agent.util.SSLClient;
import cn.cellcom.jar.util.DT;
import cn.cellcom.jar.util.MD5;

public class TokenTester {

	// static final String uri = "http://172.16.54.74:11001/rest/im/token";
	static final String uri = "http://www.jianyuekefu.com:8080/api.do?method=getToken";

	static final String account = "13631484267";

	static final String password = "suntek01";

	public static void main(String[] args) {
		String[] token = TokenTester.getToken(uri, account, password);
		System.out.println("Token: " + token[0] + " validate :" + token[1]);
		System.out.println(DT.getYYYYMMDDHHMMSS(new Date(Long.parseLong(token[1]))));
		RestTester.main(token);
	}

	public static String[] getToken(String uri, String username, String password) {
		String[] token = null;
		try {
			Map<String, String> headers = new HashMap<String, String>();
			headers.put("username", username);
			HttpResponse result = sendRequest(uri, headers);
			int statusCode = result.getStatusLine().getStatusCode();

			if (statusCode == HttpStatus.SC_OK) {
				String string = EntityUtils.toString(result.getEntity(), "UTF-8");
				TokenEntity tokenEntity = (TokenEntity) JSONObject.toBean(JSONObject.fromObject(string), TokenEntity.class);
				if (tokenEntity.getCode() == TokenEntity.NO_AUTH) {
					headers.put("authorization", MD5.toMd5(username + password + tokenEntity.getTimestamp()));
					HttpResponse result2 = sendRequest(uri, headers);
					String string2 = EntityUtils.toString(result2.getEntity(), "UTF-8");
					TokenEntity tokenEntity2 = (TokenEntity) JSONObject.toBean(JSONObject.fromObject(string2), TokenEntity.class);
					if (tokenEntity2.getCode() == TokenEntity.ERROR_AUTH) {
						System.out.println("error auth");
					} else if (tokenEntity2.getCode() == TokenEntity.ERROR_USER) {
						System.out.println("error username");
					} else if (tokenEntity2.getCode() == TokenEntity.AUTH_OK) {
						System.out.println("auth ok");
						token = new String[] { tokenEntity2.getToken(), String.valueOf(tokenEntity2.getValidateTime()) };
					}
				} else {

				}
			} else {
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return token;
	}

	/**
	 * 
	 * @param uri
	 * @param method
	 * @param headers
	 * @return
	 */
	public static HttpResponse sendRequest(String callUrl, Map<String, String> headers) {
		HttpClient client = null;
		String res = null;
		try {
			if (callUrl.startsWith("https://")) {
				client = new SSLClient();
			} else {
				client = new DefaultHttpClient();
			}
			HttpGet request = new HttpGet(callUrl);
			if (headers != null) {
				for (String key : headers.keySet()) {
					request.addHeader(key, headers.get(key));
				}
			}
			request.addHeader("Content-Type", "application/json");
			HttpResponse response = client.execute(request);
			return response;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (client != null && client.getConnectionManager() != null) {
				client.getConnectionManager().shutdown();
			}
		}
		return null;
	}
}
