package cn.cellcom.agent.util;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ThreadPool {

	private static final ExecutorService threadPool = Executors.newFixedThreadPool(10);

	public static void Execute(Runnable task) {
		// Execute
		threadPool.execute(task);
	}
}
