package cn.cellcom.agent.util;

import java.io.Serializable;

public class Belong implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1982180048351593687L;

	public Belong() {
	}

	public Belong(String province, String city) {
		this.province = province;
		this.city = city;
	}

	private String sp;

	private String province;

	private String city;

	public String getSp() {
		return sp;
	}

	public void setSp(String sp) {
		this.sp = sp;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	@Override
	public String toString() {
		return "Belong [sp=" + sp + ", province=" + province + ", city=" + city + "]";
	}
	

	public String toString2() {
		return province + city;
	}

}
