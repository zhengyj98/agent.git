package cn.cellcom.agent.struts.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

import cn.cellcom.agent.common.AgentEnv;
import cn.cellcom.agent.hql.TAgentSessionHql;
import cn.cellcom.agent.pojo.TAgentSession;
import cn.cellcom.agent.struts.form.TAgentSessionForm;
import cn.cellcom.jar.biz.AbstractBiz;
import cn.cellcom.jar.dao.IBaseDao;
import cn.cellcom.jar.env.Env;
import cn.cellcom.jar.file.FileLoad;
import cn.cellcom.jar.file.FileProcessor;
import cn.cellcom.jar.file.IFileLoad;
import cn.cellcom.jar.file.IFileProcessor;
import cn.cellcom.jar.pagination.AbstractPaginationBiz;
import cn.cellcom.jar.pagination.IPaginationBiz;
import cn.cellcom.jar.port.ExportTemplate;
import cn.cellcom.jar.port.IDataPort;
import cn.cellcom.jar.port.JxlPort;
import cn.cellcom.jar.util.BeanUtil;
import cn.cellcom.jar.util.CU;
import cn.cellcom.jar.util.LogUtil;
import cn.cellcom.jar.util.MyException;

public class TAgentSessionDpAct extends DispatchAction {

	private IPaginationBiz pbiz;

	private AbstractBiz cbiz;

	private Log log = LogFactory.getLog(this.getClass());

	/**
	 * 查询记录
	 * 
	 * @param mapping
	 * @param fm
	 * @param req
	 * @param rep
	 * @return
	 */
	public ActionForward list(ActionMapping mapping, ActionForm form, HttpServletRequest req, HttpServletResponse rep) {
		form = (ActionForm) AbstractPaginationBiz.getPaginationActionForm(req, form);
		List list = null;
		try {
			log.info("查询的语句：TAgentSessionHql()");
			list = pbiz.pagination(mapping, form, req, new TAgentSessionHql()).getDataResult();

			req.setAttribute(Env.DATA_NAME, list);
		} catch (MyException e) {
			return LogUtil.e(this.clazz, "操作出现错误", e, mapping, req);
		}
		req.setAttribute("joinTypelist", BeanUtil.startWith("TAgentSession-joinType"));
		req.setAttribute("endTypelist", BeanUtil.startWith("TAgentSession-endType"));

		return mapping.findForward("list");
	}

	/**
	 * 查询记录详细信息
	 * 
	 * @param mapping
	 * @param fm
	 * @param req
	 * @param rep
	 * @return
	 */
	public ActionForward detail(ActionMapping mapping, ActionForm form, HttpServletRequest req, HttpServletResponse rep) {

		TAgentSessionForm fm = (TAgentSessionForm) form;
		IBaseDao dao = cbiz.getDao();
		try {
			String hql = "from TAgentSession as ags, TGroup as g, TSession s, TCrm c where ags.groupId=g.id and ags.session=s.id and s.crm=c.id and ags.id=?";
			Object pojo = dao.myGet(hql, new String[] { fm.getId() });
			if (pojo == null) {
				req.setAttribute(Env.DATA_NAME, "查看的信息不存，有疑问请联系管理员");
				return mapping.findForward("error");
			}
			req.setAttribute(Env.DATA_NAME, pojo);
		} catch (MyException e) {
			return LogUtil.e(this.clazz, "操作失败，未能读取相关信息的详细资料", e, mapping, req);
		}
		req.setAttribute("joinTypelist", BeanUtil.startWith("TAgentSession-joinType"));
		req.setAttribute("endTypelist", BeanUtil.startWith("TAgentSession-endType"));
		req.setAttribute("typelist", BeanUtil.startWith("TCrm-type"));

		return mapping.findForward("detail");
	}

	/**
	 * 导出数据
	 * 
	 * @param mapping
	 * @param form
	 * @param req
	 * @param rep
	 * @return
	 */
	public ActionForward doExport(ActionMapping mapping, ActionForm form, HttpServletRequest req, HttpServletResponse rep) {

		try {
			TAgentSessionHql hql0 = new TAgentSessionHql();
			List list = this.cbiz.getDao().myList(hql0.getHql(req, form), hql0.getParaValue(req, form));
			if (list == null || list.size() < 1) {
				req.setAttribute(Env.DATA_NAME, "本次查询没有数据，所以不做导出");
				return mapping.findForward("success");
			}

			ExportTemplate template = new ExportTemplate() {
				private static final long serialVersionUID = 1L;

				@Override
				public void setRow(DataRow row, Object obj) {
					Object[] arr = (Object[]) obj;
					TAgentSession pojo = (TAgentSession) arr[0];
					row.add(CU.valueOf(pojo.getId()));
					row.add(CU.valueOf(pojo.getOrg()));
					row.add(CU.valueOf(pojo.getPid()));
					row.add(CU.valueOf(pojo.getChannel()));
					row.add(CU.valueOf(pojo.getSession()));
					row.add(CU.valueOf(pojo.getAgent()));
					row.add(CU.valueOf(pojo.getAgentNickname()));
					row.add(CU.valueOf(pojo.getJoinTime()));
					row.add(CU.valueOf(pojo.getJoinType()));
					row.add(CU.valueOf(pojo.getEndTime()));
					row.add(CU.valueOf(pojo.getEndType()));
					row.add(CU.valueOf(pojo.getFirstResponseTime()));
					row.add(CU.valueOf(pojo.getMessageCount()));

				}
			};
			String root3path3file = AgentEnv.EXPORT_PATH + CU.getSerial(".xls");
			template.setRoot3path3file(root3path3file);
			template.setContentTitle(new String[] { " id", " org", " pid", " 渠道", " 会话ID", " 坐席", " 坐席昵称", " 接入时间", " 接入方式", " 结束时间", " 结束方式",
					" 首次响应", " 消息数" });

			IDataPort port = new JxlPort();
			template.setDataList(list);
			port.export(template);

			IFileLoad fl = new FileLoad();
			fl.download(req, rep, this.servlet.getServletConfig(), root3path3file);
			IFileProcessor fp = new FileProcessor();
			fp.delete(root3path3file);
		} catch (MyException e) {
			return LogUtil.e(this.clazz, "操作出现错误", e, mapping, req);
		}

		LogUtil.e(this.clazz, "数据已经成功导出", null, mapping, req);
		return null;
	}

	public void setCbiz(AbstractBiz cbiz) {
		this.cbiz = cbiz;
	}

	public void setPbiz(IPaginationBiz pbiz) {
		this.pbiz = pbiz;
	}
}
