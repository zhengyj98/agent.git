package cn.cellcom.agent.struts.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

import cn.cellcom.agent.biz.TSettingBiz;
import cn.cellcom.agent.common.AgentConstant;
import cn.cellcom.agent.entity.TSettingEntity;
import cn.cellcom.agent.hql.TButtonHql;
import cn.cellcom.agent.pojo.TButton;
import cn.cellcom.agent.pojo.TUser;
import cn.cellcom.agent.struts.form.TButtonForm;
import cn.cellcom.jar.biz.AbstractBiz;
import cn.cellcom.jar.env.Env;
import cn.cellcom.jar.logon.LogonSession;
import cn.cellcom.jar.pagination.AbstractPaginationBiz;
import cn.cellcom.jar.pagination.IPaginationBiz;
import cn.cellcom.jar.util.LogUtil;
import cn.cellcom.jar.util.MyException;

public class TButtonDpAct extends DispatchAction {

	private IPaginationBiz pbiz;

	private AbstractBiz cbiz;

	private TSettingBiz sbiz;// setting

	private Log log = LogFactory.getLog(this.getClass());

	/**
	 * 查询记录
	 * 
	 * @param mapping
	 * @param fm
	 * @param req
	 * @param rep
	 * @return
	 */
	public ActionForward list(ActionMapping mapping, ActionForm form, HttpServletRequest req, HttpServletResponse rep) {
		TButtonForm fm = (TButtonForm) form;
		form = (ActionForm) AbstractPaginationBiz.getPaginationActionForm(req, form);
		try {
			log.info("查询的语句：TButtonHql()");
			List list = pbiz.pagination(mapping, form, req, new TButtonHql()).getDataResult();
			req.setAttribute(Env.DATA_NAME, list);

			LogonSession ls = new LogonSession(req);
			TUser user = (TUser) ls.getLogonObject();
			TSettingEntity setting = null;
			// 直接查询企业的配置
			if (StringUtils.isBlank(fm.getSubject()) || AgentConstant.DEFAULT_ORG.equals(fm.getSubject())) {
				setting = sbiz.getOrgSetting(user.getOrg(), user.getPid());
			} else if (fm.getLevel() == AgentConstant.SETTING_LEVEL.CHANNEL.ordinal()) {// 查询的是channel的配置
				setting = sbiz.getChannelSetting(user.getPid(), fm.getSubject(), true);
			} else if (fm.getLevel() == AgentConstant.SETTING_LEVEL.WORKGROUP.ordinal()) {
				setting = sbiz.getChannelSetting(user.getPid(), fm.getSubject(), true);
			} else if (fm.getLevel() == AgentConstant.SETTING_LEVEL.AGENT.ordinal()) {
				setting = sbiz.getAgentSetting(user.getPid(), fm.getSubject(), true);
			}
			req.setAttribute("setting", setting);
		} catch (MyException e) {
			return LogUtil.e(this.clazz, "操作出现错误", e, mapping, req);
		}
		// 存在读取数据库的配置

		return mapping.findForward("list");
	}

	/**
	 * 准备记录添加
	 * 
	 * @param mapping
	 * @param fm
	 * @param req
	 * @param rep
	 * @return
	 */
	public ActionForward add(ActionMapping mapping, ActionForm form, HttpServletRequest req, HttpServletResponse rep) {

		return mapping.findForward("add");
	}

	/**
	 * 执行记录添加
	 * 
	 * @param mapping
	 * @param fm
	 * @param req
	 * @param rep
	 * @return
	 */
	public ActionForward doAdd(ActionMapping mapping, ActionForm form, HttpServletRequest req, HttpServletResponse rep) {
		TButtonForm fm = (TButtonForm) form;
		Object obj = null;
		try {
			obj = cbiz.add(req, fm, null);
		} catch (MyException e) {
			LogUtil.e(this.clazz, "新增数据出现异常", e);
		}
		log.info("新增记录结果：" + obj);
		String message;
		if (obj == null || !(obj instanceof TButton)) {
			req.setAttribute(Env.DATA_NAME, "操作失败，" + obj);
			return mapping.findForward("error");
		} else {
			message = "记录添加成功";
		}

		add(mapping, fm, req, rep);
		// req.setAttribute("success", fm);
		req.setAttribute(Env.DATA_NAME, message);
		return mapping.findForward("add");
	}

	/**
	 * 准备修改记录
	 * 
	 * @param mapping
	 * @param fm
	 * @param req
	 * @param rep
	 * @return
	 */
	public ActionForward modify(ActionMapping mapping, ActionForm form, HttpServletRequest req, HttpServletResponse rep) {
		TButtonForm fm = (TButtonForm) form;
		try {
			java.lang.String key = fm.getId();
			TButton rf = (TButton) cbiz.getDao().myGet(TButton.class, key);
			if (rf == null) {
				req.setAttribute(Env.DATA_NAME, "读取数据错误");
				return mapping.findForward("error");
			}
			req.setAttribute(Env.DATA_NAME, rf);
		} catch (MyException e) {
			return LogUtil.e(this.clazz, "读取数据错误", e, mapping, req);
		}
		return mapping.findForward("modify");
	}

	/**
	 * 执行记录修改
	 * 
	 * @param mapping
	 * @param fm
	 * @param req
	 * @param rep
	 * @return
	 */
	public ActionForward doModify(ActionMapping mapping, ActionForm form, HttpServletRequest req, HttpServletResponse rep) {
		TButtonForm fm = (TButtonForm) form;
		Object obj = null;
		try {
			obj = cbiz.modify(req, fm, null);
		} catch (MyException e) {
			LogUtil.e(this.clazz, "修改数据出现异常", e);
		}
		log.info("修改记录结果：" + obj);
		if (obj == null || !(obj instanceof TButton)) {
			req.setAttribute(Env.DATA_NAME, "操作失败，" + obj);
			return mapping.findForward("error");
		}
		if (fm.isAjax()) {
			if (obj == null || !(obj instanceof TButton)) {
				Response.err(rep, "修改失败");
			} else {
				Response.succ(rep, "修改成功");
			}
			return null;
		}
		
		req.setAttribute(Env.DATA_NAME, "记录修改成功");
		return mapping.findForward("success");
	}

	/**
	 * 执行删除操作
	 * 
	 * @param mapping
	 * @param fm
	 * @param req
	 * @param rep
	 * @return
	 */
	public ActionForward doDelete(ActionMapping mapping, ActionForm form, HttpServletRequest req, HttpServletResponse rep) {
		TButtonForm fm = (TButtonForm) form;
		List res = null;
		try {
			res = cbiz.delete(req, fm, null);
		} catch (MyException e) {
			LogUtil.e(this.clazz, "删除数据出现异常", e);
		}
		if (res == null || !(res instanceof List)) {
			return LogUtil.e(this.clazz, "操作失败，执行删除记录失败", null, mapping, req);
		} else {
			req.setAttribute(Env.DATA_NAME, "操作完成，删除记录数量：" + res.size());
			return mapping.findForward("success");
		}
	}

	public void setCbiz(AbstractBiz cbiz) {
		this.cbiz = cbiz;
	}

	public void setPbiz(IPaginationBiz pbiz) {
		this.pbiz = pbiz;
	}

	public void setSbiz(TSettingBiz sbiz) {
		this.sbiz = sbiz;
	}

}
