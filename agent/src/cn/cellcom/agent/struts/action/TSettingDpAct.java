package cn.cellcom.agent.struts.action;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

import cn.cellcom.agent.biz.TSettingBiz;
import cn.cellcom.agent.common.AgentConstant;
import cn.cellcom.agent.entity.TSettingEntity;
import cn.cellcom.agent.pojo.TSetting;
import cn.cellcom.agent.pojo.TUser;
import cn.cellcom.agent.struts.form.TSettingForm;
import cn.cellcom.jar.env.Env;
import cn.cellcom.jar.logon.LogonSession;
import cn.cellcom.jar.pagination.IPaginationBiz;
import cn.cellcom.jar.util.AU;
import cn.cellcom.jar.util.BeanUtil;
import cn.cellcom.jar.util.LogUtil;
import cn.cellcom.jar.util.MyException;

public class TSettingDpAct extends DispatchAction {

	private IPaginationBiz pbiz;

	private TSettingBiz cbiz;

	private Log log = LogFactory.getLog(this.getClass());

	/**
	 * 查询记录
	 * 
	 * @param mapping
	 * @param fm
	 * @param req
	 * @param rep
	 * @return
	 */
	public ActionForward list(ActionMapping mapping, ActionForm form, HttpServletRequest req, HttpServletResponse rep) {
		TSettingForm fm = (TSettingForm) form;
		String tag = fm.getTag();
		try {
			LogonSession ls = new LogonSession(req);
			TUser user = (TUser) ls.getLogonObject();
			TSettingEntity setting = null;
			// 直接查询企业的配置
			if (StringUtils.isBlank(fm.getSubject())) {
				setting = cbiz.getOrgSetting(user.getOrg(), user.getPid());
			} else if (fm.getLevel() == AgentConstant.SETTING_LEVEL.CHANNEL.ordinal()) {// 查询的是channel的配置
				setting = cbiz.getChannelSetting(user.getPid(), fm.getSubject(), false);
			} else if (fm.getLevel() == AgentConstant.SETTING_LEVEL.WORKGROUP.ordinal()) {
				setting = cbiz.getChannelSetting(user.getPid(), fm.getSubject(), true);
			} else if (fm.getLevel() == AgentConstant.SETTING_LEVEL.AGENT.ordinal()) {
				setting = cbiz.getAgentSetting(user.getPid(), fm.getSubject(), true);
			}

			if (StringUtils.isNotBlank(setting.getServiceTime())) {
				ohNo(setting, req, 0);
			}
			req.setAttribute(Env.DATA_NAME, setting);
		} catch (MyException e) {
			return LogUtil.e(this.clazz, "操作出现错误", e, mapping, req);
		}

		req.setAttribute("modelist", BeanUtil.startWith("TSetting-ondutySettingMode"));
		return mapping.findForward(tag);
	}

	/**
	 * 很特殊的处理
	 * 
	 * @param setting
	 * @param req
	 * @param type
	 * @return
	 */
	private String ohNo(TSettingEntity setting, HttpServletRequest req, int type) {
		if (type == 0) {
			String[] everyday = setting.getServiceTime().split(";");
			// 08:00-12:00,13:00-18:00
			String firstDay = everyday[0];
			// 08:00-12:00
			String[] zone = firstDay.split(",");
			// 08:00
			String[] timeA = zone[0].split("-");
			req.setAttribute("A1", timeA[0].split(":")[0]);
			req.setAttribute("A2", timeA[0].split(":")[1]);
			req.setAttribute("A3", timeA[1].split(":")[0]);
			req.setAttribute("A4", timeA[1].split(":")[1]);
			String[] timeB = zone[1].split("-");
			req.setAttribute("B1", timeB[0].split(":")[0]);
			req.setAttribute("B2", timeB[0].split(":")[1]);
			req.setAttribute("B3", timeB[1].split(":")[0]);
			req.setAttribute("B4", timeB[1].split(":")[1]);
			return null;
		} else if (type == 1) {
			StringBuffer sb = new StringBuffer();
			sb.append(req.getParameter("A1")).append(":").append(req.getParameter("A2"));
			sb.append("-").append(req.getParameter("A3")).append(":").append(req.getParameter("A4"));
			sb.append(",").append(req.getParameter("B1")).append(":").append(req.getParameter("B2"));
			sb.append("-").append(req.getParameter("B3")).append(":").append(req.getParameter("B4"));
			return sb.toString();
		} else {
			return "";
		}
	}

	/**
	 * 修改配置
	 * 
	 * @param mapping
	 * @param fm
	 * @param req
	 * @param rep
	 * @return
	 */
	public ActionForward doModifyOrgSetting(ActionMapping mapping, ActionForm form, HttpServletRequest req, HttpServletResponse rep) {
		TSettingForm fm = (TSettingForm) form;
		String tag = fm.getTag();
		boolean result = false;
		try {
			LogonSession ls = new LogonSession(req);
			TUser user = (TUser) ls.getLogonObject();
			/**
			 * 获取某个标签下的配置项
			 */
			List<String> itemList = null;
			if (StringUtils.isBlank(tag)) {
				itemList = new ArrayList<String>();
				itemList.add(req.getParameter("k"));
			} else {
				itemList = TSettingEntity.getItemList(tag);
			}
			List<TSetting> settingList = new ArrayList<TSetting>();
			for (int i = 0; i < itemList.size(); i++) {
				String key = itemList.get(i);
				String value = null;
				if (key.equals(TSettingEntity.SERVICE_TIME)) {
					value = ohNo(null, req, 1);
				} else {
					// 多选框时候的兼容
					value = AU.toString2(req.getParameterValues(key));
				}
				TSetting one = new TSetting();
				one.setOrg(user.getOrg());
				one.setPid(user.getPid());
				one.setK(key);
				one.setV(value);
				one.setSubject(fm.getSubject());
				one.setLevel(fm.getLevel());
				settingList.add(one);
			}

			cbiz.modifyOrgSetting(user.getPid(), settingList);
			req.setAttribute(Env.DATA_NAME, "修改配置成功");
			result = true;
		} catch (MyException e) {
			return LogUtil.e(this.clazz, "操作出现错误", e, mapping, req);
		}

		if (StringUtils.isBlank(tag)) {
			if (result) {
				Response.succ(rep, "修改成功");
			} else {
				Response.err(rep, "修改失败");
			}
			return null;
		}
		return mapping.findForward("success");
	}

	public void setCbiz(TSettingBiz cbiz) {
		this.cbiz = cbiz;
	}

	public void setPbiz(IPaginationBiz pbiz) {
		this.pbiz = pbiz;
	}
}
