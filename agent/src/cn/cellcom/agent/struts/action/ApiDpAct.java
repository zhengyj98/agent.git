package cn.cellcom.agent.struts.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.cellcom.agent.biz.TUserBiz;

/**
 * 访客端访问逻辑
 * 
 * @author 郑余杰
 */
public class ApiDpAct extends DispatchAction {

	private Logger log = LoggerFactory.getLogger(this.getClass());

	private TUserBiz ubiz;


	public ActionForward test(ActionMapping mapping, ActionForm form, HttpServletRequest req, HttpServletResponse rep) {
		log.info("test api success");
		Response.succ(rep, "test api success");
		return null;
	}

	public void setUbiz(TUserBiz ubiz) {
		this.ubiz = ubiz;
	}

}
