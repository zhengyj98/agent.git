package cn.cellcom.agent.struts.form;

import org.apache.struts.action.ActionForm;

public class TChannelForm extends ActionForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// ids字段为默认增加，是为了配合删除功能的需要
	private String[] ids;

	public String[] getIds() {
		return this.ids;
	}

	public void setIds(String[] ids) {
		this.ids = ids;
	}

	private java.lang.String id;

	private java.lang.String app;

	private java.lang.String insertTime;

	private java.lang.String no;

	private java.lang.String memo;

	private java.lang.String name;

	private java.lang.Integer type;

	private java.lang.String org;

	private java.lang.String wcAppid;
	private java.lang.String wcSecret;
	private java.lang.String webUrl;
	private java.lang.String qq;

	public java.lang.String getQq() {
		return qq;
	}

	public void setQq(java.lang.String qq) {
		this.qq = qq;
	}

	public java.lang.String getWcAppid() {
		return wcAppid;
	}

	public void setWcAppid(java.lang.String wcAppid) {
		this.wcAppid = wcAppid;
	}

	public java.lang.String getWcSecret() {
		return wcSecret;
	}

	public void setWcSecret(java.lang.String wcSecret) {
		this.wcSecret = wcSecret;
	}

	public java.lang.String getWebUrl() {
		return webUrl;
	}

	public void setWebUrl(java.lang.String webUrl) {
		this.webUrl = webUrl;
	}

	public java.lang.String getId() {
		return id;
	}

	public void setId(java.lang.String id) {
		this.id = id;
	}

	public java.lang.String getApp() {
		return app;
	}

	public void setApp(java.lang.String app) {
		this.app = app;
	}

	public java.lang.String getInsertTime() {
		return insertTime;
	}

	public void setInsertTime(java.lang.String insertTime) {
		this.insertTime = insertTime;
	}

	public java.lang.String getNo() {
		return no;
	}

	public void setNo(java.lang.String no) {
		this.no = no;
	}

	public java.lang.String getMemo() {
		return memo;
	}

	public void setMemo(java.lang.String memo) {
		this.memo = memo;
	}

	public java.lang.String getName() {
		return name;
	}

	public void setName(java.lang.String name) {
		this.name = name;
	}

	public java.lang.Integer getType() {
		return type;
	}

	public void setType(java.lang.Integer type) {
		this.type = type;
	}

	public java.lang.String getOrg() {
		return org;
	}

	public void setOrg(java.lang.String org) {
		this.org = org;
	}
}
