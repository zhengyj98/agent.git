package cn.cellcom.agent.struts.form;

import org.apache.struts.action.ActionForm;

public class TSessionForm extends ActionForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// ids字段为默认增加，是为了配合删除功能的需要
	private String[] ids;

	public String[] getIds() {
		return this.ids;
	}

	public void setIds(String[] ids) {
		this.ids = ids;
	}

	private java.lang.Long requestTime;

	private java.lang.String org;

	private java.lang.String id;

	private java.lang.String crm;

	private java.lang.String channel;

	private java.lang.Long firstAgentResponseTime;

	private java.lang.Integer systemMessageCount;

	private java.lang.Short satisfyScore;

	private java.lang.String extend10;

	private java.lang.String satisfyText;

	private java.lang.Long endTime;

	private java.lang.String city;

	private java.lang.String tag4;

	private java.lang.String tag2;

	private java.lang.String tag3;

	private java.lang.Integer agentMessageCount;

	private java.lang.String province;

	private java.lang.Long queueTime;

	private java.lang.String tag1;

	private java.lang.String extend1;

	private java.lang.String trace;

	private java.lang.String extend3;

	private java.lang.String extend2;

	private java.lang.Long leftTime;

	private java.lang.String extend5;

	private java.lang.String pid;

	private java.lang.String extend4;

	private java.lang.String endType;

	private java.lang.String extend7;

	private java.lang.Long agentTime;

	private java.lang.String requestGroup;

	private java.lang.String extend6;

	private java.lang.String agent;

	private java.lang.String extend9;

	private java.lang.Integer messageCountWhenAgent;

	private java.lang.String extend8;

	private java.lang.String ip;

	private java.lang.Long robotTime;

	private java.lang.String agentName;

	private java.lang.Integer messageCount;

	private java.lang.String step;
	
	private String name;
	
	private String phone;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public java.lang.Long getRequestTime() {
		return this.requestTime;
	}

	public void setRequestTime(java.lang.Long requestTime) {
		this.requestTime = requestTime;
	}

	public java.lang.String getOrg() {
		return this.org;
	}

	public void setOrg(java.lang.String org) {
		this.org = org;
	}

	public java.lang.String getId() {
		return this.id;
	}

	public void setId(java.lang.String id) {
		this.id = id;
	}

	public java.lang.String getCrm() {
		return this.crm;
	}

	public void setCrm(java.lang.String crm) {
		this.crm = crm;
	}

	public java.lang.String getChannel() {
		return this.channel;
	}

	public void setChannel(java.lang.String channel) {
		this.channel = channel;
	}

	public java.lang.Long getFirstAgentResponseTime() {
		return this.firstAgentResponseTime;
	}

	public void setFirstAgentResponseTime(java.lang.Long firstAgentResponseTime) {
		this.firstAgentResponseTime = firstAgentResponseTime;
	}

	public java.lang.Integer getSystemMessageCount() {
		return this.systemMessageCount;
	}

	public void setSystemMessageCount(java.lang.Integer systemMessageCount) {
		this.systemMessageCount = systemMessageCount;
	}

	public java.lang.Short getSatisfyScore() {
		return this.satisfyScore;
	}

	public void setSatisfyScore(java.lang.Short satisfyScore) {
		this.satisfyScore = satisfyScore;
	}

	public java.lang.String getExtend10() {
		return this.extend10;
	}

	public void setExtend10(java.lang.String extend10) {
		this.extend10 = extend10;
	}

	public java.lang.String getSatisfyText() {
		return this.satisfyText;
	}

	public void setSatisfyText(java.lang.String satisfyText) {
		this.satisfyText = satisfyText;
	}

	public java.lang.Long getEndTime() {
		return this.endTime;
	}

	public void setEndTime(java.lang.Long endTime) {
		this.endTime = endTime;
	}

	public java.lang.String getCity() {
		return this.city;
	}

	public void setCity(java.lang.String city) {
		this.city = city;
	}

	public java.lang.String getTag4() {
		return this.tag4;
	}

	public void setTag4(java.lang.String tag4) {
		this.tag4 = tag4;
	}

	public java.lang.String getTag2() {
		return this.tag2;
	}

	public void setTag2(java.lang.String tag2) {
		this.tag2 = tag2;
	}

	public java.lang.String getTag3() {
		return this.tag3;
	}

	public void setTag3(java.lang.String tag3) {
		this.tag3 = tag3;
	}

	public java.lang.Integer getAgentMessageCount() {
		return this.agentMessageCount;
	}

	public void setAgentMessageCount(java.lang.Integer agentMessageCount) {
		this.agentMessageCount = agentMessageCount;
	}

	public java.lang.String getProvince() {
		return this.province;
	}

	public void setProvince(java.lang.String province) {
		this.province = province;
	}

	public java.lang.Long getQueueTime() {
		return this.queueTime;
	}

	public void setQueueTime(java.lang.Long queueTime) {
		this.queueTime = queueTime;
	}

	public java.lang.String getTag1() {
		return this.tag1;
	}

	public void setTag1(java.lang.String tag1) {
		this.tag1 = tag1;
	}

	public java.lang.String getExtend1() {
		return this.extend1;
	}

	public void setExtend1(java.lang.String extend1) {
		this.extend1 = extend1;
	}

	public java.lang.String getTrace() {
		return this.trace;
	}

	public void setTrace(java.lang.String trace) {
		this.trace = trace;
	}

	public java.lang.String getExtend3() {
		return this.extend3;
	}

	public void setExtend3(java.lang.String extend3) {
		this.extend3 = extend3;
	}

	public java.lang.String getExtend2() {
		return this.extend2;
	}

	public void setExtend2(java.lang.String extend2) {
		this.extend2 = extend2;
	}

	public java.lang.Long getLeftTime() {
		return this.leftTime;
	}

	public void setLeftTime(java.lang.Long leftTime) {
		this.leftTime = leftTime;
	}

	public java.lang.String getExtend5() {
		return this.extend5;
	}

	public void setExtend5(java.lang.String extend5) {
		this.extend5 = extend5;
	}

	public java.lang.String getPid() {
		return this.pid;
	}

	public void setPid(java.lang.String pid) {
		this.pid = pid;
	}

	public java.lang.String getExtend4() {
		return this.extend4;
	}

	public void setExtend4(java.lang.String extend4) {
		this.extend4 = extend4;
	}

	public java.lang.String getEndType() {
		return this.endType;
	}

	public void setEndType(java.lang.String endType) {
		this.endType = endType;
	}

	public java.lang.String getExtend7() {
		return this.extend7;
	}

	public void setExtend7(java.lang.String extend7) {
		this.extend7 = extend7;
	}

	public java.lang.Long getAgentTime() {
		return this.agentTime;
	}

	public void setAgentTime(java.lang.Long agentTime) {
		this.agentTime = agentTime;
	}

	public java.lang.String getRequestGroup() {
		return this.requestGroup;
	}

	public void setRequestGroup(java.lang.String requestGroup) {
		this.requestGroup = requestGroup;
	}

	public java.lang.String getExtend6() {
		return this.extend6;
	}

	public void setExtend6(java.lang.String extend6) {
		this.extend6 = extend6;
	}

	public java.lang.String getAgent() {
		return this.agent;
	}

	public void setAgent(java.lang.String agent) {
		this.agent = agent;
	}

	public java.lang.String getExtend9() {
		return this.extend9;
	}

	public void setExtend9(java.lang.String extend9) {
		this.extend9 = extend9;
	}

	public java.lang.Integer getMessageCountWhenAgent() {
		return this.messageCountWhenAgent;
	}

	public void setMessageCountWhenAgent(java.lang.Integer messageCountWhenAgent) {
		this.messageCountWhenAgent = messageCountWhenAgent;
	}

	public java.lang.String getExtend8() {
		return this.extend8;
	}

	public void setExtend8(java.lang.String extend8) {
		this.extend8 = extend8;
	}

	public java.lang.String getIp() {
		return this.ip;
	}

	public void setIp(java.lang.String ip) {
		this.ip = ip;
	}

	public java.lang.Long getRobotTime() {
		return this.robotTime;
	}

	public void setRobotTime(java.lang.Long robotTime) {
		this.robotTime = robotTime;
	}

	public java.lang.String getAgentName() {
		return this.agentName;
	}

	public void setAgentName(java.lang.String agentName) {
		this.agentName = agentName;
	}

	public java.lang.Integer getMessageCount() {
		return this.messageCount;
	}

	public void setMessageCount(java.lang.Integer messageCount) {
		this.messageCount = messageCount;
	}

	public java.lang.String getStep() {
		return this.step;
	}

	public void setStep(java.lang.String step) {
		this.step = step;
	}

}
