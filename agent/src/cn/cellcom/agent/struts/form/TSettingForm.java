package cn.cellcom.agent.struts.form;

import org.apache.struts.action.ActionForm;

public class TSettingForm extends ActionForm {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7496248731734939541L;

	private String tag;

	private String subject;

	private int level = 1;

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

}
