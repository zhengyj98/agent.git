package cn.cellcom.agent.struts.form;

import org.apache.struts.action.ActionForm;

public class ReporterForm extends ActionForm {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3417169166955232432L;
	private int duration = 0;

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}
}
