package cn.cellcom.agent.biz;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;

import cn.cellcom.agent.common.AgentConstant;
import cn.cellcom.agent.pojo.TButton;
import cn.cellcom.agent.pojo.TUser;
import cn.cellcom.agent.struts.form.TButtonForm;
import cn.cellcom.agent.util.RedisManager;
import cn.cellcom.jar.biz.AbstractBiz;
import cn.cellcom.jar.file.FileLoad;
import cn.cellcom.jar.file.IFileLoad;
import cn.cellcom.jar.logon.LogonSession;
import cn.cellcom.jar.reflect.ToString;
import cn.cellcom.jar.util.AU;
import cn.cellcom.jar.util.CopyUtil;
import cn.cellcom.jar.util.IDGenerator;
import cn.cellcom.jar.util.LogUtil;
import cn.cellcom.jar.util.MyException;

public class TButtonBiz extends AbstractBiz {

	@Override
	public List delete(HttpServletRequest req, ActionForm form, Object obj) {

		TButtonForm fm = (TButtonForm) form;
		try {
			LogonSession ls = new LogonSession(req);
			log.info("delete log：" + ls.getAttribute("attribute.id") + "[" + req.getRemoteAddr() + "]");
			log.info(ToString.toString(fm, false));
		} catch (MyException e) {
		}
		String[] ids = fm.getIds();
		try {
			List list = new ArrayList();

			for (int i = 0; ids != null && i < ids.length; i++) {
				java.lang.String key = ids[i];

				list.add(this.getDao().myDelete(TButton.class, key));

			}

			return list;
		} catch (MyException e) {
			LogUtil.e(this.getClass(), "删除数据出现错误", e);
			return null;
		}
	}

	@Override
	public Object add(HttpServletRequest req, ActionForm form, Object obj) {
		TButtonForm fm = (TButtonForm) form;
		LogonSession ls = new LogonSession(req);
		try {
			log.info("add log：" + ls.getAttribute("attribute.id") + "[" + req.getRemoteAddr() + "]");
			log.info(ToString.toString(fm, false));
		} catch (MyException e) {
		}
		try {
			java.lang.String key = fm.getId();
			// 你必须修改这里的id值
			if (key != null && this.getDao().myGet(TButton.class, key) != null) {
				return "该记录已经存在，不要重复添加";
			}
			TButton pojo = new TButton();
			// 以下为pojo的必填字段
			CopyUtil.copy(req, pojo, null, false);
			TUser user = (TUser) ls.getLogonObject();
			if (fm.getImgFile() != null) {
				IFileLoad fl = new FileLoad();
				String[] newName = user.getSettingFile(fm.getImgFile().getFileName());
				fl.uploadToFile(fm.getImgFile(), newName[1]);
				pojo.setImg(newName[0]);
			}
			pojo.setId(IDGenerator.getDefaultUUID());
			pojo.setOrg(user.getOrg());
			pojo.setPid(user.getPid());
			pojo.setSeq(RedisManager.getInstance().getButtonNo(user.getPid()));
			pojo.setSubject(fm.getSubject());

			this.getDao().mySave(pojo);

			return pojo;
		} catch (MyException e) {
			return LogUtil.e(this.getClass(), "新增数据出现错误", e);
		}
	}

	@Override
	public Object modify(HttpServletRequest req, ActionForm form, Object obj) {
		TButtonForm fm = (TButtonForm) form;
		LogonSession ls = new LogonSession(req);
		try {
			log.info("modify log：" + ls.getAttribute("attribute.id") + "[" + req.getRemoteAddr() + "]");
			log.info(ToString.toString(fm, false));
		} catch (MyException e) {
		}
		try {
			java.lang.String key = fm.getId();
			TButton pojo = (TButton) this.getDao().myGet(TButton.class, key);
			if (pojo == null) {
				return "修改的记录不存在";
			}
			log.info("被修改的pojo：" + ToString.toString(TButton.class, pojo, false));

			// 以下为pojo的必填字段
			CopyUtil.copy(req, pojo, null, false);

			this.getDao().myUpdate(pojo);

			return pojo;
		} catch (MyException e) {
			return LogUtil.e(this.getClass(), "修改数据出现错误", e);
		}

	}

	/**
	 * 获取企业或者某个渠道的按钮配置
	 * 
	 * @param org
	 * @param pid
	 * @param channel
	 * @return
	 * @throws MyException
	 */
	public List<TButton> getVisitorButtonList(String pid, String channel) throws MyException {
		String hql = " from " + TButton.class.getSimpleName() + " where pid=? order by seq";
		String[] values = new String[] { pid };
		List<TButton> list = this.getDao().myList(hql, values);

		List<TButton> defaultList = new ArrayList<TButton>();
		List<TButton> channelList = new ArrayList<TButton>();
		for (int i = 0; i < list.size(); i++) {
			TButton one = list.get(i);
			if (AgentConstant.DEFAULT_ORG.equals(one.getSubject())) {
				defaultList.add(one);
			} else if (channel != null && channel.equals(one.getSubject())) {
				channelList.add(one);
			}
		}
		if (AU.isNotBlank(channelList)) {
			return channelList;
		}
		return defaultList;
	}
}
