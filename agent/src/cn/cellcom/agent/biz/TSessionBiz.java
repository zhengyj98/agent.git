package cn.cellcom.agent.biz;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;

import cn.cellcom.agent.pojo.TAgentSession;
import cn.cellcom.agent.pojo.TCrm;
import cn.cellcom.agent.pojo.TSession;
import cn.cellcom.agent.pojo.TUser;
import cn.cellcom.jar.biz.AbstractBiz;
import cn.cellcom.jar.util.DT;
import cn.cellcom.jar.util.MyException;

public class TSessionBiz extends AbstractBiz {

	@Override
	public List delete(HttpServletRequest req, ActionForm form, Object obj) {

		return null;
	}

	@Override
	public Object add(HttpServletRequest req, ActionForm form, Object obj) {
		return null;
	}

	@Override
	public Object modify(HttpServletRequest req, ActionForm form, Object obj) {
		return null;

	}

	/**
	 * 查询访客正在聊天的会话
	 * 
	 * @param crm
	 * @return
	 * @throws MyException
	 */
	public TSession getTalkingSessionOfVisitor(String crm) throws MyException {
		String hql = "from " + TSession.class.getSimpleName() + " where endTime is null and crm=?";
		return (TSession) this.getDao().myGet(hql, new String[] { crm });
	}

	/**
	 * 查询坐席正在聊天的访客
	 * 
	 * @param agent
	 * @return
	 * @throws MyException
	 */
	public List getTalkingSessionOfAgent(TUser agent) throws MyException {
		String hql = "select a, b, c from " + TSession.class.getSimpleName() + " as a, " + TAgentSession.class.getSimpleName() + " as b, "
				+ TCrm.class.getSimpleName()
				+ " as c where a.id=b.session and a.crm=c.id and b.endTime is null and b.agent=? order by a.requestTime";
		return this.getDao().myList(hql, new String[] { agent.getUsername() });
	}

	/**
	 * 查询坐席的历史访客
	 * 
	 * @param agent
	 * @return
	 * @throws MyException
	 */
	public List getHistoryVisitorOfAgent(TUser agent) throws MyException {
		String hql = "select a, b, c from "
				+ TSession.class.getSimpleName()
				+ " as a, "
				+ TAgentSession.class.getSimpleName()
				+ " as b, "
				+ TCrm.class.getSimpleName()
				+ " as c where a.id=b.session and a.crm=c.id and b.endTime is not null and b.agent=? and a.requestTime>? order by a.requestTime desc limit 50";
		List list = this.getDao().myList(hql, new Object[] { agent.getUsername(), System.currentTimeMillis() - DT.ADAY * 10 });

		Set<String> temp = new HashSet<String>();
		List result = new ArrayList();
		for (int i = 0; i < list.size(); i++) {
			Object[] arr = (Object[]) list.get(i);
			TSession s = (TSession) arr[0];
			TCrm c = (TCrm) arr[2];
			if (!temp.contains(s.getCrm())) {
				temp.add(s.getCrm());
				result.add(arr);
			}
		}
		return result;
	}

	/**
	 * 查询访客的历史访问会话
	 * 
	 * @param string
	 * 
	 * @param agent
	 * @return
	 * @throws MyException
	 */
	public List getHistorySessionOfVisitor(String pid, String cid) throws MyException {
		String hql = "select a, b from " + TSession.class.getSimpleName() + " as a, " + TAgentSession.class.getSimpleName() + " as b "
				+ " where a.id=b.session and b.endTime is not null and a.pid=? and a.crm=? order by a.requestTime desc limit 30";
		List list = this.getDao().myList(hql, new Object[] { pid, cid });

		return list;
	}
}
