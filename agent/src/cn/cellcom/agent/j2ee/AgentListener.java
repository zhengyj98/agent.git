package cn.cellcom.agent.j2ee;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;

import cn.cellcom.adapter.service.QqVisitorHandler;
import cn.cellcom.adapter.service.GzhVisitorHandler;
import cn.cellcom.agent.biz.Org00Biz;
import cn.cellcom.agent.biz.TButtonBiz;
import cn.cellcom.agent.biz.TChannelBiz;
import cn.cellcom.agent.biz.TCrmBiz;
import cn.cellcom.agent.biz.TGroupBiz;
import cn.cellcom.agent.biz.TLwBiz;
import cn.cellcom.agent.biz.TQuestionBiz;
import cn.cellcom.agent.biz.TSessionBiz;
import cn.cellcom.agent.biz.TSettingBiz;
import cn.cellcom.agent.common.AgentConstant;
import cn.cellcom.agent.common.AgentEnv;
import cn.cellcom.agent.online.client.SessionManager;
import cn.cellcom.jar.biz.ListenerInitBiz;
import cn.cellcom.jar.context.SpringWebApplicationContext;
import cn.cellcom.jar.dao.IBaseDao;
import cn.cellcom.jar.pagination.IPaginationDao;
import cn.cellcom.jar.util.ABean;
import cn.cellcom.jar.util.BeanUtil;
import cn.cellcom.jar.util.LogUtil;
import cn.cellcom.jar.util.MyException;
import cn.cellcom.rpc.RpcFramework;

public class AgentListener extends ListenerInitBiz {
	private static IPaginationDao mgPageDao;

	private static IBaseDao mgDao;

	private static IBaseDao dao;

	@Override
	public Object destory(ServletContext context) {
		String hql = "update TSession set endTime = ?, endType=? where step = ? and endType is null";
		try {
			this.getDao().myExecute(hql, new Object[] { System.currentTimeMillis(), AgentConstant.SESSION_END_TYPE.BY_REBOOT.name(),
					AgentConstant.SESSION_STEP.QUEUE.name() });
		} catch (MyException e) {
			LogUtil.e(this.getClass(), "Update the queue session fail", e);
		}
		SessionManager.getInstance().destory();
		return null;
	}

	@Override
	public Object exe(ServletContext context) {
		/*
		 * ActiveMqSender.init(); ActiveMqReceiver.init();
		 */

		List<ABean> hourList23 = new ArrayList<ABean>();
		for (int i = 1; i <= 23; i++) {
			String a = i < 10 ? "0" + i : String.valueOf(i);
			hourList23.add(new ABean(a, a, a, null));
		}
		context.setAttribute("hourList23", hourList23);

		List<ABean> minutList59 = new ArrayList<ABean>();
		for (int i = 0; i <= 59; i++) {
			String a = i < 10 ? "0" + i : String.valueOf(i);
			minutList59.add(new ABean(a, a, a, null));
		}
		context.setAttribute("minutList59", minutList59);

		List<ABean> maxChatList = new ArrayList<ABean>();
		for (int i = 0; i <= 40; i++) {
			maxChatList.add(new ABean("" + i, "" + i, "" + i, null));
		}
		maxChatList.add(new ABean("50", "50", "50", null));
		maxChatList.add(new ABean("60", "60", "60", null));
		maxChatList.add(new ABean("70", "70", "70", null));
		context.setAttribute("maxChatList", maxChatList);

		context.setAttribute("satisfyInviteTypeList", BeanUtil.startWith("TSetting-satisfyInviteType"));
		context.setAttribute("chatMaxTypeList", BeanUtil.startWith("TSetting-chatMaxType"));
		context.setAttribute("selfPolicyList", BeanUtil.startWith("TSetting-selfPolicy"));
		context.setAttribute("guidePolicyList", BeanUtil.startWith("TSetting-guidePolicy"));
		context.setAttribute("visitorQueuePolicyList", BeanUtil.startWith("TSetting-visitorQueuePolicy"));
		context.setAttribute("sessionDispatchPolicyList", BeanUtil.startWith("TSetting-sessionDispatchPolicy"));
		context.setAttribute("noOnlineAgentPolicyList", BeanUtil.startWith("TSetting-noOnlineAgentPolicy"));
		context.setAttribute("queueTalkingPolicyList", BeanUtil.startWith("TSetting-queueTalkingPolicy"));
		context.setAttribute("historyMessagePolicyList", BeanUtil.startWith("TSetting-historyMessagePolicy"));
		context.setAttribute("loginDefaultStatusList", BeanUtil.startWith("TSetting-loginDefaultStatus"));
		context.setAttribute("agentLogoutList", BeanUtil.startWith("TSetting-agentLogout"));
		context.setAttribute("agentTalkingPolicyList", BeanUtil.startWith("TSetting-agentTalkingPolicy"));
		context.setAttribute("windowShowTypeList", BeanUtil.startWith("TSetting-windowShowType"));
		context.setAttribute("windowOpenTypeList", BeanUtil.startWith("TSetting-windowOpenType"));
		context.setAttribute("satisfyInputList", BeanUtil.startWith("TSetting-satisfyInput"));
		context.setAttribute("robotTypeList", BeanUtil.startWith("TSetting-robotType"));

		Thread t = new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					TSettingBiz sbiz = (TSettingBiz) SpringWebApplicationContext.getBean("TSettingBiz");
					TButtonBiz bbiz = (TButtonBiz) SpringWebApplicationContext.getBean("TButtonBiz");
					Org00Biz obiz = (Org00Biz) SpringWebApplicationContext.getBean("Org00Biz");
					TSessionBiz ssbiz = (TSessionBiz) SpringWebApplicationContext.getBean("TSessionBiz");
					TGroupBiz gbiz = (TGroupBiz) SpringWebApplicationContext.getBean("TGroupBiz");
					TQuestionBiz qbiz = (TQuestionBiz) SpringWebApplicationContext.getBean("TQuestionBiz");
					TCrmBiz cbiz = (TCrmBiz) SpringWebApplicationContext.getBean("TCrmBiz");
					TChannelBiz chbiz = (TChannelBiz) SpringWebApplicationContext.getBean("TChannelBiz");
					TLwBiz lbiz = (TLwBiz) SpringWebApplicationContext.getBean("TLwBiz");
					QqVisitorHandler.init(sbiz, bbiz, obiz, ssbiz, gbiz, qbiz, cbiz, chbiz, lbiz);

					
					RpcFramework.export(new GzhVisitorHandler(sbiz, bbiz, obiz, ssbiz, gbiz, qbiz, cbiz, chbiz, lbiz), AgentEnv.RPC_SERVER_PORT);
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		});
		t.start();
		return null;
	}

	public IBaseDao getDao() {
		return dao;
	}

	public static IBaseDao getStaticDao() {
		return dao;
	}

	public void setDao(IBaseDao dao) {
		AgentListener.dao = dao;
	}

	public static IBaseDao getMgDao() {
		return mgDao;
	}

	public void setMgDao(IBaseDao mgDao) {
		AgentListener.mgDao = mgDao;
	}

	public static IPaginationDao getMgPageDao() {
		return mgPageDao;
	}

	public void setMgPageDao(IPaginationDao mgPageDao) {
		AgentListener.mgPageDao = mgPageDao;
	}
}
