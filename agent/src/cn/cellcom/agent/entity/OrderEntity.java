package cn.cellcom.agent.entity;

import java.io.Serializable;

public class OrderEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7224388910484271371L;

	private String id;

	private String imgUrl;

	private String href;

	private String title;

	private String desc;

	private int confirmSend;

	private String time;
	
	private String price;
	
	private String orderNo;
	
	private String jsonString;
	
	public String getJsonString() {
		return jsonString;
	}

	public void setJsonString(String jsonString) {
		this.jsonString = jsonString;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getImgUrl() {
		return imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}


	public String getHref() {
		return href;
	}

	public void setHref(String href) {
		this.href = href;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public int getConfirmSend() {
		return confirmSend;
	}

	public void setConfirmSend(int confirmSend) {
		this.confirmSend = confirmSend;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}
}
