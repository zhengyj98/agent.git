package cn.cellcom.agent.entity;

public class MonitorPandect {
	private int agentCount;
	private int agentOnlineCount;
	private int agentGoawayCount;
	private int sessionCount;
	private int selfTalkingCount;
	private int agentTalkingCount;
	private int agentAvailableCount;
	private int queueingCount;
	private int otherCount;
	private int todaySessionCount;
	private int todayCrmCount;
	private int todayAgentCount;
	private int todayAgentSessionCount;

	public int getAgentCount() {
		return agentCount;
	}

	public void setAgentCount(int agentCount) {
		this.agentCount = agentCount;
	}

	public int getAgentOnlineCount() {
		return agentOnlineCount;
	}

	public void setAgentOnlineCount(int agentOnlineCount) {
		this.agentOnlineCount = agentOnlineCount;
	}

	public int getAgentGoawayCount() {
		return agentGoawayCount;
	}

	public void setAgentGoawayCount(int agentGoawayCount) {
		this.agentGoawayCount = agentGoawayCount;
	}

	public int getSessionCount() {
		return sessionCount;
	}

	public void setSessionCount(int sessionCount) {
		this.sessionCount = sessionCount;
	}

	public int getSelfTalkingCount() {
		return selfTalkingCount;
	}

	public void setSelfTalkingCount(int selfTalkingCount) {
		this.selfTalkingCount = selfTalkingCount;
	}

	public int getAgentTalkingCount() {
		return agentTalkingCount;
	}

	public void setAgentTalkingCount(int agentTalkingCount) {
		this.agentTalkingCount = agentTalkingCount;
	}

	public int getAgentAvailableCount() {
		return agentAvailableCount;
	}

	public void setAgentAvailableCount(int agentAvailableCount) {
		this.agentAvailableCount = agentAvailableCount;
	}

	public int getQueueingCount() {
		return queueingCount;
	}

	public void setQueueingCount(int queueingCount) {
		this.queueingCount = queueingCount;
	}

	public int getOtherCount() {
		return otherCount;
	}

	public void setOtherCount(int otherCount) {
		this.otherCount = otherCount;
	}

	public int getTodaySessionCount() {
		return todaySessionCount;
	}

	public void setTodaySessionCount(int todaySessionCount) {
		this.todaySessionCount = todaySessionCount;
	}

	public int getTodayCrmCount() {
		return todayCrmCount;
	}

	public void setTodayCrmCount(int todayCrmCount) {
		this.todayCrmCount = todayCrmCount;
	}

	public int getTodayAgentCount() {
		return todayAgentCount;
	}

	public void setTodayAgentCount(int todayAgentCount) {
		this.todayAgentCount = todayAgentCount;
	}

	public void setAgent(int agentCount2, int agentOnlineCount2, int agentGoawayCount2) {
		this.agentCount = agentCount2;
		this.agentOnlineCount = agentOnlineCount2;
		this.agentGoawayCount = agentGoawayCount2;
	}

	public void setSession(int sessionCount2, int selfTalkingCount2, int agentTalkingCount2, int agentAvailableCount2, int queueingCount2,
			int otherCount2) {
		this.sessionCount = sessionCount2;
		this.selfTalkingCount = selfTalkingCount2;
		this.agentAvailableCount = agentAvailableCount2;
		this.agentTalkingCount = agentTalkingCount2;
		this.queueingCount = queueingCount2;
		this.otherCount = otherCount2;
	}

	public void setToday(Integer todaySessionCount2, Integer todayCrmCount2, Integer todayAgentcount2) {
		this.todaySessionCount = todaySessionCount2;
		this.todayCrmCount = todayCrmCount2;
		this.todayAgentCount = todayAgentcount2;
	}
	

	public int getTodayAgentSessionCount() {
		return todayAgentSessionCount;
	}

	public void setTodayAgentSessionCount(int todayAgentSessionCount) {
		this.todayAgentSessionCount = todayAgentSessionCount;
	}

	public void setTodayAgentSessionCount(Integer todayAgentSessionCount) {
		this.todayAgentSessionCount = todayAgentSessionCount;
	}

}
