package cn.cellcom.agent.my.simple;

public class MySimpleResult {

	public static MySimpleJson success() {
		return new MySimpleJson();
	}

	public static MySimpleJson success(String message) {
		return new MySimpleJson(true, message);
	}

	public static MySimpleJson error() {
		return new MySimpleJson(false, "操作失败");
	}

	public static MySimpleJson error(String message) {
		return new MySimpleJson(false, message);
	}

}
