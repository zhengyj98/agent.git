package cn.cellcom.agent.my.simple;

import java.util.HashMap;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;

/**
 * $.ajax后需要接受的JSON
 * 
 * @author
 * 
 */
public class MySimpleJson {

	private boolean success = true;// 是否成功
	private String message = "操作成功";// 提示信息
	private Object obj = null;// 对象信息
	private Map<String, Object> attributes = new HashMap<>();// 其他参数

	public MySimpleJson(boolean success, String message) {
		this.success = success;
		this.message = message;
	}

	public MySimpleJson() {
	}

	public Map<String, Object> getAttributes() {
		return attributes;
	}

	public void setAttributes(Map<String, Object> attributes) {
		this.attributes = attributes;
	}

	public void addAttributes(String key, Object value) {
		this.attributes.put(key, value);
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object getObj() {
		return obj;
	}

	public void setObj(Object obj) {
		this.obj = obj;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getJsonStr() {
		JSONObject obj = new JSONObject();
		obj.put("success", this.isSuccess());
		obj.put("message", this.getMessage());
		obj.put("obj", this.obj);
		obj.put("attributes", this.attributes);
		return obj.toJSONString();
	}
}
