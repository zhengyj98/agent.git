package cn.cellcom.agent.online.dispatcher;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Set;

import cn.cellcom.agent.online.client.AgentClient;
import cn.cellcom.agent.online.client.ClientManager;
import cn.cellcom.agent.online.wrapper.SessionWrapper;

public class RandomAgentSelector extends AbstractAgentSelector {

	@Override
	public AgentClient bestAgentFrom(SessionWrapper session) {
		List<AgentClient> validateList = new ArrayList<AgentClient>();
		Set<AgentClient> agents = ClientManager.getInstance().getOnlineAgents(session.getPid());
		if(agents != null) {
			for (AgentClient ac : agents) {
				boolean validate = super.validateAgent(ac, session);
				if (validate) {
					validateList.add(ac);
				}
			}
		}

		if (validateList.size() == 0) {
			return null;
		}
		return validateList.get(new Random().nextInt(validateList.size()));
	}
}
