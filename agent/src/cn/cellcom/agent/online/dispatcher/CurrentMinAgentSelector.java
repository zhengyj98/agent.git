package cn.cellcom.agent.online.dispatcher;

import java.util.Comparator;

import cn.cellcom.agent.online.client.AgentClient;
import cn.cellcom.agent.online.wrapper.SessionWrapper;

public class CurrentMinAgentSelector extends AbstractAgentSelector {

	@Override
	public AgentClient bestAgentFrom(SessionWrapper session) {
		return super.bestAgentFrom(session, new AgentClientComparator());
	}

	private class AgentClientComparator implements Comparator<AgentClient> {
		@Override
		public int compare(AgentClient o1, AgentClient o2) {
			return (o1.getAgentConfig().getMaxChat() - o1.getCurrentChat()) - (o2.getAgentConfig().getMaxChat() - o2.getCurrentChat());
		}
	}

}
