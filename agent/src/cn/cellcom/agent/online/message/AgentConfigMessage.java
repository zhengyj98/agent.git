package cn.cellcom.agent.online.message;

import cn.cellcom.agent.online.client.AgentClient;
import cn.cellcom.agent.online.client.AgentConfig;

public class AgentConfigMessage extends BizMessage {

	public AgentConfigMessage(AgentClient agentClient) {
		super.setEvent(MessageConstant.MESSAGE_EVENT.NOTIFY);
		super.setNotify(MessageConstant.MESSAGE_NOTIFY.AGENT_CONFIG);
		AgentConfig config = agentClient.getAgentConfig();
		config.setCurrentChat(agentClient.getCurrentChat());
		super.setObject(config);
	}
}
