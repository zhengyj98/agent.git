package cn.cellcom.agent.online.message;

import java.util.List;

import cn.cellcom.agent.online.wrapper.SessionWrapper;
import cn.cellcom.agent.pojo.TQuestion;

/**
 * @author zhengyj
 *
 */
public class QuestionListMessage extends BizMessage {

	public QuestionListMessage(SessionWrapper session, List<TQuestion> list) {
		super.setPid(session.getSession().getPid());
		super.setSid(session.getId());
		super.setReceiver(session.getSession().getCrm());
		super.setEvent(MessageConstant.MESSAGE_EVENT.QUESTION_LIST);
		super.setObject(list);
		super.setCid(session.getSession().getCrm());
	}
}
