package cn.cellcom.agent.online.message;

import cn.cellcom.jar.mongo.IMongoPojo;
import cn.cellcom.jar.util.DT;

public class ArchiveMessage implements IMongoPojo {

	/**
	 * 消息的ID
	 */
	private String id;

	/**
	 * 企业ID
	 */
	private String org = "";
	
	/**
	 * 
	 */
	private String pid;

	/**
	 * 消息类型
	 */
	private String multiType = null;

	/**
	 * 消息事件
	 */
	private String event = null;

	/**
	 * 管理员/坐席/访客
	 */
	private String senderRole = "";

	/**
	 * 发送者的ID
	 */
	private String sender = "";

	/**
	 * 发送者的昵称
	 */
	private String senderNickname = "";
	
	/**
	 * 接收者，坐席之间单聊的时候使用
	 */
	private String receiver;
	
	/**
	 * 
	 */
	private String time = DT.getYYYYMMDDHHMMSS();

	/**
	 * 发送的文本内容
	 */
	private String text = null;

	/**
	 * 发送的对象
	 */
	private String object;

	/**
	 * 显示的策略： 0表示多方可以查看，1表示仅坐席，2表示仅访客，3表示仅管理，默认是所有
	 */
	private String showPolicy = MessageConstant.ARCHIVE_SHOW_POLICY.ALL.name();

	/**
	 * 会话的ID
	 */
	private String sid;

	/**
	 * 访客
	 */
	private String cid;

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getOrg() {
		return org;
	}

	public void setOrg(String org) {
		this.org = org;
	}

	public String getMultiType() {
		return multiType;
	}

	public void setMultiType(String multiType) {
		this.multiType = multiType;
	}

	public String getEvent() {
		return event;
	}

	public void setEvent(String event) {
		this.event = event;
	}

	public String getSenderRole() {
		return senderRole;
	}

	public void setSenderRole(String senderRole) {
		this.senderRole = senderRole;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public String getSenderNickname() {
		return senderNickname;
	}

	public void setSenderNickname(String senderNickname) {
		this.senderNickname = senderNickname;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getObject() {
		return object;
	}

	public void setObject(String object) {
		this.object = object;
	}

	public String getShowPolicy() {
		return showPolicy;
	}

	public void setShowPolicy(String showPolicy) {
		this.showPolicy = showPolicy;
	}

	public String getSid() {
		return sid;
	}

	public void setSid(String sid) {
		this.sid = sid;
	}

	public String getCid() {
		return cid;
	}

	public void setCid(String cid) {
		this.cid = cid;
	}

	@Override
	public String get_IdName() {
		return "id";
	}

	public String getReceiver() {
		return receiver;
	}

	public void setReceiver(String receiver) {
		this.receiver = receiver;
	}

	
}
