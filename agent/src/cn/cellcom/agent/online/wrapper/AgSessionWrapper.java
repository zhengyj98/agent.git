package cn.cellcom.agent.online.wrapper;

import cn.cellcom.agent.online.client.AgentClient;
import cn.cellcom.agent.pojo.TAgentSession;

public class AgSessionWrapper {

	private TAgentSession agsession;
	private AgentClient agclient;

	AgSessionWrapper(TAgentSession agsession, AgentClient agclient) {
		this.agclient = agclient;
		this.agsession = agsession;
	}

	public TAgentSession getAgsession() {
		return agsession;
	}

	public void setAgsession(TAgentSession agsession) {
		this.agsession = agsession;
	}

	public AgentClient getAgclient() {
		return agclient;
	}

	public void setAgclient(AgentClient agclient) {
		this.agclient = agclient;
	}
	
	
}
