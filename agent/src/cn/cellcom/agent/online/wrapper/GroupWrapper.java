package cn.cellcom.agent.online.wrapper;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import cn.cellcom.agent.online.client.AgentClient;
import cn.cellcom.agent.pojo.TGroup;

public class GroupWrapper {

	private TGroup group;

	/**
	 * 本组的签入本组的坐席
	 */
	Map<String, AgentClient> agents = new HashMap<String, AgentClient>();

	public String getId() {
		return group.getId();
	}

	public TGroup getGroup() {
		return group;
	}

	public GroupWrapper(TGroup group) {
		this.group = group;
		// startDetectionTask();
	}

	public void addAgentClient(AgentClient agent) {
		agents.put(agent.getId(), agent);
	}

	public void removeAgentClient(AgentClient agent) {
		agents.remove(agent.getId());
	}

	public Map<String, AgentClient> getAgents() {
		return agents;
	}

	public int countAgents() {
		return agents.size();
	}

	public int countOnlineAgents() {
		return -2;
	}

}
