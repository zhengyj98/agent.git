package cn.cellcom.agent.online.client;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SessionManager {
	private Logger log = LoggerFactory.getLogger(this.getClass());

	private static SessionManager manager = null;
	private Map<String, SessionPool> pools = new ConcurrentHashMap<String, SessionPool>();

	private SessionManager() {
	}

	public static SessionManager getInstance() {
		if (manager == null) {
			manager = new SessionManager();
		}
		return manager;
	}

	public SessionPool getPool(String pid) {
		SessionPool q = pools.get(pid);
		if (q == null) {
			synchronized (("pool_" + pid).intern()) {
				if (q == null) {
					q = new SessionPool(pid);
					pools.put(pid, q);
					log.info("create Session pool for pid " + pid);
				}
			}
		}
		return q;
	}

	/**
	 * 销毁
	 */
	public void destory() {
		for (SessionPool pool : pools.values()) {
			pool.writeBack();
		}
	}
}
