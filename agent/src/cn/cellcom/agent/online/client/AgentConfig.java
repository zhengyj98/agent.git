package cn.cellcom.agent.online.client;

import cn.cellcom.agent.common.AgentConstant.AGENT_STATUS;

/**
 * 坐席的登陆的在线信息
 * 
 * @author zhengyj
 * 
 */
public class AgentConfig {

	private AGENT_STATUS status;

	private int maxChat = 0;

	private int currentChat = 0;

	public AgentConfig() {
	}

	public AGENT_STATUS getStatus() {
		return status;
	}

	public void setStatus(AGENT_STATUS status) {
		this.status = status;
	}

	public int getMaxChat() {
		return maxChat;
	}

	public void setMaxChat(int maxChat) {
		this.maxChat = maxChat;
	}

	public int getCurrentChat() {
		return currentChat;
	}

	public void setCurrentChat(int currentChat) {
		this.currentChat = currentChat;
	}

}
