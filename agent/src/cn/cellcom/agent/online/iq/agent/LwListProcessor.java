package cn.cellcom.agent.online.iq.agent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.cellcom.agent.biz.TLwBiz;
import cn.cellcom.agent.online.client.AgentClient;
import cn.cellcom.agent.online.client.Client;
import cn.cellcom.agent.online.iq.AbstractChainedProcessor;
import cn.cellcom.agent.online.iq.Processor;
import cn.cellcom.agent.online.iq.ProcessorResult;
import cn.cellcom.agent.online.message.BizMessage;
import cn.cellcom.agent.pojo.TCrm;
import cn.cellcom.agent.pojo.TLw;
import cn.cellcom.agent.struts.form.VisitorForm;
import cn.cellcom.jar.biz.AbstractBiz;
import cn.cellcom.jar.util.LogUtil;
import cn.cellcom.jar.util.MyException;

public class LwListProcessor extends AbstractChainedProcessor {

	private Logger log = LoggerFactory.getLogger(this.getClass());

	public LwListProcessor(Processor next) {
		super(next);
	}

	@Override
	public ProcessorResult doProcess(Map<String, AbstractBiz> bizs, Client client2, VisitorForm cf) {
		AgentClient client = (AgentClient) client2;
		List list = new ArrayList();
		try {
			TLwBiz lbiz = (TLwBiz) bizs.get("lbiz");
			if ("lwNoHandled".equals(cf.getEvent())) {
				list = lbiz.getNoHandled(client.getPid());
			} else if ("lwHandled".equals(cf.getEvent())) {
				list = lbiz.getHandled(client.getPid());
			}
			for (int i = 0; i < list.size(); i++) {
				Object[] arr = (Object[]) list.get(i);
				Map<String, Object> map = new HashMap<String, Object>();
				TLw lw = (TLw) arr[0];
				TCrm crm = (TCrm) arr[1];
				map.put("channel", lw.getChannel());
				map.put("crm", crm);
				// 格式兼容分配会话的时候下发的消息类型
				BizMessage bm = new BizMessage();
				bm.setSid(lw.getId());
				bm.setObject(map);
				list.set(i, bm);

			}
		} catch (MyException e) {
			LogUtil.e(this.getClass(), "", e);
			return new ProcessorResult(false, this, list);
		}
		return new ProcessorResult(true, this, list);
	}

	@Override
	public boolean isAcceptable(VisitorForm cf) {
		return "lwNoHandled".equals(cf.getEvent()) || "lwHandled".equals(cf.getEvent());
	}
}
