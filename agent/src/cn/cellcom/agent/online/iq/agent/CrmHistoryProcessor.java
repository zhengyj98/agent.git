package cn.cellcom.agent.online.iq.agent;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.cellcom.agent.biz.TSessionBiz;
import cn.cellcom.agent.online.client.AgentClient;
import cn.cellcom.agent.online.client.Client;
import cn.cellcom.agent.online.iq.AbstractChainedProcessor;
import cn.cellcom.agent.online.iq.Processor;
import cn.cellcom.agent.online.iq.ProcessorResult;
import cn.cellcom.agent.struts.form.VisitorForm;
import cn.cellcom.jar.biz.AbstractBiz;
import cn.cellcom.jar.util.MyException;

public class CrmHistoryProcessor extends AbstractChainedProcessor {

	public static final String NAME = "crmHistory";

	private Logger log = LoggerFactory.getLogger(this.getClass());

	public CrmHistoryProcessor(Processor next) {
		super(next);
	}

	@Override
	public ProcessorResult doProcess(Map<String, AbstractBiz> bizs, Client client, VisitorForm cf) {
		List list = null;
		try {
			TSessionBiz cbiz = (TSessionBiz) bizs.get("sbiz");
			AgentClient ac = (AgentClient) client;
			list = cbiz.getHistorySessionOfVisitor(ac.getPid(), cf.getCid());
		} catch (MyException e) {
			log.error("[{}] get crm history[{}] fail.", client.getId(), cf.getCid());
			log.error("", e.getException());
			return new ProcessorResult(false, this, "");
		}
		return new ProcessorResult(true, this, list);
	}

	@Override
	public boolean isAcceptable(VisitorForm message) {
		return NAME.equals(message.getEvent());
	}
}
