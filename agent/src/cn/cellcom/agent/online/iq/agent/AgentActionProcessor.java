package cn.cellcom.agent.online.iq.agent;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.cellcom.agent.common.AgentConstant.AGENT_STATUS;
import cn.cellcom.agent.common.AgentConstant.AG_SESSION_END_TYPE;
import cn.cellcom.agent.online.client.AgentClient;
import cn.cellcom.agent.online.client.Client;
import cn.cellcom.agent.online.client.IDManager;
import cn.cellcom.agent.online.iq.AbstractChainedProcessor;
import cn.cellcom.agent.online.iq.Processor;
import cn.cellcom.agent.online.iq.ProcessorResult;
import cn.cellcom.agent.online.message.MessageConstant;
import cn.cellcom.agent.online.message.MessageConstant.ARCHIVE_EVENT;
import cn.cellcom.agent.online.message.MessageConstant.MESSAGE_NOTIFY;
import cn.cellcom.agent.online.message.MessageReceipt;
import cn.cellcom.agent.online.wrapper.SessionWrapper;
import cn.cellcom.agent.struts.form.VisitorForm;
import cn.cellcom.jar.biz.AbstractBiz;
import cn.cellcom.jar.util.AU;
import cn.cellcom.jar.util.CU;
import cn.cellcom.jar.util.MyException;

public class AgentActionProcessor extends AbstractChainedProcessor {
	private Logger log = LoggerFactory.getLogger(this.getClass());

	public AgentActionProcessor(Processor next) {
		super(next);
	}

	@Override
	public ProcessorResult doProcess(Map<String, AbstractBiz> bizs, Client client, VisitorForm cf) {
		AgentClient ac = (AgentClient) client;
		if (MessageConstant.MESSAGE_EVENT.AG_CHANGE_STATUS.name().equals(cf.getEvent())) {
			log.info("[{}] set the status to [{}]", ac.getId(), cf.getValue());
			// 设置状态然后再次通知其他坐席
			ac.getAgentConfig().setStatus(AGENT_STATUS.valueOf(cf.getValue()));
			ac.notifyAgentInfo(false);
		} else if (MessageConstant.MESSAGE_EVENT.AG_JOIN_GROUP.name().equals(cf.getEvent())) {// 进入技能组
			log.info("[{}] join the groups [{}]", ac.getId(), cf.getValue());
			try {
				ac.joinGroup(AU.toList(CU.splitString(cf.getValue(), ",", false)));
			} catch (MyException e) {
				log.error("[{}] join group[{}] fail", ac.getId(), cf.getValue());
				return new ProcessorResult(false, this, null);
			}
		} else if (MessageConstant.MESSAGE_EVENT.AG_END_SESSION.name().equals(cf.getEvent())) {// 结束会话
			String sid = ac.getIdMap().get(cf.getCid());
			log.info("[{}] end the session[{}] of crm[{}]", ac.getId(), sid, cf.getCid());
			SessionWrapper session = ac.getSession(sid);
			//主持人
			if(session.getMastAgent() == null || session.getMastAgent().getId().equals(ac.getId())) {
				ac.archive(session, ARCHIVE_EVENT.AG_END_SESSION, null);
			} else {//非主持人结束会话要归档
				ac.archive(session, ARCHIVE_EVENT.AG_END_INVITE, ac.getNickname());
			}
			ac.endSession(sid, AG_SESSION_END_TYPE.BY_AGENT);
		} else if (MessageConstant.MESSAGE_EVENT.CHAT.name().equals(cf.getEvent())) {
			String sid = ac.getIdMap().get(cf.getCid());
			SessionWrapper session = ac.getSession(sid);
			if (session == null) {
				log.error("[{}] send chat message to cid[{}], but session[{}] is null", ac.getId(), cf.getCid(), sid);
				// client.sendBizMessage(new ErrorMessage(ac.getId()));
				return new ProcessorResult(false, this, null);
			}
			ac.archiveWithId(ac.getSession(sid), IDManager.getMid(), ARCHIVE_EVENT.CHAT.name(), cf.getValue(), cf.getMultiType(), null);
			session.message(client, cf.getValue(), cf.getValue2(), cf.getMultiType());
		} else if (MessageConstant.MESSAGE_EVENT.AG_RECEIVE_SESSION.name().equals(cf.getEvent())) {
			log.info("[{}] receive the session[{}]", ac.getId(), cf.getSid());
			SessionWrapper session = ac.getSession(cf.getSid());
			if (session != null) {
				ac.archive(session, ARCHIVE_EVENT.AG_RECEIVE_SESSION, null);
				ac.receiveSession(session);
			} else {
				log.error("[{}] receive the session[{}], but the is null", ac.getId(), cf.getId());
				return new ProcessorResult(false, this, null);
			}
		} else if (MessageConstant.MESSAGE_EVENT.AG_SEND_SATISFY.name().equals(cf.getEvent())) {
			log.info("[{}] send satisfy to [{}]", ac.getId(), cf.getCid());
			SessionWrapper session = ac.getSession(ac.getIdMap().get(cf.getCid()));
			if (session != null && session.getVisitor() != null && session.getSession().getSatisfyScore() == null) {
				// 访客在线并且未做过评价
				session.getSystem().sysSendNotify(cf.getCid(), MESSAGE_NOTIFY.AG_SEND_SATISFY, null);
			} else {
				return new ProcessorResult(false, this, null);
			}
		}
		return new ProcessorResult(true, this, new MessageReceipt(cf.getId(), true));
	}

	@Override
	public boolean isAcceptable(VisitorForm cf) {
		return MessageConstant.MESSAGE_EVENT.AG_CHANGE_STATUS.name().equals(cf.getEvent())
				|| MessageConstant.MESSAGE_EVENT.AG_JOIN_GROUP.name().equals(cf.getEvent())
				|| MessageConstant.MESSAGE_EVENT.AG_END_SESSION.name().equals(cf.getEvent())
				|| MessageConstant.MESSAGE_EVENT.CHAT.name().equals(cf.getEvent())
				|| MessageConstant.MESSAGE_EVENT.AG_SEND_SATISFY.name().equals(cf.getEvent())
				|| MessageConstant.MESSAGE_EVENT.AG_RECEIVE_SESSION.name().equals(cf.getEvent());
	}
}
