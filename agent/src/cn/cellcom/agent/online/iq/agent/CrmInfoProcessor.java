package cn.cellcom.agent.online.iq.agent;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.cellcom.agent.biz.TCrmBiz;
import cn.cellcom.agent.online.client.Client;
import cn.cellcom.agent.online.iq.AbstractChainedProcessor;
import cn.cellcom.agent.online.iq.Processor;
import cn.cellcom.agent.online.iq.ProcessorResult;
import cn.cellcom.agent.pojo.TCrm;
import cn.cellcom.agent.struts.form.VisitorForm;
import cn.cellcom.jar.biz.AbstractBiz;
import cn.cellcom.jar.util.MyException;

public class CrmInfoProcessor extends AbstractChainedProcessor {

	public static final String NAME = "crmInfo";

	private Logger log = LoggerFactory.getLogger(this.getClass());

	public CrmInfoProcessor(Processor next) {
		super(next);
	}

	@Override
	public ProcessorResult doProcess(Map<String, AbstractBiz> bizs, Client client, VisitorForm cf) {
		TCrmBiz cbiz = (TCrmBiz) bizs.get("cbiz");
		TCrm crm = null;
		try {
			crm = cbiz.getCrm(cf.getCid());
		} catch (MyException e) {
			log.error("[{}] get crmifo[{}]", client.getId(), cf.getCid());
			log.error("", e.getException());
			return new ProcessorResult(false, this, "");
		}
		return new ProcessorResult(true, this, crm);
	}

	@Override
	public boolean isAcceptable(VisitorForm message) {
		return NAME.equals(message.getEvent());
	}
}
