package cn.cellcom.agent.online.iq.agent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.cellcom.agent.biz.TGroupBiz;
import cn.cellcom.agent.biz.TSessionBiz;
import cn.cellcom.agent.online.client.AgentClient;
import cn.cellcom.agent.online.client.Client;
import cn.cellcom.agent.online.client.SessionManager;
import cn.cellcom.agent.online.iq.AbstractChainedProcessor;
import cn.cellcom.agent.online.iq.Processor;
import cn.cellcom.agent.online.iq.ProcessorResult;
import cn.cellcom.agent.online.message.BizMessage;
import cn.cellcom.agent.online.wrapper.GroupWrapper;
import cn.cellcom.agent.online.wrapper.SessionWrapper;
import cn.cellcom.agent.pojo.TAgentSession;
import cn.cellcom.agent.pojo.TCrm;
import cn.cellcom.agent.pojo.TGroup;
import cn.cellcom.agent.pojo.TSession;
import cn.cellcom.agent.struts.form.VisitorForm;
import cn.cellcom.jar.biz.AbstractBiz;
import cn.cellcom.jar.util.LogUtil;
import cn.cellcom.jar.util.MyException;

public class SessionListProcessor extends AbstractChainedProcessor {

	private Logger log = LoggerFactory.getLogger(this.getClass());

	public SessionListProcessor(Processor next) {
		super(next);
	}

	@Override
	public ProcessorResult doProcess(Map<String, AbstractBiz> bizs, Client client2, VisitorForm cf) {
		AgentClient client = (AgentClient) client2;
		List list = new ArrayList();
		try {
			TSessionBiz sbiz = (TSessionBiz) bizs.get("sbiz");
			TGroupBiz gbiz = (TGroupBiz) bizs.get("gbiz");
			if ("talking".equals(cf.getEvent())) {
				list = sbiz.getTalkingSessionOfAgent(client.getAgent());
			} else if ("history".equals(cf.getEvent())) {
				list = sbiz.getHistoryVisitorOfAgent(client.getAgent());
			}
			for (int i = 0; i < list.size(); i++) {
				Object[] arr = (Object[]) list.get(i);
				Map<String, Object> map = new HashMap<String, Object>();
				TSession tSession = (TSession) arr[0];
				TAgentSession agSession = (TAgentSession) arr[1];
				TGroup group = gbiz.getGroup(tSession.getRequestGroup());
				TCrm crm = (TCrm) arr[2];
				map.put("channel", tSession.getChannel());
				map.put("crm", crm);
				map.put("group", group);
				map.put("session", tSession);
				// 格式兼容分配会话的时候下发的消息类型
				BizMessage bm = new BizMessage();
				bm.setSid(tSession.getId());
				bm.setObject(map);
				list.set(i, bm);
				// 正在聊天的会话，那么就进入会话，
				if ("talking".equals(cf.getEvent())) {
					SessionWrapper session = client.getSession(tSession.getId());
					if (session == null) {
						session = SessionManager.getInstance().getPool(tSession.getPid()).create(tSession, client.getSetting());
					} else {
						log.info("[{}] have exist talking session[{}] in SessionManager", crm.getId(), session.getId());
					}
					session.joinAgsession(agSession, client, false);
					session.setCrm(crm);
					session.setGroup(new GroupWrapper(group));
				}
			}
		} catch (MyException e) {
			LogUtil.e(this.getClass(), "", e);
		}
		return new ProcessorResult(true, this, list);
	}

	@Override
	public boolean isAcceptable(VisitorForm cf) {
		return "talking".equals(cf.getEvent()) || "history".equals(cf.getEvent()) || "waiting".equals(cf.getEvent());
	}
}
