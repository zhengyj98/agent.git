package cn.cellcom.agent.online.iq.visitor;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.cellcom.agent.biz.TSessionBiz;
import cn.cellcom.agent.common.AgentConstant;
import cn.cellcom.agent.online.client.Client;
import cn.cellcom.agent.online.client.IDManager;
import cn.cellcom.agent.online.client.VisitorClient;
import cn.cellcom.agent.online.iq.AbstractChainedProcessor;
import cn.cellcom.agent.online.iq.Processor;
import cn.cellcom.agent.online.iq.ProcessorResult;
import cn.cellcom.agent.online.message.MessageConstant;
import cn.cellcom.agent.online.message.MessageConstant.ARCHIVE_EVENT;
import cn.cellcom.agent.online.message.MessageConstant.ARCHIVE_SHOW_POLICY;
import cn.cellcom.agent.online.message.MessageConstant.MULIT_TYPE;
import cn.cellcom.agent.online.wrapper.SessionWrapper;
import cn.cellcom.agent.pojo.TLw;
import cn.cellcom.agent.struts.form.VisitorForm;
import cn.cellcom.jar.biz.AbstractBiz;
import cn.cellcom.jar.util.MyException;

/**
 * 访客提交留言
 * 
 * @author zhengyj
 * 
 */
public class LwProcessor extends AbstractChainedProcessor {

	public static final String NAME = MessageConstant.MESSAGE_EVENT.LEAVEWORD.name();

	private Logger log = LoggerFactory.getLogger(this.getClass());

	public LwProcessor(Processor next) {
		super(next);
	}

	@Override
	public ProcessorResult doProcess(Map<String, AbstractBiz> bizs, Client client, VisitorForm cf) {
		VisitorClient vc = (VisitorClient) client;
		try {
			TSessionBiz sbiz = (TSessionBiz) bizs.get("sbiz");
			TLw lw = (TLw) cf.getObject();
			lw.setId(IDManager.getLid());
			lw.setCreateTime(System.currentTimeMillis());
			lw.setPid(cf.getPid());
			SessionWrapper ss = vc.getSessionByPid(cf.getPid());
			if (ss != null) {
				lw.setSid(ss.getId());
			}
			lw.setOrg(vc.getOrg());
			lw.setCrm(vc.getId());
			lw.setStatus(AgentConstant.NO);
			lw.setChannel(vc.getChannel());
			sbiz.getDao().mySave(lw);
			vc.archiveWithId(vc.getSessionByPid(cf.getPid()), lw.getId(), ARCHIVE_EVENT.LW.name(), null, MULIT_TYPE.TEXT.name(), null,
					ARCHIVE_SHOW_POLICY.ALL, cf.getPid());
		} catch (MyException e) {
			log.error("[{}] do leaveword[{}] for the pid[{}]", vc.getId(), cf.getValue(), cf.getPid(), e.getException());
			return new ProcessorResult(false, this, null);
		}
		log.info("[{}] do leaveword[{}] for the pid[{}]", vc.getId(), cf.getValue(), cf.getPid());
		return new ProcessorResult(true, this, null);
	}

	@Override
	public boolean isAcceptable(VisitorForm message) {
		return NAME.equals(message.getEvent());
	}
}
