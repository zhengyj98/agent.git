package cn.cellcom.agent.online.iq.visitor;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.cellcom.agent.online.client.Client;
import cn.cellcom.agent.online.client.VisitorClient;
import cn.cellcom.agent.online.iq.AbstractChainedProcessor;
import cn.cellcom.agent.online.iq.Processor;
import cn.cellcom.agent.online.iq.ProcessorResult;
import cn.cellcom.agent.online.wrapper.SessionWrapper;
import cn.cellcom.agent.struts.form.VisitorForm;
import cn.cellcom.jar.biz.AbstractBiz;

public class SettingProcessor extends AbstractChainedProcessor {

	private static final String NAME = "setting";

	private Logger log = LoggerFactory.getLogger(this.getClass());

	public SettingProcessor(Processor next) {
		super(next);
	}

	@Override
	public ProcessorResult doProcess(Map<String, AbstractBiz> bizs, Client client, VisitorForm cf) {
		SessionWrapper session = ((VisitorClient) client).getSessionByPid(cf.getPid());
		if(session == null) {
			return new ProcessorResult(false, this, null);
		}
		return new ProcessorResult(true, this, session.getSetting());
	}

	@Override
	public boolean isAcceptable(VisitorForm message) {
		return NAME.equals(message.getEvent());
	}
}
