package cn.cellcom.agent.online.iq.visitor;

import cn.cellcom.agent.online.iq.GetMessageProessor;
import cn.cellcom.agent.online.iq.Processor;
import cn.cellcom.agent.online.iq.agent.LwDetailProcessor;

public class VisitorProcessors {
	static Processor processor = null;

	static {
		GetMessageProessor unknown = new GetMessageProessor(null);
		ButtonProcessor buttonList = new ButtonProcessor(unknown);
		SettingProcessor setting = new SettingProcessor(buttonList);
		CrmInfoProcessor crm = new CrmInfoProcessor(setting);
		FaqProcessor faq = new FaqProcessor(crm);
		LogProcessor log = new LogProcessor(faq);
		GetGroupListProcessor groups = new GetGroupListProcessor(log);
		SatisfyProcessor satisfy = new SatisfyProcessor(groups);
		VisitorActionProcessor message = new VisitorActionProcessor(satisfy);
		LwProcessor leaveword = new LwProcessor(message);
		EnterGroupProcessor enterGroup = new EnterGroupProcessor(leaveword);
		LwDetailProcessor lwDetail = new LwDetailProcessor(enterGroup);
		processor = lwDetail;
	}

	private VisitorProcessors() {
	}

	public static synchronized Processor createProcessors() {
		return processor;
	}
}
