package cn.cellcom.agent.online.iq.visitor;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.cellcom.agent.common.AgentConstant;
import cn.cellcom.agent.online.client.Client;
import cn.cellcom.agent.online.handler.LogHandler;
import cn.cellcom.agent.online.iq.AbstractChainedProcessor;
import cn.cellcom.agent.online.iq.Processor;
import cn.cellcom.agent.online.iq.ProcessorResult;
import cn.cellcom.agent.struts.form.VisitorForm;
import cn.cellcom.jar.biz.AbstractBiz;

public class LogProcessor extends AbstractChainedProcessor {

	private static final String NAME = "log";

	private Logger log = LoggerFactory.getLogger(this.getClass());

	public LogProcessor(Processor next) {
		super(next);
	}

	@Override
	public ProcessorResult doProcess(Map<String, AbstractBiz> bizs, Client client, VisitorForm cf) {
		String pid = cf.getPid();
		List list = LogHandler.getLog(pid, client.getId(), null, cf.getStart(), cf.getCount(), AgentConstant.USER_TYPE_VISITOR);
		return new ProcessorResult(true, this, list);
	}

	@Override
	public boolean isAcceptable(VisitorForm message) {
		return NAME.equals(message.getEvent());
	}
}
