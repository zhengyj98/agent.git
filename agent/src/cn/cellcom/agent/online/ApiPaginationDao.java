package cn.cellcom.agent.online;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import cn.cellcom.agent.common.AgentConstant.ThirdCallback;
import cn.cellcom.agent.entity.OrderEntity;
import cn.cellcom.agent.util.ThirdCallbackUtils;
import cn.cellcom.jar.pagination.IPaginationDao;
import cn.cellcom.jar.util.MyException;

public class ApiPaginationDao implements IPaginationDao {

	@Override
	public int myCount(String hql, Object[] values) throws MyException {
		System.out.println("1");
		return 0;
	}

	@Override
	public String myGetAliasInHql(String hql) throws MyException {
		System.out.println("2");
		return null;
	}

	@Override
	public StringBuffer myGetCountHqlForPagination(String hql) throws MyException {
		return new StringBuffer();
	}

	@Override
	public StringBuffer myGetDataHqlForPagination(Class clazz) throws MyException {
		System.out.println("4");
		return null;
	}

	@Override
	public List myList(String hql, Object[] values, int currentPage, int pageSize) throws MyException {
		String[] arr = hql.split(",");
		ThirdCallback thirdCallback = ThirdCallback.valueOf(arr[0]);
		String tid = arr[1];
		String accessKey = arr[2];
		String callUrl = arr[3];
		Map<String, Object> params = (Map<String, Object>) values[0];
		if(params == null) {
			params = new HashMap<String, Object>();
		}
		params.put("currentPage", currentPage);
		params.put("pageSize", pageSize);
		Object object = values[1];
		String result = ThirdCallbackUtils.call(thirdCallback, tid, accessKey, callUrl, params, object);
		JSONArray jsonarr = JSONArray.fromObject(result);
		List<OrderEntity> orderList = new ArrayList<OrderEntity>();
		for (int i = 0; i < 5; i++) {
			OrderEntity e = new OrderEntity();
			e.setId(String.valueOf(i));
			e.setTime("2018-12-12 12:12:12");
			e.setTitle("大衣" + i);
			e.setDesc("这是一件非常好的大衣哦，测试用的数据哦哦哦哦哦");
			e.setOrderNo("2011111" + i);
			e.setPrice("123.52");
			e.setImgUrl("../file/load.do?method=get&id=CRM_DD9E37F0551B4D95A30AA861D78CC5E531&bizType=SETTING&fileName=jianyue.png");
			e.setHref("http://www.baidu.com");
			String json = JSONObject.fromObject(e).toString();
			e.setJsonString(json);
			orderList.add(e);
		}
		return orderList;
	}

	@Override
	public List myList(String hql, int currentPage, int pageSize) throws MyException {
		System.out.println("6");
		return null;
	}

}
