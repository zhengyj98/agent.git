package cn.cellcom.agent.online.handler;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import cn.cellcom.agent.common.AgentConstant;
import cn.cellcom.agent.j2ee.AgentListener;
import cn.cellcom.agent.online.message.ArchiveMessage;
import cn.cellcom.agent.online.message.MessageConstant;
import cn.cellcom.jar.util.LogUtil;
import cn.cellcom.jar.util.MyException;

/**
 * 历史消息处理器
 * 
 * @author zhengyj
 * 
 */
public class LogHandler {

	public static void archive(ArchiveMessage am) {
		try {
			AgentListener.getMgDao().mySave(am);
		} catch (MyException e) {
			LogUtil.e(LogHandler.class, "Archive Message error :" + am);
		} catch (Exception e) {
			LogUtil.e(LogHandler.class, "Archive Message error :" + am);
		}
	}

	/**
	 * 获取访客的历史消息
	 * 
	 * @param setting
	 * @param org
	 * @param pid
	 * @param cid
	 * @param start
	 * @param count
	 * @param showPolic
	 * @return
	 */
	public static List getLog(String pid, String cid, String sid, int start, int count, String userType) {
		if (start <= 0) {
			start = 1;
		}
		if (count <= 0) {
			count = 15;
		}
		String hql = null;
		Object[] values = null;
		if (AgentConstant.USER_TYPE_VISITOR.equals(userType)) {
			hql = "from " + ArchiveMessage.class.getCanonicalName() + " where (pid = ?) and (showPolicy = ?)";
			if(StringUtils.isNotBlank(cid)) {
				hql = hql + " and (cid = ?) ";
				values = new Object[] { pid, MessageConstant.ARCHIVE_SHOW_POLICY.ALL.name(), cid };
			}
			if(StringUtils.isNotBlank(sid)) {
				hql = hql + " and (sid = ?)";
				values = new Object[] { pid, MessageConstant.ARCHIVE_SHOW_POLICY.ALL.name(), sid };
			}
			hql = hql + " order by time desc";
		} else if (AgentConstant.USER_TYPE_AGENT.equals(userType)) {
			hql = "from " + ArchiveMessage.class.getCanonicalName()
					+ " where (pid = ?) and (showPolicy = ? or showPolicy = ?)";
			if(StringUtils.isNotBlank(cid)) {
				hql = hql + " and (cid = ?) ";
				values = new Object[] { pid, MessageConstant.ARCHIVE_SHOW_POLICY.ALL.name(),
						MessageConstant.ARCHIVE_SHOW_POLICY.MANAGER_AND_AGENT.name(), cid };
			}
			if(StringUtils.isNotBlank(sid)) {
				hql = hql + " and (sid = ?)";
				values = new Object[] { pid, MessageConstant.ARCHIVE_SHOW_POLICY.ALL.name(),
						MessageConstant.ARCHIVE_SHOW_POLICY.MANAGER_AND_AGENT.name(), sid };
			}
			hql = hql + " order by time desc";
		} else if (AgentConstant.USER_TYPE_MANAGER.equals(userType)) {
			hql = "from " + ArchiveMessage.class.getCanonicalName()
					+ " where (pid = ?) and (showPolicy = ? or showPolicy = ? or showPolicy = ?)";
			if(StringUtils.isNotBlank(cid)) {
				hql = hql + " and (cid = ?) ";
				values = new Object[] { pid, MessageConstant.ARCHIVE_SHOW_POLICY.ALL.name(),
						MessageConstant.ARCHIVE_SHOW_POLICY.MANAGER_AND_AGENT.name(), MessageConstant.ARCHIVE_SHOW_POLICY.ONLY_MANAGER.name(), cid };
			}
			if(StringUtils.isNotBlank(sid)) {
				hql = hql + " and (sid = ?)";
				values = new Object[] { pid, MessageConstant.ARCHIVE_SHOW_POLICY.ALL.name(),
						MessageConstant.ARCHIVE_SHOW_POLICY.MANAGER_AND_AGENT.name(), MessageConstant.ARCHIVE_SHOW_POLICY.ONLY_MANAGER.name(), sid };
			}
			hql = hql + " order by time desc";
		}

		try {
			return AgentListener.getMgPageDao().myList(hql, values, start, count);
		} catch (MyException e) {
			LogUtil.e(LogHandler.class, "", e);
		}
		return new ArrayList();
	}
}
