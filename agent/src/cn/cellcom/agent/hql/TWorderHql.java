package cn.cellcom.agent.hql;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForm;

import cn.cellcom.jar.dao.IHql;
import cn.cellcom.jar.logon.LogonSession;
import cn.cellcom.jar.util.AU;
import cn.cellcom.jar.util.MyException;
import cn.cellcom.agent.struts.form.TWorderForm;
import cn.cellcom.jar.util.form.FormUtil;
import cn.cellcom.jar.util.form.FormUtil.SpecialField;
import cn.cellcom.jar.util.form.FormUtil.SpecialValue;

public class TWorderHql implements IHql {

	private Log log = LogFactory.getLog(this.getClass());

	public String getHql(HttpServletRequest req, Object fm) {
		FormUtil fu = new FormUtil();
		try {
			StringBuffer sb = new StringBuffer("from TWorder as w, TCrm as c where w.crm=c.id ");

			// 以下读取session的值作为默认查询
			sb.append(" and w.pid = ? ");

			SpecialField sf = null;
			sf = fu.newSpecialField("w.sn", "w.sn", "like");
			sf.add("w.fagent", "w.fagent", "like");
			sf.add("w.status", "w.status", "=");
			sf.add("w.level", "w.level", "=");
			sf.add("w.createSource", "w.createSource", "=");

			sb.append(fu.fieldToString(fu.getInput((ActionForm) fm, new String[] { "ids" }), null, null, sf));
			return sb.append(" order by w.createTime desc").toString();
		} catch (MyException e) {
			log.error("分析获取hql语句失败", e.getException());
			return null;
		}
	}

	public Object[] getParaValue(HttpServletRequest req, Object fm) {
		LogonSession ls = new LogonSession(req);
		FormUtil fu = new FormUtil();
		try {
			SpecialValue sv = null;
			sv = fu.newSpecialValue("w.sn", "%", "%", null);
			sv.add("w.fagent", "%", "%", null);

			Object[] obj = new Object[] {};
			// 以下读取session的值作为默认查询
			obj = AU.append(obj, ls.getAttribute("attribute.pid"));
			return AU.append(obj, fu.valueToList(fu.getInput((ActionForm) fm, new String[] { "ids" }), sv).toArray());
		} catch (MyException e) {
			log.error("分析获取hql语句失败", e.getException());
			return null;
		}
	}
}
