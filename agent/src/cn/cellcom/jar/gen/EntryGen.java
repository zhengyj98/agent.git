package cn.cellcom.jar.gen;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * 该类将读取doc下面的config.txt以及xls配置文件，生成相应的代码、文件。 该类为必须执行的类
 * 
 * @author Administrator
 */
public class EntryGen {

	private static Log log = LogFactory.getLog(EntryGen.class);

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		TableGen.setWait(100);// 停顿时间
		TableGen.gen();
	}

}
