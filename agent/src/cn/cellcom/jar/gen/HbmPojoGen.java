package cn.cellcom.jar.gen;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * 生产hibernate的pojo文件
 * 
 * @author Administrator
 */
public class HbmPojoGen {

	private static Log log = LogFactory.getLog(HbmPojoGen.class);

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		PojoGen.gen();
	}

}
