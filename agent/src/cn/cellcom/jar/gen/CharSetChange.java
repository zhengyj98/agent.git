package cn.cellcom.jar.gen;

import cn.cellcom.jar.file.Gbk2Utf8;

/**
 * 对文件夹下面的所有文件进行gbk、utf8之间的转换
 * 
 * @author Administrator
 * 
 */
public class CharSetChange {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//Gbk2Utf8.gbk2Utf8("C:\\work\\biz\\agent\\src\\cn", "C:\\work\\biz\\agent\\src\\cn2");
		Gbk2Utf8.gbk2Utf8("C:/work/biz/agent/WebRoot/user", "C:/work/biz/agent/WebRoot/user2");

	}

}
