package cn.cellcom.jar.gen;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import cn.cellcom.jar.file.FileProcessor;
import cn.cellcom.jar.file.IFileProcessor;
import cn.cellcom.jar.file.IFileProcessor.MODE;
import cn.cellcom.jar.reflect.JavaBase;
import cn.cellcom.jar.util.AU;
import cn.cellcom.jar.util.MyException;

public class AddConfig {

	private static final String INTEGER = "Integer";
	private static final String STRING = "String";
	
	private static String libEntity = "C:/work/biz/agent/src/cn/cellcom/agent/entity/TSettingEntity.java";
	private static String jsp = "C:/work/biz/agent/WebRoot/base/";
	private static String beanutil = "C:/work/biz/agent/src/conf/cellcom/beanutil.properties";
	private static String AgentListener = "C:/work/biz/agent/src/cn/cellcom/agent/j2ee/AgentListener.java";
	
	

	static IFileProcessor ifp = new FileProcessor();

	public static void main(String[] args) throws MyException {
		/*jsp = jsp + "EnterSetting.jsp";
		List<String> list = new ArrayList<String>();
		list.add("-0,0,可以排队");
		list.add("-1,1,禁止排队");
		String defaultValue = "常见问题";
		doit("FIRST_MENU_TAG", "菜单页抬头", "菜单页抬头", "enterList", STRING, defaultValue , null);*/
		
		/*jsp = jsp + "AgentSetting.jsp";
		List<String> list = new ArrayList<String>();
		list.add("-0,0,不采取自助回复");
		list.add("-1,1,自助回复");
		doit("AGENT_TALKING_POLICY", "坐席会话自助服务", "坐席会话自助服务", "agentList", INTEGER, "0", list);*/
		
		jsp = jsp + "SelfSetting.jsp";
		List<String> list = new ArrayList<String>();
		list.add("0,0,本地机器人");
		list.add("1,1,图灵机器人");
		String defaultValue = "0";
		doit("ROBOT_TYPE", "机器人类型", "机器人类型", "selfList", STRING, defaultValue , list);
		
		/*jsp = jsp + "EnterSetting.jsp";
		String defaultValue  = "http://www.baidu.com";
		doit("SUPPORT_HOLDER", "土猪软件", "土猪软件", "enterList", STRING, defaultValue, null);*/
		
		/*jsp = jsp + "ExtendSetting.jsp";
		String defaultValue  = "";
		doit("CALL_METHOD", "回调动作", "回调动作", "extendList", STRING, defaultValue, null);*/
		
		/*List<String> list = new ArrayList<String>();
		list.add("-D,D,页面嵌入DIV模式");
		list.add("-P,P,弹出独立窗口模式");*/
		/*jsp = jsp + "UISetting.jsp";
		String defaultValue  = "600";
		doit("WINDOW_HEIGHT", "窗口高度", "窗口高度", "uiList", INTEGER, defaultValue, null);*/
		
		/*jsp = jsp + "SatisfySetting.jsp";
		List<String> list = new ArrayList<String>();
		list.add("-0,0,不允许输入评论");
		list.add("-1,1,允许输入评论");
		String defaultValue  = "0";
		doit("SATISFY_INPUT", "满意度评论", "满意度评论", "evaluateList", INTEGER, defaultValue, list);*/
	}

	private static void doit(String fieldName, String text, String detailText, String whichList, String fieldType, String defaultValue, List<String> beanList) throws MyException {
		String lower = JavaBase.toJavaName(fieldName, false);
		if(libEntity != null) {
			String temp = libEntity + ".temp";
			try {
				ifp.rename(libEntity, temp);
			} catch (MyException e) {
				e.getException().printStackTrace();
			}
			List<String> list = ifp.readList(temp);
			for (int i = 0; i < list.size(); i++) {
				String a = list.get(i);
				ifp.write(libEntity, a, MODE.WRITE_APPEND_MODE);
				if(a.contains("引用")) {
					String y = "public static final String " + fieldName + " = \"" + lower + "\";//" + text;
					ifp.write(libEntity, y, MODE.WRITE_APPEND_MODE);
				}
				
				if(StringUtils.isNotBlank(whichList) && a.contains("List<String> " + whichList)) {
					String y = whichList + ".add(" +fieldName  + ");";
					ifp.write(libEntity, y, MODE.WRITE_APPEND_MODE);
				}
				
				if(a.contains("变量定义")) {
					ifp.write(libEntity, "/**", MODE.WRITE_APPEND_MODE);
					ifp.write(libEntity, "*" + detailText, MODE.WRITE_APPEND_MODE);
					ifp.write(libEntity, "**/", MODE.WRITE_APPEND_MODE);
					String y = "private " + fieldType + " " + lower + ";";
					ifp.write(libEntity, y, MODE.WRITE_APPEND_MODE);
				}
				
				if (a.contains("setter")) {
					ifp.write(libEntity, "public " + fieldType + " get" + JavaBase.toJavaName(fieldName, true) + "() {", MODE.WRITE_APPEND_MODE);
					ifp.write(libEntity, "return " + lower + ";", MODE.WRITE_APPEND_MODE);
					ifp.write(libEntity, "}", MODE.WRITE_APPEND_MODE);
	
					ifp.write(libEntity, "public void set" + JavaBase.toJavaName(fieldName, true) + "(" + fieldType + " " + lower + ") {", MODE.WRITE_APPEND_MODE);
					ifp.write(libEntity, "this." + lower + " = " + lower + ";", MODE.WRITE_APPEND_MODE);
					ifp.write(libEntity, "}", MODE.WRITE_APPEND_MODE);
				}
			}
			ifp.delete(temp);
		}
		
		if(jsp != null) {
			String temp = jsp + ".temp";
			try {
				ifp.rename(jsp, temp);
			} catch (MyException e) {
				e.getException().printStackTrace();
			}
			List<String> list = ifp.readList(temp);
			for (int i = 0; i < list.size(); i++) {
				String a = list.get(i);
				ifp.write(jsp, a, MODE.WRITE_APPEND_MODE);
				if(a.contains("<table")) {
					ifp.write(jsp, "						<tr class='out-70' onMouseOver='this.className=\"over-70\"' onMouseOut='this.className=\"out-70\"'>", MODE.WRITE_APPEND_MODE);
					ifp.write(jsp, "							<td class='zw-txt'>" + text + "：</td>", MODE.WRITE_APPEND_MODE);
					ifp.write(jsp, "							<td class='zw-txt' align='left'>", MODE.WRITE_APPEND_MODE);
					if(AU.isNotBlank(beanList)) {
						ifp.write(jsp, "								<myjsp:select added='class=\"s-select\"' name='" + lower + "' id='" + lower + "' listname='" + lower + "List' value='${result." + lower + "}'/>", MODE.WRITE_APPEND_MODE);
					} else {
						//int类型则仅有一行
						int width = INTEGER.equals(fieldType) ? 30 : 400;
						String h = INTEGER.equals(fieldType) ? ";height:20px" : "";
						int line = INTEGER.equals(fieldType) ? 1 : 2;						
						ifp.write(jsp, "								<myjsp:sinput name='" + lower + "' id='" + lower + "' value='${result." + lower + "}' add=\"style='width:" + width + "px" + h + "'\" line='" + line + "'/>", MODE.WRITE_APPEND_MODE);
					}
					ifp.write(jsp, "							</td>", MODE.WRITE_APPEND_MODE);
					ifp.write(jsp, "						</tr>", MODE.WRITE_APPEND_MODE);
				}
			}
			ifp.delete(temp);
		}
		
		if(AU.isNotBlank(beanList)) {
			String key = "TSetting-" + lower;
			ifp.write(beanutil, "", MODE.WRITE_APPEND_MODE);
			for (int i = 0; i < beanList.size(); i++) {
				String one = beanList.get(i);
				ifp.write(beanutil, key + one, MODE.WRITE_APPEND_MODE);
			}
			ifp.write(AgentListener, "context.setAttribute(\"" + lower + "List\", BeanUtil.startWith(\"" + key + "\"));", MODE.WRITE_APPEND_MODE);
		}
		
		String sql = "insert into t_setting values('" + System.currentTimeMillis() + "','defaultorg','defaultorg',null,'" + lower + "','" + defaultValue + "',0)";
		System.out.println(sql);
	}

}
