/*
Navicat MySQL Data Transfer

Source Server         : 127.0.0.1
Source Server Version : 50527
Source Host           : localhost:3306
Source Database       : agent

Target Server Type    : MYSQL
Target Server Version : 50527
File Encoding         : 65001

Date: 2018-12-12 10:50:39
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `t_agent_session`
-- ----------------------------
DROP TABLE IF EXISTS `t_agent_session`;
CREATE TABLE `t_agent_session` (
  `id` varchar(100) NOT NULL,
  `org` varchar(100) NOT NULL,
  `pid` varchar(100) DEFAULT NULL,
  `channel` varchar(100) NOT NULL,
  `session` varchar(100) NOT NULL,
  `group_id` varchar(100) NOT NULL,
  `agent` varchar(100) NOT NULL,
  `agent_nickname` varchar(80) NOT NULL,
  `join_time` bigint(11) NOT NULL,
  `join_type` varchar(30) NOT NULL COMMENT '0表示首位进入\r\n1表示后续进入',
  `end_time` bigint(11) DEFAULT NULL,
  `end_type` varchar(30) DEFAULT NULL,
  `first_response_time` bigint(11) DEFAULT NULL,
  `message_count` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `indx_session` (`session`),
  KEY `indx_channel` (`channel`),
  KEY `indx_pid` (`pid`),
  KEY `indx_endType` (`end_type`),
  KEY `indx_joinType` (`join_type`),
  KEY `indx_groupId` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=gbk;

-- ----------------------------
-- Records of t_agent_session
-- ----------------------------
INSERT INTO `t_agent_session` VALUES ('AGSSN_05F5439C5FB1488C9ECCD655C666A9C678', 'jianyue', 'jianyue', '10001', 'SSN_BD7632A4415541D0B16D1DAF49ACE31F66', 'G_4FB3F910E4AD4516AA45C8CCBC1D21A345', '13631484267', 'jianyue', '1517994749472', 'RECEIVE_QUEUE', '1517994802531', 'BY_REPEAT', null, '0');
INSERT INTO `t_agent_session` VALUES ('AGSSN_0704BF9551C0424DAB6A0DB42F5DB99004', 'jianyue', 'jianyue', '10001', 'SSN_76CE0BADAA28417F98600DA4553924A952', 'G_4FB3F910E4AD4516AA45C8CCBC1D21A345', '13631484267', 'jianyue', '1524644230037', 'RECEIVE_QUEUE', '1529247240084', 'BY_AGENT', null, '0');
INSERT INTO `t_agent_session` VALUES ('AGSSN_0A6667BB814E45FB9182ABBC1C79B23705', 'jianyue', 'jianyue', '10001', 'SSN_8275258F9DA54440AB84644EDFDE2F5D71', 'G_4FB3F910E4AD4516AA45C8CCBC1D21A345', '13631484267', 'jianyue', '1519458612950', 'RECEIVE_QUEUE', '1519459466405', 'BY_AGENT', null, '0');
INSERT INTO `t_agent_session` VALUES ('AGSSN_0BC9E76466DF42288D7A97B6C2F7E66C31', 'jianyue', 'jianyue', 'Q33845916', 'SSN_47CCDEF4669E41DE991BA09F31FB621D13', 'G_4FB3F910E4AD4516AA45C8CCBC1D21A345', '13631484267', 'jianyue', '1528866998893', 'RECEIVE_QUEUE', '1529247236109', 'BY_AGENT', null, '0');
INSERT INTO `t_agent_session` VALUES ('AGSSN_204DF347525043BFAE68293CCDB4BAF492', 'jianyue', 'jianyue', '10001', 'SSN_462920E4F00E4DF18973E8FF25F9361458', 'G_4FB3F910E4AD4516AA45C8CCBC1D21A345', '13631484267', 'jianyue', '1517994802538', 'RECEIVE_QUEUE', '1519459471537', 'BY_AGENT', null, '0');
INSERT INTO `t_agent_session` VALUES ('AGSSN_2F16209FFDAA45619C8E5F206561706F55', 'jianyue', 'jianyue', 'Q453781573', 'SSN_AD85624872C141FBAEF4C06599EE880901', 'G_4FB3F910E4AD4516AA45C8CCBC1D21A345', '13631484267', 'jianyue', '1528889800863', 'RECEIVE_QUEUE', '1529247238036', 'BY_AGENT', null, '0');
INSERT INTO `t_agent_session` VALUES ('AGSSN_38EAB194B925444ABF9B27130155399B08', 'jianyue', 'jianyue', '10001', 'SSN_89A5F537B58A499CAFFCD51AC9EF598792', 'G_4FB3F910E4AD4516AA45C8CCBC1D21A345', '13631484267', 'jianyue', '1519441674459', 'RECEIVE_QUEUE', '1519459469307', 'BY_AGENT', null, '0');
INSERT INTO `t_agent_session` VALUES ('AGSSN_3E14F712C53B4D1EA157D3D72543002B51', 'jianyue', 'jianyue', '10001', 'SSN_A080A143716A4F63B514611E253D8B8026', 'G_4FB3F910E4AD4516AA45C8CCBC1D21A345', '13631484267', 'jianyue', '1520651253284', 'RECEIVE_QUEUE', '1529247234093', 'BY_AGENT', null, '0');
INSERT INTO `t_agent_session` VALUES ('AGSSN_420BEB003BBF458997A28D5B6884225F61', 'jianyue', 'jianyue', '10001', 'SSN_96D709D27A8C422982F7BB8707585F9063', 'G_4FB3F910E4AD4516AA45C8CCBC1D21A345', '13631484267', 'jianyue', '1515912860238', 'RECEIVE_QUEUE', '1519459473765', 'BY_AGENT', null, '0');
INSERT INTO `t_agent_session` VALUES ('AGSSN_878E7EE61D9C4C60B8239FE2687C5D7D53', 'jianyue', 'jianyue', 'Q33845916', 'SSN_F7A4790F184F42F0887E57A9094AD0D635', 'G_4FB3F910E4AD4516AA45C8CCBC1D21A345', '13631484267', 'jianyue', '1528866812638', 'RECEIVE_QUEUE', '1528866998890', 'BY_REPEAT', null, '0');
INSERT INTO `t_agent_session` VALUES ('AGSSN_8A82DD3F1C01427084C9A110276345FC23', 'jianyue', 'jianyue', 'Q33845916', 'SSN_6637CF820FDE440EB3A1A9C35C398BB029', 'G_4FB3F910E4AD4516AA45C8CCBC1D21A345', '13631484267', 'jianyue', '1528866774260', 'RECEIVE_QUEUE', '1528866812635', 'BY_REPEAT', '1528866785698', '1');
INSERT INTO `t_agent_session` VALUES ('AGSSN_8FF0633C42D845C8BF4720AD2C28294619', 'jianyue', 'jianyue', '10001', 'SSN_13367159BA6349F288F834144001759810', 'G_4FB3F910E4AD4516AA45C8CCBC1D21A345', '13631484267', 'jianyue', '1529247196670', 'RECEIVE_QUEUE', '1529247220556', 'BY_AGENT', null, '0');
INSERT INTO `t_agent_session` VALUES ('AGSSN_BB73776754BE40EF938A50C8368F33D704', 'jianyue', 'jianyue', 'Q453781573', 'SSN_6D2BF8370FCA4218A401F3BF36CB99C271', 'G_4FB3F910E4AD4516AA45C8CCBC1D21A345', '13631484267', 'jianyue', '1529247025603', 'RECEIVE_QUEUE', '1529247224949', 'BY_AGENT', null, '0');
INSERT INTO `t_agent_session` VALUES ('AGSSN_EA5DC31CA3CB4490809F22D70DD67F4E18', 'jianyue', 'jianyue', '10001', 'SSN_953F1C92923A4483A2EB6447015AD1EC16', 'G_4FB3F910E4AD4516AA45C8CCBC1D21A345', '13631484267', 'jianyue', '1519459488338', 'RECEIVE_QUEUE', '1529247230485', 'BY_AGENT', null, '0');
INSERT INTO `t_agent_session` VALUES ('AGSSN_EEA92FACA4034014B17A3E7898D8262343', 'jianyue', 'jianyue', 'Q453781573', 'SSN_A63885A2130F48829FCBAC7F57C8DF5706', 'G_4FB3F910E4AD4516AA45C8CCBC1D21A345', '13631484267', 'jianyue', '1529026634983', 'RECEIVE_QUEUE', '1529247228037', 'BY_AGENT', null, '0');

-- ----------------------------
-- Table structure for `t_button`
-- ----------------------------
DROP TABLE IF EXISTS `t_button`;
CREATE TABLE `t_button` (
  `id` varchar(100) NOT NULL DEFAULT '',
  `org` varchar(100) CHARACTER SET gbk NOT NULL,
  `pid` varchar(100) CHARACTER SET gbk NOT NULL,
  `seq` int(11) DEFAULT NULL,
  `img` varchar(80) DEFAULT NULL,
  `href` varchar(80) DEFAULT NULL,
  `name` varchar(80) DEFAULT NULL,
  `title` varchar(80) DEFAULT NULL,
  `memo` varchar(200) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `subject` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=gb2312;

-- ----------------------------
-- Records of t_button
-- ----------------------------
INSERT INTO `t_button` VALUES ('9D75DA2A6B0E4EE08960D32989D498DA29', 'jianyue', 'jianyue', '1', 'jianyue.png', 'http://www.jianyuekefu.com', '简约客服', '这个按钮可以跳转到简约客服', '注册时产生的测试按钮', null, 'defaultorg');
INSERT INTO `t_button` VALUES ('C72E61D4874B4E43BE6347B58CF48D8D01', 'guangzhouqili04644', 'guangzhouqili04644', '1', 'jianyue.png', 'http://www.jianyuekefu.com', '简约客服', '这个按钮可以跳转到简约客服', '注册时产生的测试按钮', null, 'defaultorg');

-- ----------------------------
-- Table structure for `t_channel`
-- ----------------------------
DROP TABLE IF EXISTS `t_channel`;
CREATE TABLE `t_channel` (
  `id` varchar(100) NOT NULL,
  `org` varchar(100) NOT NULL,
  `pid` varchar(100) NOT NULL,
  `no` varchar(80) NOT NULL,
  `name` varchar(80) NOT NULL,
  `memo` varchar(200) DEFAULT NULL,
  `type` varchar(10) NOT NULL,
  `insert_time` datetime NOT NULL,
  `wc_appid` varchar(100) DEFAULT NULL,
  `wc_secret` varchar(100) DEFAULT NULL,
  `web_url` varchar(100) DEFAULT NULL,
  `qq` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=gbk;

-- ----------------------------
-- Records of t_channel
-- ----------------------------
INSERT INTO `t_channel` VALUES ('39B4CDD11E96483290E2565F8C575EE887', 'jianyue', 'jianyue', 'Q453781573', '简约客服', '', 'QQ', '2018-06-13 19:23:52', null, null, null, '453781573');
INSERT INTO `t_channel` VALUES ('D360DC2CF3C94F82AD9DCAE2C5B14E2079', 'jianyue', 'jianyue', '10001', '官方渠道', '默认渠道', 'WEB', '2018-01-14 14:53:52', null, null, null, null);
INSERT INTO `t_channel` VALUES ('FB0CCD37C5D846B7874A521EDCD3873147', 'jianyue', 'jianyue', 'Q33845916', '一头猪', '', 'QQ', '2018-06-13 10:09:25', null, null, null, '33845916');
INSERT INTO `t_channel` VALUES ('FDD845081205457AAC1CBDC0BB04882372', 'guangzhouqili04644', 'guangzhouqili04644', '10001', '官方渠道', '默认渠道', 'WEB', '2018-01-15 10:47:31', null, null, null, null);

-- ----------------------------
-- Table structure for `t_channel_router`
-- ----------------------------
DROP TABLE IF EXISTS `t_channel_router`;
CREATE TABLE `t_channel_router` (
  `id` varchar(100) CHARACTER SET gbk NOT NULL,
  `org` varchar(100) CHARACTER SET gbk NOT NULL,
  `pid` varchar(100) CHARACTER SET gbk NOT NULL,
  `status` varchar(1) CHARACTER SET gbk NOT NULL,
  `channel` varchar(100) CHARACTER SET gbk NOT NULL,
  `route_group` varchar(100) CHARACTER SET gbk NOT NULL,
  `route_agent` varchar(100) CHARACTER SET gbk DEFAULT NULL,
  `create_time` bigint(20) NOT NULL,
  `in_support_group` varchar(100) CHARACTER SET gbk DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=gb2312;

-- ----------------------------
-- Records of t_channel_router
-- ----------------------------

-- ----------------------------
-- Table structure for `t_crm`
-- ----------------------------
DROP TABLE IF EXISTS `t_crm`;
CREATE TABLE `t_crm` (
  `id` varchar(100) NOT NULL,
  `org` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `head` varchar(100) DEFAULT NULL,
  `phone` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `qq` varchar(100) DEFAULT NULL,
  `sex` int(11) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `province` varchar(100) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `memo` varchar(100) DEFAULT NULL,
  `tid` varchar(100) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `create_time` bigint(20) NOT NULL,
  `extend1` varchar(1000) DEFAULT NULL,
  `extend2` varchar(1000) DEFAULT NULL,
  `extend3` varchar(1000) DEFAULT NULL,
  `extend4` varchar(1000) DEFAULT NULL,
  `extend5` varchar(1000) DEFAULT NULL,
  `extend6` varchar(1000) DEFAULT NULL,
  `extend7` varchar(1000) DEFAULT NULL,
  `extend8` varchar(1000) DEFAULT NULL,
  `extend9` varchar(1000) DEFAULT NULL,
  `extend10` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `indx_phone` (`phone`),
  KEY `indx_name` (`name`),
  KEY `indx_tid` (`tid`)
) ENGINE=InnoDB DEFAULT CHARSET=gbk;

-- ----------------------------
-- Records of t_crm
-- ----------------------------
INSERT INTO `t_crm` VALUES ('241282860', 'jianyue', '3', null, null, null, '1', '0', null, '1', null, null, null, null, null, '0', '1528865489497', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_crm` VALUES ('2502423763', 'jianyue', '2', null, null, null, '1', '0', null, '1', null, null, null, null, null, '0', '1528857577040', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_crm` VALUES ('2603034262', 'jianyue', '7', null, null, null, '1', '0', null, '1', null, null, null, null, null, '0', '1528957601895', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_crm` VALUES ('2679020244', 'jianyue', '5', null, null, null, '1', '0', null, '1', null, null, null, null, null, '0', '1528889133270', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_crm` VALUES ('3101502925', 'jianyue', '4', null, null, null, '1', '0', null, '1', null, null, null, null, null, '0', '1528888794089', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_crm` VALUES ('4035724768', 'jianyue', '1', null, null, null, '1', '0', null, '1', null, null, null, null, null, '0', '1528856008722', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_crm` VALUES ('4149582464', 'jianyue', '9', null, null, null, '1', '0', null, '1', null, null, null, null, null, '0', '1529245724469', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_crm` VALUES ('632413544', 'jianyue', '8', null, null, null, '1', '0', null, '1', null, null, null, null, null, '0', '1529025678623', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_crm` VALUES ('CRM_0C9492D35DD04F18BB232CAA39324C7181', 'jianyue', '1', null, null, null, '1', '0', null, '1', null, null, null, null, null, '0', '1515912841211', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_crm` VALUES ('CRM_12471520579C4CD6B99E35061D65B2C264', 'jianyue', '6', null, null, null, '1', '0', null, '1', null, null, null, null, null, '0', '1528957510664', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_crm` VALUES ('CRM_254FAC44689E43288F37E157C627298613', 'jianyue', '15', null, null, null, '1', '0', null, '1', null, null, null, null, null, '0', '1526458115694', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_crm` VALUES ('CRM_35ECA963A2094C769723A50F7049E9E539', 'jianyue', '12', null, null, null, '1', '0', null, '1', null, null, null, null, null, '0', '1524645468200', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_crm` VALUES ('CRM_3DE342D1424C4BA2BF105514A11EFF6679', 'jianyue', '10', null, null, null, '1', '0', null, '1', null, null, null, null, null, '0', '1520651245654', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_crm` VALUES ('CRM_41C35604AB67449A9971453627A0C04F94', 'jianyue', '10', null, null, null, '1', '0', null, '1', null, null, null, null, null, '0', '1544182269636', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_crm` VALUES ('CRM_592BDAFF9B4E479BA58ED5642A517A8D80', 'jianyue', '9', null, null, null, '1', '0', null, '1', null, null, null, null, null, '0', '1519458339822', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_crm` VALUES ('CRM_6AB2A61C88254E5E855019F9D6DE5AFB49', 'jianyue', '管理员', null, '13631484267', null, '1', '0', null, '1', null, null, null, null, '13631484267', '1', '1517394970278', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_crm` VALUES ('CRM_73E8CC8E825E4C288C52A4DC8B1F66C515', 'jianyue', '肥郭', null, null, null, '1', '0', null, '1', null, null, null, null, null, '0', '1517994565239', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_crm` VALUES ('CRM_96A60FDB6B6E4AA39A87E59E00FF8BDB01', 'jianyue', '2', null, null, null, '1', '0', null, '1', null, null, null, null, null, '0', '1515982383433', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_crm` VALUES ('CRM_A54F0EA373FF495F954102B2364299E031', 'jianyue', '6', null, null, null, '1', '0', null, '1', null, null, null, null, null, '0', '1517469028713', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_crm` VALUES ('CRM_A9DFA9ECB5CF403E88C8355F06EDB10816', 'jianyue', '5', null, null, null, '1', '0', null, '1', null, null, null, null, null, '0', '1517468969185', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_crm` VALUES ('CRM_D83F31B1996E4A97BCF0468F003AA3CC30', 'jianyue', '8', null, null, null, '1', '0', null, '1', null, null, null, null, null, '0', '1519441639519', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_crm` VALUES ('CRM_D99A51C133684DEAA19C2E986DEEC46B03', 'jianyue', '3', null, null, null, '1', '0', null, '1', null, null, null, null, null, '0', '1517394966596', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_crm` VALUES ('CRM_F3D021279C07443EBA58D3164903CA1E29', 'jianyue', '11', null, null, null, '1', '0', null, '1', null, null, null, null, null, '0', '1524644222513', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_crm` VALUES ('CRM_F819BE6907E6478FB0ED3A0F1CA44A2157', 'guangzhouqili04644', '1', null, null, null, '1', '0', null, '1', null, null, null, null, null, '0', '1515984471486', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_crm` VALUES ('CRM_FD92C3F638024ED79CAB6A4710E3CC1B87', 'jianyue', '13', null, null, null, '1', '0', null, '1', null, null, null, null, null, '0', '1524922290481', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_crm` VALUES ('CRM_FF61E9A20D5B404B97ED9B0B137746EE28', 'jianyue', '14', null, null, null, '1', '0', null, '1', null, null, null, null, null, '0', '1525053622375', null, null, null, null, null, null, null, null, null, null);

-- ----------------------------
-- Table structure for `t_crm_field`
-- ----------------------------
DROP TABLE IF EXISTS `t_crm_field`;
CREATE TABLE `t_crm_field` (
  `id` varchar(80) NOT NULL,
  `org` varchar(80) NOT NULL,
  `fname` varchar(80) NOT NULL,
  `fshow_name` varchar(80) NOT NULL,
  `ftype` varchar(6) DEFAULT NULL,
  `fsystem` int(6) DEFAULT NULL,
  `fsearch` int(6) DEFAULT NULL,
  `fenable` int(6) DEFAULT NULL,
  `frequied` int(6) DEFAULT NULL,
  `fmodify` int(6) DEFAULT NULL,
  `flist` int(6) DEFAULT NULL,
  `create_time` bigint(20) NOT NULL,
  `ind` int(6) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=gb2312;

-- ----------------------------
-- Records of t_crm_field
-- ----------------------------
INSERT INTO `t_crm_field` VALUES ('CF_0123A6B8F3EF4FFF8FA795AB05E1AC8585', 'guangzhouqili04644', 'status', '状态', 'S', '1', '1', '1', '0', '1', '1', '1515984451190', '5');
INSERT INTO `t_crm_field` VALUES ('CF_04817A9032174D85BAAB750C2AE3441415', 'jianyue', 'head', '头像', 'H', '1', '0', '1', '0', '1', '0', '1515912832483', '13');
INSERT INTO `t_crm_field` VALUES ('CF_06FD57E8812B470893D74C1C53A4FF2104', 'guangzhouqili04644', 'extend1', '扩展1', 'I', '1', '0', '0', '0', '0', '0', '1515984451191', '14');
INSERT INTO `t_crm_field` VALUES ('CF_1266B06374864013BC4899DF24834FBA49', 'jianyue', 'level', '等级', 'S', '1', '1', '1', '0', '1', '1', '1515912832483', '4');
INSERT INTO `t_crm_field` VALUES ('CF_1533FFC5EDB14C29ADC7389DE9AF621212', 'jianyue', 'memo', '备注', 'T', '1', '1', '1', '0', '1', '1', '1515912832483', '11');
INSERT INTO `t_crm_field` VALUES ('CF_1695C78788944FAD887C029B781F810469', 'guangzhouqili04644', 'extend5', '扩展5', 'I', '1', '0', '0', '0', '0', '0', '1515984451191', '18');
INSERT INTO `t_crm_field` VALUES ('CF_199FD686B1844616AC9076B1A2CAA48E07', 'jianyue', 'extend8', '扩展8', 'I', '1', '0', '0', '0', '0', '0', '1515912832484', '21');
INSERT INTO `t_crm_field` VALUES ('CF_1DD0421FFBBD4ACCB4E316BD049EF24296', 'guangzhouqili04644', 'province', '省份', 'S', '1', '1', '1', '0', '1', '1', '1515984451191', '9');
INSERT INTO `t_crm_field` VALUES ('CF_2381008FDEBB49D8967A7DE9B4E6824A80', 'jianyue', 'sex', '性别', 'S', '1', '1', '1', '0', '1', '1', '1515912832483', '7');
INSERT INTO `t_crm_field` VALUES ('CF_2A7F1E77D1BD4762AD6C536BA67A67FF42', 'jianyue', 'tid', 'TID', 'I', '1', '1', '1', '0', '0', '1', '1515912832482', '0');
INSERT INTO `t_crm_field` VALUES ('CF_2D524CA7A9AE4D0090EC52CE9955CF1D46', 'jianyue', 'qq', 'QQ', 'I', '1', '1', '1', '0', '1', '1', '1515912832483', '6');
INSERT INTO `t_crm_field` VALUES ('CF_2DBF7DD3804B403BA0310D62BC34F00663', 'jianyue', 'company', '企业', 'I', '1', '1', '1', '0', '1', '1', '1515912832483', '8');
INSERT INTO `t_crm_field` VALUES ('CF_3B0BC744227247A2979A4AE418FC78B959', 'jianyue', 'extend5', '扩展5', 'I', '1', '0', '0', '0', '0', '0', '1515912832484', '18');
INSERT INTO `t_crm_field` VALUES ('CF_3B21DCD3F56C4383A2FEB65DCC85A7AB19', 'jianyue', 'status', '状态', 'S', '1', '1', '1', '0', '1', '1', '1515912832483', '5');
INSERT INTO `t_crm_field` VALUES ('CF_3CC395F056434A8E86B9527C693DE93101', 'guangzhouqili04644', 'tid', 'TID', 'I', '1', '1', '1', '0', '0', '1', '1515984451190', '0');
INSERT INTO `t_crm_field` VALUES ('CF_44A3A9B640A6448E88B1F6C4CCA930D370', 'guangzhouqili04644', 'email', '邮箱', 'I', '1', '1', '1', '0', '1', '1', '1515984451190', '3');
INSERT INTO `t_crm_field` VALUES ('CF_454E21A7431346D7B74BD5BC55AA278085', 'jianyue', 'extend9', '扩展9', 'I', '1', '0', '0', '0', '0', '0', '1515912832484', '22');
INSERT INTO `t_crm_field` VALUES ('CF_4BBF303746BC43AB9C2E3ED756792F4289', 'guangzhouqili04644', 'sex', '性别', 'S', '1', '1', '1', '0', '1', '1', '1515984451191', '7');
INSERT INTO `t_crm_field` VALUES ('CF_4FEAA9227B4D42F09D6691FD87F7EB0627', 'jianyue', 'extend7', '扩展7', 'I', '1', '0', '0', '0', '0', '0', '1515912832484', '20');
INSERT INTO `t_crm_field` VALUES ('CF_5029E847EF74467082C9053DBA9D7BE421', 'guangzhouqili04644', 'extend6', '扩展6', 'I', '1', '0', '0', '0', '0', '0', '1515984451191', '19');
INSERT INTO `t_crm_field` VALUES ('CF_549189B98C3844008CE7BA1E02A6CE3317', 'guangzhouqili04644', 'phone', '电话', 'I', '1', '1', '1', '0', '1', '1', '1515984451190', '2');
INSERT INTO `t_crm_field` VALUES ('CF_5C04D2A725B049E984474AB2B40B055958', 'jianyue', 'email', '邮箱', 'I', '1', '1', '1', '0', '1', '1', '1515912832482', '3');
INSERT INTO `t_crm_field` VALUES ('CF_632A52E45CE644B7A5AA6A3D7795DF6478', 'jianyue', 'extend3', '扩展3', 'I', '1', '0', '0', '0', '0', '0', '1515912832483', '16');
INSERT INTO `t_crm_field` VALUES ('CF_69897FB3362B4B339CF5D8EDA95F98AD90', 'guangzhouqili04644', 'extend4', '扩展4', 'I', '1', '0', '0', '0', '0', '0', '1515984451191', '17');
INSERT INTO `t_crm_field` VALUES ('CF_69F44F08B0214A71A16760D02D01FB1219', 'jianyue', 'phone', '电话', 'I', '1', '1', '1', '0', '1', '1', '1515912832482', '2');
INSERT INTO `t_crm_field` VALUES ('CF_6CC45BD8008D40C799AA07630A21B1A206', 'guangzhouqili04644', 'type', '类型', 'S', '1', '1', '1', '0', '0', '1', '1515984451191', '12');
INSERT INTO `t_crm_field` VALUES ('CF_6CCE2DE3BABD46B69E4CDD22E81B165385', 'jianyue', 'extend2', '扩展2', 'I', '1', '0', '0', '0', '0', '0', '1515912832483', '15');
INSERT INTO `t_crm_field` VALUES ('CF_711DE61840364B8B8E22072F5D474A0F55', 'guangzhouqili04644', 'id', 'ID', 'H', '1', '0', '1', '0', '1', '0', '1515984451190', '0');
INSERT INTO `t_crm_field` VALUES ('CF_72F0AEE80CE4476298A74CDB3086838C40', 'jianyue', 'province', '省份', 'S', '1', '1', '1', '0', '1', '1', '1515912832483', '9');
INSERT INTO `t_crm_field` VALUES ('CF_74D1DE6A98A14B39AA71D379D4B6AE2518', 'jianyue', 'city', '城市', 'S', '1', '1', '1', '0', '1', '1', '1515912832483', '10');
INSERT INTO `t_crm_field` VALUES ('CF_76649DA572EE4B0693C41BDD025628E177', 'guangzhouqili04644', 'qq', 'QQ', 'I', '1', '1', '1', '0', '1', '1', '1515984451190', '6');
INSERT INTO `t_crm_field` VALUES ('CF_8D239C9481714A59B5BEADF99AE7846D02', 'guangzhouqili04644', 'level', '等级', 'S', '1', '1', '1', '0', '1', '1', '1515984451190', '4');
INSERT INTO `t_crm_field` VALUES ('CF_8DDF569A5F3C4329A27D1E4D7898D56572', 'jianyue', 'extend6', '扩展6', 'I', '1', '0', '0', '0', '0', '0', '1515912832484', '19');
INSERT INTO `t_crm_field` VALUES ('CF_8DE27DA010694641AF691372C6173F8222', 'jianyue', 'extend1', '扩展1', 'I', '1', '0', '0', '0', '0', '0', '1515912832483', '14');
INSERT INTO `t_crm_field` VALUES ('CF_95C1B164FCEB498EB34E34643723919976', 'jianyue', 'extend10', '扩展10', 'I', '1', '0', '0', '0', '0', '0', '1515912832484', '23');
INSERT INTO `t_crm_field` VALUES ('CF_A600372D2FB049B3B9F89A777AFF9B2186', 'guangzhouqili04644', 'city', '城市', 'S', '1', '1', '1', '0', '1', '1', '1515984451191', '10');
INSERT INTO `t_crm_field` VALUES ('CF_AD8D5065D43F455686BD1BB2C79BC66101', 'guangzhouqili04644', 'company', '企业', 'I', '1', '1', '1', '0', '1', '1', '1515984451191', '8');
INSERT INTO `t_crm_field` VALUES ('CF_BADD31C8059C43B1BCB61408CAC5A59815', 'guangzhouqili04644', 'extend2', '扩展2', 'I', '1', '0', '0', '0', '0', '0', '1515984451191', '15');
INSERT INTO `t_crm_field` VALUES ('CF_BD9847B9E2BA4E349A066879BC345CE343', 'guangzhouqili04644', 'extend9', '扩展9', 'I', '1', '0', '0', '0', '0', '0', '1515984451192', '22');
INSERT INTO `t_crm_field` VALUES ('CF_CBE5594571EE4049B93706406F696EFD10', 'jianyue', 'extend4', '扩展4', 'I', '1', '0', '0', '0', '0', '0', '1515912832484', '17');
INSERT INTO `t_crm_field` VALUES ('CF_D1602AF8B5A8496A8DE6BD540A43DC0D96', 'jianyue', 'name', '姓名', 'I', '1', '1', '1', '1', '1', '1', '1515912832482', '1');
INSERT INTO `t_crm_field` VALUES ('CF_D675F2A2B64A483DA2A8431AA71347A452', 'guangzhouqili04644', 'memo', '备注', 'T', '1', '1', '1', '0', '1', '1', '1515984451191', '11');
INSERT INTO `t_crm_field` VALUES ('CF_DFC90102747C4210846524D0CCCFC96346', 'guangzhouqili04644', 'extend7', '扩展7', 'I', '1', '0', '0', '0', '0', '0', '1515984451191', '20');
INSERT INTO `t_crm_field` VALUES ('CF_E199252F6F6344B580E1F2F44F74F1E882', 'jianyue', 'type', '类型', 'S', '1', '1', '1', '0', '0', '1', '1515912832483', '12');
INSERT INTO `t_crm_field` VALUES ('CF_E96A9E6E0E7A4164B63B9C1EE28EBAFD98', 'guangzhouqili04644', 'extend8', '扩展8', 'I', '1', '0', '0', '0', '0', '0', '1515984451192', '21');
INSERT INTO `t_crm_field` VALUES ('CF_EB12C38CAA884A5CB6705BBA972E2EFB75', 'guangzhouqili04644', 'head', '头像', 'H', '1', '0', '1', '0', '1', '0', '1515984451191', '13');
INSERT INTO `t_crm_field` VALUES ('CF_EBB70C47768B407AA616877831D38FCE17', 'jianyue', 'id', 'ID', 'H', '1', '0', '1', '0', '1', '0', '1515912832482', '0');
INSERT INTO `t_crm_field` VALUES ('CF_F7216155DBC045ABAB5CB657D35EEBBE58', 'guangzhouqili04644', 'extend10', '扩展10', 'I', '1', '0', '0', '0', '0', '0', '1515984451192', '23');
INSERT INTO `t_crm_field` VALUES ('CF_F9FD7CAEA6214960B2D83FB6B2FA599073', 'guangzhouqili04644', 'extend3', '扩展3', 'I', '1', '0', '0', '0', '0', '0', '1515984451191', '16');
INSERT INTO `t_crm_field` VALUES ('CF_FFC43664C4D649E6A0166815F3DD265561', 'guangzhouqili04644', 'name', '姓名', 'I', '1', '1', '1', '1', '1', '1', '1515984451190', '1');

-- ----------------------------
-- Table structure for `t_crm_field_option`
-- ----------------------------
DROP TABLE IF EXISTS `t_crm_field_option`;
CREATE TABLE `t_crm_field_option` (
  `id` varchar(100) NOT NULL,
  `org` varchar(100) DEFAULT NULL,
  `field_fname` varchar(100) DEFAULT NULL,
  `option_text` varchar(256) DEFAULT NULL,
  `option_value` varchar(150) DEFAULT NULL,
  `create_time` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=gb2312;

-- ----------------------------
-- Records of t_crm_field_option
-- ----------------------------
INSERT INTO `t_crm_field_option` VALUES ('01308B6928394418AFB3CA30B390E30768', 'jianyue', 'level', '红名单', '4', '1515912832591');
INSERT INTO `t_crm_field_option` VALUES ('08A6D0D6B4CF47D2BB91F816249A585C20', 'jianyue', 'type', '主动注册', '3', '1515912832591');
INSERT INTO `t_crm_field_option` VALUES ('0F80D1716D224925BFBFCCC708FA4AF994', 'guangzhouqili04644', 'level', '普通', '1', '1515984451214');
INSERT INTO `t_crm_field_option` VALUES ('141B44F4B33C4AABA923872AA21C2F1F64', 'jianyue', 'level', '会员', '2', '1515912832591');
INSERT INTO `t_crm_field_option` VALUES ('1AF014118A164FB2BB40528BEF6B41BA23', 'guangzhouqili04644', 'type', '对接获取', '2', '1515984451214');
INSERT INTO `t_crm_field_option` VALUES ('448FB2921079453E895F33E44A76F2F033', 'jianyue', 'type', '系统增加', '4', '1515912832591');
INSERT INTO `t_crm_field_option` VALUES ('44D0A1C0F39643BFA775EC8B384C983832', 'guangzhouqili04644', 'type', '主动注册', '3', '1515984451214');
INSERT INTO `t_crm_field_option` VALUES ('5395DE4B180C423789E1E9CDCE6D11BB61', 'guangzhouqili04644', 'status', '可用', '0', '1515984451214');
INSERT INTO `t_crm_field_option` VALUES ('5EA9E9A5B7C147B8BF91F747351BAA5157', 'guangzhouqili04644', 'type', '系统增加', '4', '1515984451214');
INSERT INTO `t_crm_field_option` VALUES ('6C5539D612F94F9BB9DB5A37E6E88B8F82', 'jianyue', 'sex', '未知', '1', '1515912832591');
INSERT INTO `t_crm_field_option` VALUES ('6CE8DAD8E86A405B9467D7E056E1DD1493', 'guangzhouqili04644', 'level', '黄金', '3', '1515984451214');
INSERT INTO `t_crm_field_option` VALUES ('7061DB6D921343119386AF976D31E8A426', 'jianyue', 'status', '禁用', '1', '1515912832590');
INSERT INTO `t_crm_field_option` VALUES ('78C65072E00744139A4FC591A756FE9B37', 'jianyue', 'sex', '女士', '3', '1515912832591');
INSERT INTO `t_crm_field_option` VALUES ('7F817CEFE48E45749B81081A39CD18D091', 'jianyue', 'level', '普通', '1', '1515912832590');
INSERT INTO `t_crm_field_option` VALUES ('87A584FAD4314376912A64B5965C385E59', 'guangzhouqili04644', 'sex', '未知', '1', '1515984451214');
INSERT INTO `t_crm_field_option` VALUES ('88ACCFA624CD4D3C990053D5D0160E2244', 'jianyue', 'type', '匿名访问', '1', '1515912832591');
INSERT INTO `t_crm_field_option` VALUES ('8C92A4C102DE4E2E8D33ED3F33BC9AE949', 'guangzhouqili04644', 'type', '匿名访问', '1', '1515984451214');
INSERT INTO `t_crm_field_option` VALUES ('9AE88CC79BD44A21AE78D0DB8B4FE1F379', 'guangzhouqili04644', 'sex', '先生', '2', '1515984451214');
INSERT INTO `t_crm_field_option` VALUES ('9B09C5F419A5424897FC3D8C8628679784', 'guangzhouqili04644', 'status', '禁用', '1', '1515984451214');
INSERT INTO `t_crm_field_option` VALUES ('9D67F72288E24A858B3007DAE1309DFC87', 'jianyue', 'status', '可用', '0', '1515912832590');
INSERT INTO `t_crm_field_option` VALUES ('BA26913547BE4EF79D830208BFC93BA347', 'guangzhouqili04644', 'level', '红名单', '4', '1515984451214');
INSERT INTO `t_crm_field_option` VALUES ('C81A11E8182644F5A59836B82207FF7004', 'guangzhouqili04644', 'sex', '女士', '3', '1515984451214');
INSERT INTO `t_crm_field_option` VALUES ('CAD5CEE0E71E433FB2B1E46F9120CAC285', 'jianyue', 'sex', '先生', '2', '1515912832591');
INSERT INTO `t_crm_field_option` VALUES ('D2ADDFFEAB554255A5DE4AD90B21DFE802', 'jianyue', 'type', '对接获取', '2', '1515912832591');
INSERT INTO `t_crm_field_option` VALUES ('D7FE2EB9EA464F5A95A5A0CE6299738833', 'guangzhouqili04644', 'level', '会员', '2', '1515984451214');
INSERT INTO `t_crm_field_option` VALUES ('E91243A084E14626871CC5A77D7E16AD82', 'jianyue', 'level', '黄金', '3', '1515912832591');

-- ----------------------------
-- Table structure for `t_extend`
-- ----------------------------
DROP TABLE IF EXISTS `t_extend`;
CREATE TABLE `t_extend` (
  `id` varchar(80) NOT NULL,
  `org` varchar(100) CHARACTER SET gbk NOT NULL,
  `pid` varchar(100) CHARACTER SET gbk NOT NULL,
  `name` varchar(80) NOT NULL,
  `type` varchar(1) NOT NULL,
  `url` varchar(300) NOT NULL,
  `create_time` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=gb2312;

-- ----------------------------
-- Records of t_extend
-- ----------------------------
INSERT INTO `t_extend` VALUES ('4CD477A5336944AFB3057F1ACB712EAC71', 'jianyue', 'jianyue', '嵌入页', '1', 'https://www.baidu.com?crmid={{crm.id}}', '1524644875864');

-- ----------------------------
-- Table structure for `t_group`
-- ----------------------------
DROP TABLE IF EXISTS `t_group`;
CREATE TABLE `t_group` (
  `id` varchar(100) NOT NULL,
  `org` varchar(100) NOT NULL,
  `pid` varchar(100) NOT NULL,
  `no` varchar(80) NOT NULL,
  `name` varchar(80) NOT NULL,
  `memo` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=gbk;

-- ----------------------------
-- Records of t_group
-- ----------------------------
INSERT INTO `t_group` VALUES ('G_216AD526A3A84B67A09FBF131E59726F70', 'guangzhouqili04644', 'guangzhouqili04644', '10001', '服务组', null);
INSERT INTO `t_group` VALUES ('G_4FB3F910E4AD4516AA45C8CCBC1D21A345', 'jianyue', 'jianyue', '10001', '服务组', null);

-- ----------------------------
-- Table structure for `t_group_user`
-- ----------------------------
DROP TABLE IF EXISTS `t_group_user`;
CREATE TABLE `t_group_user` (
  `id` varchar(100) NOT NULL DEFAULT '1',
  `org` varchar(100) NOT NULL,
  `pid` varchar(100) DEFAULT NULL,
  `groupId` varchar(100) NOT NULL,
  `user` varchar(100) NOT NULL,
  `switch_policy` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=gbk;

-- ----------------------------
-- Records of t_group_user
-- ----------------------------
INSERT INTO `t_group_user` VALUES ('0F812BC50B034759AC797ACC290738CF50', 'jianyue', 'jianyue', 'G_4FB3F910E4AD4516AA45C8CCBC1D21A345', '13631484266', '1');
INSERT INTO `t_group_user` VALUES ('3EFCD7FDAFAE4313A95A52EAE12D486D74', 'jianyue', 'jianyue', 'G_4FB3F910E4AD4516AA45C8CCBC1D21A345', '13631484267', '1');
INSERT INTO `t_group_user` VALUES ('GU_FF14C01E7E7344A7A6D480948D8544D734', 'guangzhouqili04644', 'guangzhouqili04644', 'G_216AD526A3A84B67A09FBF131E59726F70', '13922739111', '0');

-- ----------------------------
-- Table structure for `t_lw`
-- ----------------------------
DROP TABLE IF EXISTS `t_lw`;
CREATE TABLE `t_lw` (
  `id` varchar(100) NOT NULL,
  `org` varchar(100) NOT NULL,
  `pid` varchar(100) NOT NULL,
  `sid` varchar(100) DEFAULT NULL,
  `channel` varchar(100) DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `phone` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `status` varchar(3) DEFAULT NULL,
  `qq` varchar(100) DEFAULT NULL,
  `memo` varchar(1000) DEFAULT NULL,
  `crm` varchar(100) DEFAULT NULL,
  `handle_user` varchar(100) DEFAULT NULL,
  `handle_time` bigint(100) DEFAULT NULL,
  `handle_result` varchar(1000) DEFAULT NULL,
  `tag1` varchar(100) DEFAULT NULL,
  `tag2` varchar(100) DEFAULT NULL,
  `tag3` varchar(100) DEFAULT NULL,
  `tag4` varchar(100) DEFAULT NULL,
  `create_time` bigint(20) NOT NULL,
  `extend1` varchar(1000) DEFAULT NULL,
  `extend2` varchar(1000) DEFAULT NULL,
  `extend3` varchar(1000) DEFAULT NULL,
  `extend4` varchar(1000) DEFAULT NULL,
  `extend5` varchar(1000) DEFAULT NULL,
  `extend6` varchar(1000) DEFAULT NULL,
  `extend7` varchar(1000) DEFAULT NULL,
  `extend8` varchar(1000) DEFAULT NULL,
  `extend9` varchar(1000) DEFAULT NULL,
  `extend10` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=gbk;

-- ----------------------------
-- Records of t_lw
-- ----------------------------
INSERT INTO `t_lw` VALUES ('LW_B56F9FA0196A41C4B37019B9938D6A2968', 'jianyue', 'jianyue', 'SSN_DA44E35C1CA34CE592B06C8ED56C224F57', '10001', '郑余杰', '13631484267', '33845916@qq.com', 'N', '33845916', '这个东西怎么卖？', 'CRM_FD92C3F638024ED79CAB6A4710E3CC1B87', null, null, null, null, null, null, null, '1524922603115', null, null, null, null, null, null, null, null, null, null);

-- ----------------------------
-- Table structure for `t_org`
-- ----------------------------
DROP TABLE IF EXISTS `t_org`;
CREATE TABLE `t_org` (
  `pid` varchar(100) NOT NULL,
  `org` varchar(100) NOT NULL,
  `name` varchar(80) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `insert_time` datetime NOT NULL,
  `first_logon` datetime DEFAULT NULL,
  `ip` varchar(100) NOT NULL,
  PRIMARY KEY (`pid`)
) ENGINE=InnoDB DEFAULT CHARSET=gbk;

-- ----------------------------
-- Records of t_org
-- ----------------------------
INSERT INTO `t_org` VALUES ('guangzhouqili04644', 'guangzhouqili04644', '广州奇利', '13922739111', '2018-01-15 10:47:31', '2018-01-15 10:47:49', '58.62.52.188');
INSERT INTO `t_org` VALUES ('jianyue', 'jianyue', '简约客服', '13631484267', '2018-01-14 14:53:52', '2018-01-14 14:53:58', '115.175.199.180');

-- ----------------------------
-- Table structure for `t_org_capability`
-- ----------------------------
DROP TABLE IF EXISTS `t_org_capability`;
CREATE TABLE `t_org_capability` (
  `id` varchar(100) NOT NULL,
  `start_time` int(11) NOT NULL,
  `end_time` int(11) NOT NULL,
  `type` varchar(100) NOT NULL COMMENT '0表示坐席数据的能力\r\n1表示？',
  `capability` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=gbk;

-- ----------------------------
-- Records of t_org_capability
-- ----------------------------

-- ----------------------------
-- Table structure for `t_question`
-- ----------------------------
DROP TABLE IF EXISTS `t_question`;
CREATE TABLE `t_question` (
  `id` varchar(100) NOT NULL,
  `org` varchar(100) NOT NULL,
  `pid` varchar(100) NOT NULL,
  `no` varchar(11) NOT NULL,
  `question` varchar(200) NOT NULL,
  `answer` varchar(2000) NOT NULL,
  `insert_time` varchar(20) NOT NULL,
  `times` int(11) DEFAULT NULL,
  `type` varchar(1) DEFAULT NULL COMMENT 'F：FAQ，\r\nM：标准用于，\r\nA：自定义',
  `level` int(11) DEFAULT NULL,
  `subject` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=gbk;

-- ----------------------------
-- Records of t_question
-- ----------------------------

-- ----------------------------
-- Table structure for `t_session`
-- ----------------------------
DROP TABLE IF EXISTS `t_session`;
CREATE TABLE `t_session` (
  `id` varchar(100) NOT NULL,
  `org` varchar(100) NOT NULL,
  `pid` varchar(100) DEFAULT NULL,
  `channel` varchar(100) NOT NULL,
  `crm` varchar(100) NOT NULL,
  `request_time` bigint(100) NOT NULL,
  `robot_time` bigint(11) DEFAULT NULL,
  `queue_time` bigint(11) DEFAULT NULL,
  `agent_time` bigint(100) DEFAULT NULL,
  `end_time` bigint(11) DEFAULT NULL,
  `left_time` bigint(11) DEFAULT NULL,
  `agent` varchar(100) DEFAULT NULL,
  `agent_name` varchar(100) DEFAULT NULL,
  `end_type` varchar(30) DEFAULT NULL,
  `first_agent_response_time` bigint(11) DEFAULT NULL,
  `request_group` varchar(100) DEFAULT NULL,
  `message_count` int(11) DEFAULT '0',
  `message_count_when_agent` int(11) DEFAULT '0',
  `agent_message_count` int(11) DEFAULT NULL,
  `system_message_count` int(11) DEFAULT NULL,
  `satisfy_score` tinyint(4) DEFAULT NULL,
  `satisfy_text` varchar(500) DEFAULT NULL,
  `step` varchar(30) DEFAULT NULL,
  `tag1` varchar(100) DEFAULT NULL,
  `tag2` varchar(100) DEFAULT NULL,
  `tag3` varchar(100) DEFAULT NULL,
  `tag4` varchar(100) DEFAULT NULL,
  `ip` varchar(100) DEFAULT NULL,
  `trace` varchar(100) DEFAULT NULL,
  `province` varchar(100) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `extend1` varchar(200) DEFAULT NULL,
  `extend2` varchar(200) DEFAULT NULL,
  `extend3` varchar(200) DEFAULT NULL,
  `extend4` varchar(200) DEFAULT NULL,
  `extend5` varchar(200) DEFAULT NULL,
  `extend6` varchar(200) DEFAULT NULL,
  `extend7` varchar(200) DEFAULT NULL,
  `extend8` varchar(200) DEFAULT NULL,
  `extend9` varchar(200) DEFAULT NULL,
  `extend10` varchar(200) DEFAULT NULL,
  `type` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `indx_pid_crm` (`pid`,`crm`),
  KEY `indx_pid` (`pid`),
  KEY `indx_requesttime` (`request_time`),
  KEY `indx_channel` (`channel`),
  KEY `indx_crm` (`crm`),
  KEY `indx_group` (`request_group`)
) ENGINE=InnoDB DEFAULT CHARSET=gbk;

-- ----------------------------
-- Records of t_session
-- ----------------------------
INSERT INTO `t_session` VALUES ('SSN_021684A231964429A6F3BDEA3592B1A956', 'jianyue', 'jianyue', '10001', 'CRM_41C35604AB67449A9971453627A0C04F94', '1544182273472', '1544182282611', null, null, '1544182545994', null, null, null, 'BY_OFFLINE', null, 'G_4FB3F910E4AD4516AA45C8CCBC1D21A345', '5', '0', '0', '6', null, null, 'SELF', null, null, null, null, '127.0.0.1', null, 'IANA', '', null, null, null, null, null, null, null, null, null, null, 'S');
INSERT INTO `t_session` VALUES ('SSN_03983B91A68F4CE1A28E2A375ABE9E1640', 'jianyue', 'jianyue', '10001', 'CRM_FD92C3F638024ED79CAB6A4710E3CC1B87', '1524922310937', '1524922312400', null, null, '1524922317439', null, null, null, 'BY_VISITOR', null, 'G_4FB3F910E4AD4516AA45C8CCBC1D21A345', '0', '0', '0', '2', null, null, 'SELF', null, null, null, null, '183.236.19.67', '04C029DA739D44D78093832A5131CA3E93', '广东', '广州', null, null, null, null, null, null, null, null, null, null, 'S');
INSERT INTO `t_session` VALUES ('SSN_0ECC196DD444470CA68B6098023CFA0B86', 'jianyue', 'jianyue', 'Q33845916', '241282860', '1528866449786', '1528866457480', null, null, '1528866756203', null, null, null, 'BY_VISITOR', null, 'G_4FB3F910E4AD4516AA45C8CCBC1D21A345', '0', '0', '0', '4', null, null, 'SELF', null, null, null, null, 'wechat', null, '未知', '未知', null, null, null, null, null, null, null, null, null, null, 'S');
INSERT INTO `t_session` VALUES ('SSN_13367159BA6349F288F834144001759810', 'jianyue', 'jianyue', '10001', 'CRM_12471520579C4CD6B99E35061D65B2C264', '1529247140579', '1529247141128', '1529247184493', '1529247196661', '1529247220551', null, '13631484267', '管理员', 'BY_AGENT', null, 'G_4FB3F910E4AD4516AA45C8CCBC1D21A345', '8', '2', '0', '9', null, null, 'AGENT', null, null, null, null, '127.0.0.1', null, 'IANA', '', null, null, null, null, null, null, null, null, null, null, 'S');
INSERT INTO `t_session` VALUES ('SSN_14940431F3C14A1E83D594397D6D244D09', 'jianyue', 'jianyue', '10001', 'CRM_F3D021279C07443EBA58D3164903CA1E29', '1524644665285', '1524644665748', null, null, null, null, null, null, null, null, 'G_4FB3F910E4AD4516AA45C8CCBC1D21A345', '0', '0', '0', '0', null, null, 'SELF', null, null, null, null, '119.129.119.132', '0D7A0C4CB17147639F7675DC7BABECB940', '广东', '广州', null, null, null, null, null, null, null, null, null, null, 'S');
INSERT INTO `t_session` VALUES ('SSN_1D90C3AA7BEE4068A2452AD835EE326199', 'jianyue', 'jianyue', 'Q453781573', '632413544', '1529026524638', null, null, null, '1529026524690', null, null, null, 'BY_VISITOR', null, null, '0', '0', '0', '1', null, null, 'LOGIN', null, null, null, null, 'wechat', null, '未知', '未知', null, null, null, null, null, null, null, null, null, null, 'S');
INSERT INTO `t_session` VALUES ('SSN_296D4B4379434E87B3BB9C00754FA42005', 'jianyue', 'jianyue', '10001', 'CRM_592BDAFF9B4E479BA58ED5642A517A8D80', '1519537509514', '1519537536605', null, null, null, null, null, null, null, null, 'G_4FB3F910E4AD4516AA45C8CCBC1D21A345', '0', '0', '0', '1', '5', null, 'SELF', null, null, null, null, '58.62.53.70', '6D6C29DFA9CB43B2A28477021F3124D110', '广东', '广州', null, null, null, null, null, null, null, null, null, null, 'S');
INSERT INTO `t_session` VALUES ('SSN_29C4AC2C42BD4253A193F7578613F30925', 'jianyue', 'jianyue', '10001', 'CRM_D83F31B1996E4A97BCF0468F003AA3CC30', '1519441639531', '1519441640090', '1519441644614', null, '1519441650986', null, null, null, 'BY_VISITOR', null, 'G_4FB3F910E4AD4516AA45C8CCBC1D21A345', '0', '0', '0', '2', null, null, 'QUEUE', null, null, null, null, '113.67.156.162', '71C5FDC46EAC4AA8AF21C4850A71E72139', '广东', '广州', null, null, null, null, null, null, null, null, null, null, 'S');
INSERT INTO `t_session` VALUES ('SSN_31CE706AC5434584ACBCC2218F67818F25', 'jianyue', 'jianyue', '10001', 'CRM_12471520579C4CD6B99E35061D65B2C264', '1528957521645', null, null, null, '1528957530593', null, null, null, 'BY_VISITOR', null, null, '0', '0', '0', '1', null, null, 'LOGIN', null, null, null, null, '127.0.0.1', null, 'IANA', '', null, null, null, null, null, null, null, null, null, null, 'S');
INSERT INTO `t_session` VALUES ('SSN_3563BDDC8C6344A8B1670DB2EF5D897C94', 'jianyue', 'jianyue', '10001', 'CRM_12471520579C4CD6B99E35061D65B2C264', '1529245944460', null, null, null, '1529246664207', null, null, null, 'BY_VISITOR', null, null, '0', '0', '0', '1', null, null, 'LOGIN', null, null, null, null, '127.0.0.1', null, 'IANA', '', null, null, null, null, null, null, null, null, null, null, 'S');
INSERT INTO `t_session` VALUES ('SSN_3EE7C41086B748AEACA1A70C651212CA11', 'jianyue', 'jianyue', 'Q33845916', '241282860', '1528866982090', null, null, null, '1528866986815', null, null, null, 'BY_VISITOR', null, null, '0', '0', '0', '2', null, null, 'LOGIN', null, null, null, null, 'wechat', null, '未知', '未知', null, null, null, null, null, null, null, null, null, null, 'S');
INSERT INTO `t_session` VALUES ('SSN_462920E4F00E4DF18973E8FF25F9361458', 'jianyue', 'jianyue', '10001', 'CRM_73E8CC8E825E4C288C52A4DC8B1F66C515', '1517994758393', '1517994758732', '1517994802520', '1517994802521', '1517995567888', null, '13631484267', '管理员', 'BY_SILENT', '1517994829361', 'G_4FB3F910E4AD4516AA45C8CCBC1D21A345', '14', '12', '11', '5', null, null, 'AGENT', null, null, null, null, '121.32.108.24', '01525CBBAE0B49DF87E429F992F8C29B76', '广东', '广州', null, null, null, null, null, null, null, null, null, null, 'S');
INSERT INTO `t_session` VALUES ('SSN_47CCDEF4669E41DE991BA09F31FB621D13', 'jianyue', 'jianyue', 'Q33845916', '241282860', '1528866990130', '1528866991937', '1528866998885', '1528866998886', '1528867584679', null, '13631484267', '管理员', 'BY_SILENT', '1528867012956', 'G_4FB3F910E4AD4516AA45C8CCBC1D21A345', '4', '3', '10', '7', null, null, 'AGENT', null, null, null, null, 'wechat', null, '未知', '未知', null, null, null, null, null, null, null, null, null, null, 'S');
INSERT INTO `t_session` VALUES ('SSN_4953F4B81D5942C6B090ECCE8DF5B04391', 'jianyue', 'jianyue', '10001', 'CRM_A54F0EA373FF495F954102B2364299E031', '1517469028733', null, null, null, null, null, null, null, null, null, null, '0', '0', '0', '0', null, null, 'LOGIN', null, null, null, null, '183.192.201.97', null, '上海', '', null, null, null, null, null, null, null, null, null, null, 'S');
INSERT INTO `t_session` VALUES ('SSN_4CB4E3352FC345F0A633C5BB2B29500054', 'jianyue', 'jianyue', 'Q453781573', '2679020244', '1528889495423', '1528889501681', '1528889509783', null, '1528889689821', null, null, null, 'BY_QUEUE_OVER_TIME', null, 'G_4FB3F910E4AD4516AA45C8CCBC1D21A345', '2', '0', '0', '5', null, null, 'QUEUE', null, null, null, null, 'wechat', null, '未知', '未知', null, null, null, null, null, null, null, null, null, null, 'S');
INSERT INTO `t_session` VALUES ('SSN_4CBB07A0B3F44B9C8E5CED37DF0020B129', 'jianyue', 'jianyue', '10001', 'CRM_592BDAFF9B4E479BA58ED5642A517A8D80', '1519537115244', '1519537115816', '1519537164880', null, '1519537345604', null, null, null, 'BY_QUEUE_OVER_TIME', null, 'G_4FB3F910E4AD4516AA45C8CCBC1D21A345', '2', '0', '0', '4', null, null, 'QUEUE', null, null, null, null, '58.62.53.70', '6D6C29DFA9CB43B2A28477021F3124D110', '广东', '广州', null, null, null, null, null, null, null, null, null, null, 'S');
INSERT INTO `t_session` VALUES ('SSN_4FDA7FC4D8C246AB870BFAFE15F5316F91', 'jianyue', 'jianyue', '10001', 'CRM_12471520579C4CD6B99E35061D65B2C264', '1529246667814', '1529246909104', null, null, '1529247007607', null, null, null, 'BY_VISITOR', null, 'G_4FB3F910E4AD4516AA45C8CCBC1D21A345', '7', '0', '0', '9', '5', null, 'SELF', null, null, null, null, '127.0.0.1', null, 'IANA', '', null, null, null, null, null, null, null, null, null, null, 'S');
INSERT INTO `t_session` VALUES ('SSN_53B90A5B24544703B672AF3A22F0C64244', 'jianyue', 'jianyue', 'Q33845916', '241282860', '1528866935905', null, null, null, '1528866977447', null, null, null, 'BY_VISITOR', null, null, '0', '0', '0', '2', null, null, 'LOGIN', null, null, null, null, 'wechat', null, '未知', '未知', null, null, null, null, null, null, null, null, null, null, 'S');
INSERT INTO `t_session` VALUES ('SSN_5DCB4BBF49A04BD09FA247C19E1F384852', 'jianyue', 'jianyue', '10001', 'CRM_A9DFA9ECB5CF403E88C8355F06EDB10816', '1517468969200', '1517468975737', null, null, '1517468989514', null, null, null, 'BY_VISITOR', null, 'G_4FB3F910E4AD4516AA45C8CCBC1D21A345', '0', '0', '0', '2', null, null, 'SELF', null, null, null, null, '1.80.3.152', '6C607EE367044E089A9B2401C22AE27D55', '陕西', '西安', null, null, null, null, null, null, null, null, null, null, 'S');
INSERT INTO `t_session` VALUES ('SSN_65BFF258EE2F4B8CB85135A077656FC513', 'jianyue', 'jianyue', 'Q453781573', '4149582464', '1529247081726', '1529247084334', null, null, '1529247891420', null, null, null, 'BY_VISITOR', null, 'G_4FB3F910E4AD4516AA45C8CCBC1D21A345', '0', '0', '0', '2', null, null, 'SELF', null, null, null, null, 'wechat', null, '未知', '未知', null, null, null, null, null, null, null, null, null, null, 'S');
INSERT INTO `t_session` VALUES ('SSN_6637CF820FDE440EB3A1A9C35C398BB029', 'jianyue', 'jianyue', 'Q33845916', '241282860', '1528866763173', '1528866765488', '1528866774234', '1528866774247', '1528866799889', null, '13631484267', '管理员', 'BY_VISITOR', '1528866785699', 'G_4FB3F910E4AD4516AA45C8CCBC1D21A345', '1', '0', '1', '5', null, null, 'AGENT', null, null, null, null, 'wechat', null, '未知', '未知', null, null, null, null, null, null, null, null, null, null, 'S');
INSERT INTO `t_session` VALUES ('SSN_6D2BF8370FCA4218A401F3BF36CB99C271', 'jianyue', 'jianyue', 'Q453781573', '4149582464', '1529245724564', '1529245740233', '1529247025400', '1529247025584', '1529247079876', null, '13631484267', '管理员', 'BY_VISITOR', null, 'G_4FB3F910E4AD4516AA45C8CCBC1D21A345', '1', '1', '0', '5', null, null, 'AGENT', null, null, null, null, 'wechat', null, '未知', '未知', null, null, null, null, null, null, null, null, null, null, 'S');
INSERT INTO `t_session` VALUES ('SSN_71779117B0204C1D9CB62A70F287301901', 'jianyue', 'jianyue', '10001', 'CRM_6AB2A61C88254E5E855019F9D6DE5AFB49', '1528957543021', null, null, null, '1528957545670', null, null, null, 'BY_VISITOR', null, null, '0', '0', '0', '1', null, null, 'LOGIN', null, null, null, null, '127.0.0.1', null, 'IANA', '', null, null, null, null, null, null, null, null, null, null, 'S');
INSERT INTO `t_session` VALUES ('SSN_740B307867AA461BB695ED561F86FF0997', 'jianyue', 'jianyue', '10001', 'CRM_96A60FDB6B6E4AA39A87E59E00FF8BDB01', '1515982383455', '1515982389795', null, null, null, null, null, null, null, null, 'G_4FB3F910E4AD4516AA45C8CCBC1D21A345', '0', '0', '0', '0', null, null, 'SELF', null, null, null, null, '119.130.228.87', 'F521A2F131414086A834C42D6A16A9AB05', '广东', '广州', null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_session` VALUES ('SSN_76CE0BADAA28417F98600DA4553924A952', 'jianyue', 'jianyue', '10001', 'CRM_F3D021279C07443EBA58D3164903CA1E29', '1524644222594', '1524644223542', '1524644229997', '1524644230029', '1524644532434', null, '13631484267', '管理员', 'BY_SILENT', null, 'G_4FB3F910E4AD4516AA45C8CCBC1D21A345', '0', '0', '0', '3', null, null, 'AGENT', null, null, null, null, '119.129.119.132', '6BD820A953E9498FA6FED4BFA9A2F42D97', '广东', '广州', null, null, null, null, null, null, null, null, null, null, 'S');
INSERT INTO `t_session` VALUES ('SSN_8275258F9DA54440AB84644EDFDE2F5D71', 'jianyue', 'jianyue', '10001', 'CRM_592BDAFF9B4E479BA58ED5642A517A8D80', '1519458339952', '1519458573231', '1519458591946', '1519458612940', '1519458929526', null, '13631484267', '管理员', 'BY_SILENT', '1519458653416', 'G_4FB3F910E4AD4516AA45C8CCBC1D21A345', '2', '1', '2', '5', null, null, 'AGENT', null, null, null, null, '113.67.156.162', null, '广东', '广州', null, null, null, null, null, null, null, null, null, null, 'S');
INSERT INTO `t_session` VALUES ('SSN_89A5F537B58A499CAFFCD51AC9EF598792', 'jianyue', 'jianyue', '10001', 'CRM_D83F31B1996E4A97BCF0468F003AA3CC30', '1519441673024', '1519441673403', '1519441674450', '1519441674451', '1519444484291', null, '13631484267', '管理员', 'BY_SILENT', null, 'G_4FB3F910E4AD4516AA45C8CCBC1D21A345', '0', '0', '0', '4', null, null, 'AGENT', null, null, null, null, '113.67.156.162', '697EB3F3E0F7407291103DE80F6E8E8909', '广东', '广州', null, null, null, null, null, null, null, null, null, null, 'S');
INSERT INTO `t_session` VALUES ('SSN_8B8600E2E6EB4DD1AC03C953AC3C033974', 'jianyue', 'jianyue', 'Q33845916', '241282860', '1528865704901', '1528865821901', null, null, '1528865869102', null, null, null, 'BY_VISITOR', null, 'G_4FB3F910E4AD4516AA45C8CCBC1D21A345', '0', '0', '0', '2', null, null, 'SELF', null, null, null, null, 'wechat', null, '未知', '未知', null, null, null, null, null, null, null, null, null, null, 'S');
INSERT INTO `t_session` VALUES ('SSN_8CCDA992EBD04556A0EA90E2F11C0EB900', 'jianyue', 'jianyue', 'Q33845916', '2502423763', '1528857577583', null, null, null, null, null, null, null, null, null, null, '0', '0', '0', '0', null, null, 'LOGIN', null, null, null, null, 'wechat', null, '未知', '未知', null, null, null, null, null, null, null, null, null, null, 'S');
INSERT INTO `t_session` VALUES ('SSN_8CDCAC2A37E34C9B85C17306EF4C91CE26', 'jianyue', 'jianyue', 'Q453781573', '632413544', '1529026899552', '1529026901106', null, null, null, null, null, null, null, null, 'G_4FB3F910E4AD4516AA45C8CCBC1D21A345', '0', '0', '0', '1', null, null, 'SELF', null, null, null, null, 'wechat', null, '未知', '未知', null, null, null, null, null, null, null, null, null, null, 'S');
INSERT INTO `t_session` VALUES ('SSN_91EBCA5754584D89A75C4E5E2DB3995D52', 'jianyue', 'jianyue', 'Q33845916', '4035724768', '1528856009167', null, null, null, null, null, null, null, null, null, null, '0', '0', '0', '0', null, null, 'LOGIN', null, null, null, null, 'wechat', null, '未知', '未知', null, null, null, null, null, null, null, null, null, null, 'S');
INSERT INTO `t_session` VALUES ('SSN_953F1C92923A4483A2EB6447015AD1EC16', 'jianyue', 'jianyue', '10001', 'CRM_592BDAFF9B4E479BA58ED5642A517A8D80', '1519459154284', '1519459162221', '1519459488323', '1519459488331', '1519459791839', null, '13631484267', '管理员', 'BY_SILENT', '1519459507203', 'G_4FB3F910E4AD4516AA45C8CCBC1D21A345', '1', '0', '2', '5', null, null, 'AGENT', null, null, null, null, '113.67.156.162', '011F87C508244D618FC9E833A8C25E8352', '广东', '广州', null, null, null, null, null, null, null, null, null, null, 'S');
INSERT INTO `t_session` VALUES ('SSN_965CC91A07A347F7A18E967F234F9E5659', 'jianyue', 'jianyue', '10001', 'CRM_254FAC44689E43288F37E157C627298613', '1526458115708', '1526458124521', '1526458150538', null, '1526458166485', null, null, null, 'BY_VISITOR', null, 'G_4FB3F910E4AD4516AA45C8CCBC1D21A345', '3', '0', '0', '4', null, null, 'QUEUE', null, null, null, null, '183.6.189.150', 'BB2FEABC8E03453199ACB8822BBE570A77', '广东', '广州', null, null, null, null, null, null, null, null, null, null, 'S');
INSERT INTO `t_session` VALUES ('SSN_96D709D27A8C422982F7BB8707585F9063', 'jianyue', 'jianyue', '10001', 'CRM_0C9492D35DD04F18BB232CAA39324C7181', '1515912841529', '1515912849165', '1515912860219', '1515912860227', '1519459473757', null, '13631484267', '管理员', 'BY_AGENT', null, 'G_4FB3F910E4AD4516AA45C8CCBC1D21A345', '1', '0', '0', '3', null, null, 'AGENT', null, null, null, null, '115.175.199.180', '4938B0D292B3499B8B983AD45A1ED0A042', '广东', '广州', null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_session` VALUES ('SSN_A080A143716A4F63B514611E253D8B8026', 'jianyue', 'jianyue', '10001', 'CRM_3DE342D1424C4BA2BF105514A11EFF6679', '1520651245725', '1520651246915', '1520651253232', '1520651253266', '1520651684331', null, '13631484267', '管理员', 'BY_SILENT', '1520651411986', 'G_4FB3F910E4AD4516AA45C8CCBC1D21A345', '2', '1', '1', '5', null, null, 'AGENT', null, null, null, null, '123.197.161.27', '738599CA6C3C4EC69F006AB8FA4D9D7484', '广东', '广州', null, null, null, null, null, null, null, null, null, null, 'S');
INSERT INTO `t_session` VALUES ('SSN_A24F5C8DED214132A7FD28DD0147262C09', 'guangzhouqili04644', 'guangzhouqili04644', '10001', 'CRM_F819BE6907E6478FB0ED3A0F1CA44A2157', '1515984471533', '1515984472209', null, null, null, null, null, null, null, null, 'G_216AD526A3A84B67A09FBF131E59726F70', '0', '0', '0', '0', null, null, 'SELF', null, null, null, null, '58.62.52.188', 'E2CF84FE680A4142975F1F197B0CEFDC00', '广东', '广州', null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_session` VALUES ('SSN_A63885A2130F48829FCBAC7F57C8DF5706', 'jianyue', 'jianyue', 'Q453781573', '632413544', '1529026531419', '1529026533519', '1529026634949', '1529026634978', '1529027189536', null, '13631484267', '管理员', 'BY_SILENT', null, 'G_4FB3F910E4AD4516AA45C8CCBC1D21A345', '10', '2', '0', '14', null, null, 'AGENT', null, null, null, null, 'wechat', null, '未知', '未知', null, null, null, null, null, null, null, null, null, null, 'S');
INSERT INTO `t_session` VALUES ('SSN_ABB8F5B5260B404DBAA20B72D944A85473', 'jianyue', 'jianyue', 'Q453781573', '2679020244', '1528889133548', '1528889155035', '1528889241629', null, '1528889422859', null, null, null, 'BY_QUEUE_OVER_TIME', null, 'G_4FB3F910E4AD4516AA45C8CCBC1D21A345', '4', '0', '0', '6', null, null, 'QUEUE', null, null, null, null, 'wechat', null, '未知', '未知', null, null, null, null, null, null, null, null, null, null, 'S');
INSERT INTO `t_session` VALUES ('SSN_AD707E1E68C94AB3844E8FB88E5232EF53', 'jianyue', 'jianyue', '10001', 'CRM_FD92C3F638024ED79CAB6A4710E3CC1B87', '1524922290493', '1524922298904', null, null, '1524922306804', null, null, null, 'BY_VISITOR', null, 'G_4FB3F910E4AD4516AA45C8CCBC1D21A345', '0', '0', '0', '2', null, null, 'SELF', null, null, null, null, '183.236.19.67', '04C029DA739D44D78093832A5131CA3E93', '广东', '广州', null, null, null, null, null, null, null, null, null, null, 'S');
INSERT INTO `t_session` VALUES ('SSN_AD85624872C141FBAEF4C06599EE880901', 'jianyue', 'jianyue', 'Q453781573', '2679020244', '1528889723686', '1528889733303', '1528889736623', '1528889800817', '1528891109900', null, '13631484267', '管理员', 'BY_SILENT', '1528889818271', 'G_4FB3F910E4AD4516AA45C8CCBC1D21A345', '3', '0', '3', '8', null, null, 'AGENT', null, null, null, null, 'wechat', null, '未知', '未知', null, null, null, null, null, null, null, null, null, null, 'S');
INSERT INTO `t_session` VALUES ('SSN_B86739138B34454A851F9D1BAC189BDB73', 'jianyue', 'jianyue', 'Q453781573', '2603034262', '1528957601901', '1528957605898', null, null, null, null, null, null, null, null, 'G_4FB3F910E4AD4516AA45C8CCBC1D21A345', '0', '0', '0', '1', null, null, 'SELF', null, null, null, null, 'wechat', null, '未知', '未知', null, null, null, null, null, null, null, null, null, null, 'S');
INSERT INTO `t_session` VALUES ('SSN_BD7632A4415541D0B16D1DAF49ACE31F66', 'jianyue', 'jianyue', '10001', 'CRM_73E8CC8E825E4C288C52A4DC8B1F66C515', '1517994565280', '1517994571665', '1517994749454', '1517994749462', '1517994754230', null, '13631484267', '管理员', 'BY_VISITOR', null, 'G_4FB3F910E4AD4516AA45C8CCBC1D21A345', '0', '0', '0', '2', null, null, 'AGENT', null, null, null, null, '121.32.108.24', 'CC8F5E975626409EBD791A367585339911', '广东', '广州', null, null, null, null, null, null, null, null, null, null, 'S');
INSERT INTO `t_session` VALUES ('SSN_BF1FF2D973EE4DC283E2D09C50AC7DFE48', 'jianyue', 'jianyue', '10001', 'CRM_D99A51C133684DEAA19C2E986DEEC46B03', '1517395071761', '1517395072678', null, null, null, null, null, null, null, null, 'G_4FB3F910E4AD4516AA45C8CCBC1D21A345', '0', '0', '0', '0', null, null, 'SELF', null, null, null, null, '58.62.53.94', '2C26C975E5DD4AC6A8F00C39D385BA7223', '广东', '广州', null, null, null, null, null, null, null, null, null, null, 'S');
INSERT INTO `t_session` VALUES ('SSN_C1C49F4D05EA4591970620DDC9FBDB6712', 'jianyue', 'jianyue', 'Q33845916', '241282860', '1528866928984', null, null, null, '1528866933066', null, null, null, 'BY_VISITOR', null, null, '0', '0', '0', '2', null, null, 'LOGIN', null, null, null, null, 'wechat', null, '未知', '未知', null, null, null, null, null, null, null, null, null, null, 'S');
INSERT INTO `t_session` VALUES ('SSN_C35652F0A596464B8E4F63F64EADE7AE73', 'jianyue', 'jianyue', '10001', 'CRM_35ECA963A2094C769723A50F7049E9E539', '1524645468211', '1524645477514', null, null, null, null, null, null, null, null, 'G_4FB3F910E4AD4516AA45C8CCBC1D21A345', '0', '0', '0', '0', null, null, 'SELF', null, null, null, null, '119.129.119.132', '16ADB68D43A74901B4182433F9D2169700', '广东', '广州', null, null, null, null, null, null, null, null, null, null, 'S');
INSERT INTO `t_session` VALUES ('SSN_C5661FFB9FEC4A8D96C109B60BD5900E88', 'jianyue', 'jianyue', '10001', 'CRM_D83F31B1996E4A97BCF0468F003AA3CC30', '1519444624702', null, null, null, null, null, null, null, null, null, null, '0', '0', '0', '0', null, null, 'LOGIN', null, null, null, null, '113.67.156.162', '4C401CFDE784498E94DA41742075FBC282', '广东', '广州', null, null, null, null, null, null, null, null, null, null, 'S');
INSERT INTO `t_session` VALUES ('SSN_C6A2E8AF9AB84796ACD5482FDF5161C181', 'jianyue', 'jianyue', 'Q453781573', '632413544', '1529025678678', '1529025688970', null, null, '1529026520083', null, null, null, 'BY_VISITOR', null, 'G_4FB3F910E4AD4516AA45C8CCBC1D21A345', '32', '0', '0', '35', null, null, 'SELF', null, null, null, null, 'wechat', null, '未知', '未知', null, null, null, null, null, null, null, null, null, null, 'S');
INSERT INTO `t_session` VALUES ('SSN_CAA4F81D186846C3BD76D3DD2765D15628', 'jianyue', 'jianyue', 'Q33845916', '3101502925', '1528888794125', null, null, null, null, null, null, null, null, null, null, '0', '0', '0', '0', null, null, 'LOGIN', null, null, null, null, 'wechat', null, '未知', '未知', null, null, null, null, null, null, null, null, null, null, 'S');
INSERT INTO `t_session` VALUES ('SSN_D4B5DE8223804E1A8EE60BF0301CDB6F16', 'jianyue', 'jianyue', 'Q33845916', '241282860', '1528865873112', '1528866196114', null, null, '1528866251826', null, null, null, 'BY_VISITOR', null, 'G_4FB3F910E4AD4516AA45C8CCBC1D21A345', '0', '0', '0', '4', null, null, 'SELF', null, null, null, null, 'wechat', null, '未知', '未知', null, null, null, null, null, null, null, null, null, null, 'S');
INSERT INTO `t_session` VALUES ('SSN_D618129E27CA4B02BAE79615C07686AD76', 'jianyue', 'jianyue', 'Q33845916', '241282860', '1528865493339', null, null, null, '1528865694200', null, null, null, 'BY_VISITOR', null, null, '0', '0', '0', '2', null, null, 'LOGIN', null, null, null, null, 'wechat', null, '未知', '未知', null, null, null, null, null, null, null, null, null, null, 'S');
INSERT INTO `t_session` VALUES ('SSN_DA44E35C1CA34CE592B06C8ED56C224F57', 'jianyue', 'jianyue', '10001', 'CRM_FD92C3F638024ED79CAB6A4710E3CC1B87', '1524922326571', '1524922335460', '1524922562888', null, '1524922608805', null, null, null, 'BY_VISITOR', null, 'G_4FB3F910E4AD4516AA45C8CCBC1D21A345', '1', '0', '0', '3', '5', null, 'QUEUE', null, null, null, null, '183.236.19.67', '80A6B9F97BD04A74AF92B9A6706143E100', '广东', '广州', null, null, null, null, null, null, null, null, null, null, 'S');
INSERT INTO `t_session` VALUES ('SSN_E7509158ED504A468BABF9BB9E0DA06F77', 'jianyue', 'jianyue', '10001', 'CRM_3DE342D1424C4BA2BF105514A11EFF6679', '1520651903257', '1520651904092', null, null, null, null, null, null, null, null, 'G_4FB3F910E4AD4516AA45C8CCBC1D21A345', '0', '0', '0', '0', null, null, 'SELF', null, null, null, null, '123.197.161.27', 'C76051354434479AB7AEC3A37A84A89C70', '广东', '广州', null, null, null, null, null, null, null, null, null, null, 'S');
INSERT INTO `t_session` VALUES ('SSN_F7A4790F184F42F0887E57A9094AD0D635', 'jianyue', 'jianyue', 'Q33845916', '241282860', '1528866804239', '1528866805987', '1528866812628', '1528866812629', '1528866921408', null, '13631484267', '管理员', 'BY_VISITOR', '1528866825275', 'G_4FB3F910E4AD4516AA45C8CCBC1D21A345', '1', '0', '1', '5', null, null, 'AGENT', null, null, null, null, 'wechat', null, '未知', '未知', null, null, null, null, null, null, null, null, null, null, 'S');
INSERT INTO `t_session` VALUES ('SSN_FAE998428D054A1B8E8EA4FD3C0364E641', 'jianyue', 'jianyue', '10001', 'CRM_12471520579C4CD6B99E35061D65B2C264', '1528957535729', null, null, null, '1529245931820', null, null, null, 'BY_VISITOR', null, null, '0', '0', '0', '1', null, null, 'LOGIN', null, null, null, null, '127.0.0.1', null, 'IANA', '', null, null, null, null, null, null, null, null, null, null, 'S');
INSERT INTO `t_session` VALUES ('SSN_FBA1E145C1FB4B6FB3F35067FEA7F7FA15', 'jianyue', 'jianyue', '10001', 'CRM_12471520579C4CD6B99E35061D65B2C264', '1528957510913', null, null, null, '1528957520975', null, null, null, 'BY_VISITOR', null, null, '0', '0', '0', '1', null, null, 'LOGIN', null, null, null, null, '127.0.0.1', null, 'IANA', '', null, null, null, null, null, null, null, null, null, null, 'S');
INSERT INTO `t_session` VALUES ('SSN_FD021177E058408FBB019CA201EE009132', 'jianyue', 'jianyue', '10001', 'CRM_FF61E9A20D5B404B97ED9B0B137746EE28', '1525053622390', '1525053628480', null, null, '1525078834867', null, null, null, 'BY_VISITOR', null, 'G_4FB3F910E4AD4516AA45C8CCBC1D21A345', '0', '0', '0', '2', null, null, 'SELF', null, null, null, null, '118.196.221.157', 'C30F0254C9C049FBB40FA880D01A172248', '广东', '广州', null, null, null, null, null, null, null, null, null, null, 'S');
INSERT INTO `t_session` VALUES ('SSN_FD405039F8E44817A7387409A607F79425', 'jianyue', 'jianyue', 'Q453781573', '4149582464', '1529247910629', '1529247912450', null, null, null, null, null, null, null, null, 'G_4FB3F910E4AD4516AA45C8CCBC1D21A345', '0', '0', '0', '1', null, null, 'SELF', null, null, null, null, 'wechat', null, '未知', '未知', null, null, null, null, null, null, null, null, null, null, 'S');

-- ----------------------------
-- Table structure for `t_setting`
-- ----------------------------
DROP TABLE IF EXISTS `t_setting`;
CREATE TABLE `t_setting` (
  `id` varchar(100) NOT NULL,
  `org` varchar(100) NOT NULL,
  `pid` varchar(100) NOT NULL,
  `subject` varchar(100) DEFAULT NULL,
  `k` varchar(100) NOT NULL,
  `v` varchar(500) NOT NULL,
  `level` int(1) NOT NULL DEFAULT '1' COMMENT '0表示系统级别默认\r\n1表示企业级别默认\r\n2表示app级别默认\r\n3表示channel级别默认\r\n4表示user级别默认',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=gbk;

-- ----------------------------
-- Records of t_setting
-- ----------------------------
INSERT INTO `t_setting` VALUES ('033C3E1FDC59410D9A390B4E544B3D5E97', 'jianyuekefu57714', 'jianyuekefu57714', '', 'selfPolicy', '1', '1');
INSERT INTO `t_setting` VALUES ('03FFB1998DC04817A91E6B93C3DD538B94', 'jianyue', 'jianyue', '', 'selfSendQuestion', '', '1');
INSERT INTO `t_setting` VALUES ('09E5A26025CC47FAA753599DBA8CCCAA56', 'jianyuekefu57714', 'jianyuekefu57714', '', 'windowOpenType', 'D', '1');
INSERT INTO `t_setting` VALUES ('0B4F977410264CD19F3EFF22713EBFC375', 'jianyuekefu57714', 'jianyuekefu57714', '', 'windowHeight', '0', '1');
INSERT INTO `t_setting` VALUES ('0BAB8C314657427F910AB1A8084BCC2391', 'jianyuekefu57714', 'jianyuekefu57714', '', 'satisfyInviteType', '0', '1');
INSERT INTO `t_setting` VALUES ('0C8E2681508B47A8BD39A128A749B59258', 'jianyue', 'jianyue', '', 'tulingKey', 'e159df614ea14d60b4e5c9c0e688dae4', '1');
INSERT INTO `t_setting` VALUES ('0FA60BC4B88849BB8C306609FE177E7204', 'jianyuekefu57714', 'jianyuekefu57714', '', 'supportUrl', 'http://www.jianyuekefu.com', '1');
INSERT INTO `t_setting` VALUES ('1', 'defaultorg', 'defaultorg', null, 'serviceSettingMode', '1', '0');
INSERT INTO `t_setting` VALUES ('10', 'defaultorg', 'defaultorg', null, 'overtimeEndSessionTip', '会话已经超过时间，如果有问题请继续联系客服咨询', '0');
INSERT INTO `t_setting` VALUES ('11', 'defaultorg', 'defaultorg', null, 'agentLeftTip', '坐席暂时离开，请稍等', '0');
INSERT INTO `t_setting` VALUES ('111', 'defaultorg', 'defaultorg', null, 'sessionDispatchPolicy', '0', '0');
INSERT INTO `t_setting` VALUES ('12', 'defaultorg', 'defaultorg', null, 'guidePolicy', '0', '0');
INSERT INTO `t_setting` VALUES ('1509617263658', 'defaultorg', 'defaultorg', null, 'visitorQueuePolicy', '2', '0');
INSERT INTO `t_setting` VALUES ('1509632036689', 'defaultorg', 'defaultorg', null, 'selfPolicy', '1', '0');
INSERT INTO `t_setting` VALUES ('1509632332957', 'defaultorg', 'defaultorg', null, 'guidePopTip', '欢迎您的光临，如需更多服务请联系我们', '0');
INSERT INTO `t_setting` VALUES ('1509633247450', 'defaultorg', 'defaultorg', null, 'queueLimit', '3', '0');
INSERT INTO `t_setting` VALUES ('1509633858633', 'defaultorg', 'defaultorg', null, 'silentNoticeLimit', '3', '0');
INSERT INTO `t_setting` VALUES ('1509634093019', 'defaultorg', 'defaultorg', null, 'silentNoticeTip', '您已经休息了3分钟，请不要再沉默了，来撩我们吧', '0');
INSERT INTO `t_setting` VALUES ('1509634294241', 'defaultorg', 'defaultorg', null, 'silentEndTip', '也许您已经休息了，那我就不再打扰您了，如有任何疑惑请随时联系我们', '0');
INSERT INTO `t_setting` VALUES ('1509635853453', 'defaultorg', 'defaultorg', null, 'silentEndLimit', '5', '0');
INSERT INTO `t_setting` VALUES ('1509682084261', 'defaultorg', 'defaultorg', null, 'enterFirstTip', '你好，我是小猪，很高兴为您服务', '0');
INSERT INTO `t_setting` VALUES ('1509682484261', 'defaultorg', 'defaultorg', null, 'satisfyLimit', '5', '0');
INSERT INTO `t_setting` VALUES ('1509682815578', 'defaultorg', 'defaultorg', null, 'chatMaxType', '0', '0');
INSERT INTO `t_setting` VALUES ('1509685584705', 'defaultorg', 'defaultorg', null, 'satisfyThanksTip', '感谢您的评价，我们将一如既往地努力', '0');
INSERT INTO `t_setting` VALUES ('1509688584705', 'defaultorg', 'defaultorg', null, 'satisfyInviteType', '0', '0');
INSERT INTO `t_setting` VALUES ('1509689815578', 'defaultorg', 'defaultorg', null, 'chatMaxLimit', '10', '0');
INSERT INTO `t_setting` VALUES ('1509695584705', 'defaultorg', 'defaultorg', null, 'satisfyInviteTip', '尊敬的用户，请您对我的服务进行评价', '0');
INSERT INTO `t_setting` VALUES ('1509891762705', 'defaultorg', 'defaultorg', null, 'orgHeader', 'jianyue.png', '0');
INSERT INTO `t_setting` VALUES ('1510127764231', 'defaultorg', 'defaultorg', null, 'noAgentOnlineTip', '当前无客服在线，请稍后访问我们，感谢您的支持', '0');
INSERT INTO `t_setting` VALUES ('1510132962874', 'defaultorg', 'defaultorg', null, 'supportUrl', 'http://www.jianyuekefu.com', '0');
INSERT INTO `t_setting` VALUES ('1510133028635', 'defaultorg', 'defaultorg', null, 'supportHolder', '简约客服 13631484267', '0');
INSERT INTO `t_setting` VALUES ('1510156702832', 'defaultorg', 'defaultorg', null, 'visitorEndSessionTip', '您已经主动结束了会话,感谢您的支持', '0');
INSERT INTO `t_setting` VALUES ('1510223043233', 'defaultorg', 'defaultorg', null, 'noOnlineAgentPolicy', '0', '0');
INSERT INTO `t_setting` VALUES ('1510372365799', 'defaultorg', 'defaultorg', null, 'visitorQueueTip', '您目前正在排队，会话被接入后坐席将回答您的问题，感谢您的支持', '0');
INSERT INTO `t_setting` VALUES ('1510380052059', 'defaultorg', 'defaultorg', null, 'queueTalkingPolicy', '0', '0');
INSERT INTO `t_setting` VALUES ('1510411247742', 'defaultorg', 'defaultorg', null, 'selfSendQuestion', '', '0');
INSERT INTO `t_setting` VALUES ('1510539764317', 'defaultorg', 'defaultorg', null, 'selfUnknowTip', '我不太理解您的意思，请换个问法，或者咨询人工坐席', '0');
INSERT INTO `t_setting` VALUES ('1510545733833', 'defaultorg', 'defaultorg', null, 'selfName', '小猪机器人', '0');
INSERT INTO `t_setting` VALUES ('1510670045327', 'defaultorg', 'defaultorg', null, 'beforeGroupList', '请选择技能组', '0');
INSERT INTO `t_setting` VALUES ('1510670825646', 'defaultorg', 'defaultorg', null, 'historyMessagePolicy', '1', '0');
INSERT INTO `t_setting` VALUES ('1510974932507', 'defaultorg', 'defaultorg', null, 'buttonTag', '快速访问', '0');
INSERT INTO `t_setting` VALUES ('1511167167186', 'defaultorg', 'defaultorg', null, 'loginDefaultStatus', 'ONLINE', '0');
INSERT INTO `t_setting` VALUES ('1512387329073', 'defaultorg', 'defaultorg', null, 'accessKey', 'JIANYUE', '0');
INSERT INTO `t_setting` VALUES ('1512439990167', 'defaultorg', 'defaultorg', null, 'callUrl', '', '0');
INSERT INTO `t_setting` VALUES ('1512572631318', 'defaultorg', 'defaultorg', null, 'agentLogout', '0', '0');
INSERT INTO `t_setting` VALUES ('1512994449786', 'defaultorg', 'defaultorg', null, 'guideButtonText', '访问客服', '0');
INSERT INTO `t_setting` VALUES ('1512994905681', 'defaultorg', 'defaultorg', null, 'guidePopLater', '10', '0');
INSERT INTO `t_setting` VALUES ('1513235970827', 'defaultorg', 'defaultorg', null, 'agentTalkingPolicy', '1', '0');
INSERT INTO `t_setting` VALUES ('1513245069361', 'defaultorg', 'defaultorg', null, 'selfErrorLimit', '3', '0');
INSERT INTO `t_setting` VALUES ('1513247194402', 'defaultorg', 'defaultorg', null, 'selfAutoToManualTip', '现在系统为您转接人工坐席', '0');
INSERT INTO `t_setting` VALUES ('1513248524546', 'defaultorg', 'defaultorg', null, 'keyWordToManual', '转人工,人工坐席', '0');
INSERT INTO `t_setting` VALUES ('1513256248626', 'defaultorg', 'defaultorg', null, 'firstMenuTag', '常见问题', '0');
INSERT INTO `t_setting` VALUES ('1513565746858', 'defaultorg', 'defaultorg', null, 'callMethod', '', '0');
INSERT INTO `t_setting` VALUES ('1513906095529', 'defaultorg', 'defaultorg', null, 'windowShowType', 'B', '0');
INSERT INTO `t_setting` VALUES ('1513906211475', 'defaultorg', 'defaultorg', null, 'windowOpenType', 'P', '0');
INSERT INTO `t_setting` VALUES ('1513912414569', 'defaultorg', 'defaultorg', null, 'windowBackgroundColor', '00A4F5', '0');
INSERT INTO `t_setting` VALUES ('1513913254400', 'defaultorg', 'defaultorg', null, 'windowWidth', '880', '0');
INSERT INTO `t_setting` VALUES ('1513913295944', 'defaultorg', 'defaultorg', null, 'windowHeight', '600', '0');
INSERT INTO `t_setting` VALUES ('1514430294333', 'defaultorg', 'defaultorg', null, 'satisfyInput', '0', '0');
INSERT INTO `t_setting` VALUES ('1528956218597', 'defaultorg', 'defaultorg', null, 'robotType', '1', '0');
INSERT INTO `t_setting` VALUES ('1528956688435', 'defaultorg', 'defaultorg', null, 'tulingKey', '', '0');
INSERT INTO `t_setting` VALUES ('1780171ED3944518BDA6B235AFC4921939', 'jianyue', 'jianyue', 'Q33845916', 'noAgentOnlineTip', '当前无客服在线，请稍后访问我们，感谢您的支持', '2');
INSERT INTO `t_setting` VALUES ('17F73F3B25D64909B45ACBDDD67C687828', 'jianyue', 'jianyue', 'Q33845916', 'satisfyInviteTip', '尊敬的用户，请您对我的服务进行评价', '2');
INSERT INTO `t_setting` VALUES ('1AA63796BE0F4AACA0733EDC8A10599904', 'jianyue', 'jianyue', 'Q453781573', 'offdutyRejectTip', '当前是下班时间，暂无客服为您服务，请在工作时间周一至周五（上午09:00-12:00,下午14:00-18:00）联系我们！，<a href=\"javascript:toggleLw()\">我要留言</a>', '2');
INSERT INTO `t_setting` VALUES ('1CAC9543AF0B407D9BA984EF42DD464A17', 'jianyuekefu57714', 'jianyuekefu57714', '', 'callUrl', 'http://www.baidu.com', '1');
INSERT INTO `t_setting` VALUES ('2', 'defaultorg', 'defaultorg', null, 'serviceDay', '1,2,3,4,5', '0');
INSERT INTO `t_setting` VALUES ('21B45803728F46E7B2034EAFA765B23619', 'jianyue', 'jianyue', 'Q33845916', 'guidePopTip', '欢迎您的光临，如需更多服务请联系我们', '2');
INSERT INTO `t_setting` VALUES ('2218502C59B44F188B1762E5373FE01692', 'jianyue', 'jianyue', '', 'selfName', '小猪机器人', '1');
INSERT INTO `t_setting` VALUES ('23288312F9A341518DFF9C522588818928', 'jianyue', 'jianyue', '', 'selfErrorLimit', '3', '1');
INSERT INTO `t_setting` VALUES ('3', 'defaultorg', 'defaultorg', null, 'serviceTime', '09:00-12:00,14:00-18:00', '0');
INSERT INTO `t_setting` VALUES ('3325762D4497404BB33B37CDB1EBE91C25', 'jianyue', 'jianyue', '', 'selfUnknowTip', '我不太理解您的意思，请换个问法，或者咨询人工坐席', '1');
INSERT INTO `t_setting` VALUES ('35AF45DEAAD7495D99A77CB34842DF8D84', 'jianyuekefu57714', 'jianyuekefu57714', '', 'queueTalkingPolicy', '0', '1');
INSERT INTO `t_setting` VALUES ('361B088DE07442A1A47B756EE6FF5FD128', 'jianyuekefu57714', 'jianyuekefu57714', '', 'windowShowType', 'S', '1');
INSERT INTO `t_setting` VALUES ('3B1DD0CBAB6040EAB41E7C67766FCFA719', 'jianyuekefu57714', 'jianyuekefu57714', '', 'sessionDispatchPolicy', '0', '1');
INSERT INTO `t_setting` VALUES ('3D77DEA368FB4AEC9B3E1F654B59C2C536', 'jianyuekefu57714', 'jianyuekefu57714', '', 'queueLimit', '3', '1');
INSERT INTO `t_setting` VALUES ('3EB664859CE14B4DBF2191EAC82BCDD655', 'jianyuekefu57714', 'jianyuekefu57714', '', 'chatMaxLimit', '10', '1');
INSERT INTO `t_setting` VALUES ('4', 'defaultorg', 'defaultorg', null, 'offdutyReject', '', '0');
INSERT INTO `t_setting` VALUES ('4089FD7114C1492A96CA8332DB5FC5A281', 'jianyue', 'jianyue', '', 'keyWordToManual', '转人工,人工坐席', '1');
INSERT INTO `t_setting` VALUES ('442656F6D1C74E86B9D4CC53C2D2DCA800', 'jianyuekefu57714', 'jianyuekefu57714', '', 'selfAutoToManualTip', '现在系统为您转接人工坐席', '1');
INSERT INTO `t_setting` VALUES ('4994F98F9DFD4B39BCE2C946482B821952', 'jianyuekefu57714', 'jianyuekefu57714', '', 'agentTalkingPolicy', '1', '1');
INSERT INTO `t_setting` VALUES ('4AE83C7CA94849ECAB08539C16DAC55492', 'jianyuekefu57714', 'jianyuekefu57714', '', 'selfErrorLimit', '3', '1');
INSERT INTO `t_setting` VALUES ('4D3BE4FF5DCE4BA8AA51DD933D69EFF900', 'jianyue', 'jianyue', 'Q453781573', 'visitorEndSessionTip', '您已经主动结束了会话,感谢您的支持', '2');
INSERT INTO `t_setting` VALUES ('4DDBFF6AC7264E71BCE9FDC68D0E0F8B54', 'jianyuekefu57714', 'jianyuekefu57714', '', 'windowWidth', '400', '1');
INSERT INTO `t_setting` VALUES ('5', 'defaultorg', 'defaultorg', null, 'offdutyRejectTip', '当前是下班时间，暂无客服为您服务，请在工作时间周一至周五（上午09:00-12:00,下午14:00-18:00）联系我们！，<a href=\"javascript:toggleLw()\">我要留言</a>', '0');
INSERT INTO `t_setting` VALUES ('532C465A82DB4DE3BC40FE29A8DFFAD449', 'jianyue', 'jianyue', 'Q453781573', 'satisfyThanksTip', '感谢您的评价，我们将一如既往地努力', '2');
INSERT INTO `t_setting` VALUES ('55BB66A6B8D54909BD8301ED5AD6FDC526', 'jianyuekefu57714', 'jianyuekefu57714', '', 'callMethod', '', '1');
INSERT INTO `t_setting` VALUES ('57A804E40DD0480EA068E26B8087B31438', 'jianyuekefu57714', 'jianyuekefu57714', '', 'orgHeader', 'jianyuekefu57714-13631484267-37832378842.png', '1');
INSERT INTO `t_setting` VALUES ('5C3A61AD023441FBAC6A509FE61DE62F46', 'jianyue', 'jianyue', 'Q33845916', 'silentEndTip', '也许您已经休息了，那我就不再打扰您了，如有任何疑惑请随时联系我们', '2');
INSERT INTO `t_setting` VALUES ('5F0EDC168F944277B35DFF1FD3E4464954', 'jianyuekefu57714', 'jianyuekefu57714', '', 'selfUnknowTip', '我不太理解您的意思，请换个问法，或者咨询人工坐席', '1');
INSERT INTO `t_setting` VALUES ('5F1D11F0485349FA9ECE74141CC3648F92', 'jianyuekefu57714', 'jianyuekefu57714', '', 'beforeGroupList', '请选择技能组', '1');
INSERT INTO `t_setting` VALUES ('5F1D5876198E4CFFB290CF75617701F808', 'jianyue', 'jianyue', 'Q453781573', 'silentNoticeTip', '您已经休息了3分钟，请不要再沉默了，来撩我们吧', '2');
INSERT INTO `t_setting` VALUES ('6621C3D2CBE144138BAB29DF210B529823', 'jianyuekefu57714', 'jianyuekefu57714', '', 'guidePolicy', '3', '1');
INSERT INTO `t_setting` VALUES ('6A743F21B6C44E4687916020F132996205', 'jianyue', 'jianyue', 'Q453781573', 'agentEndSessionTip', '我们的会话已经结束，感谢您的咨询', '2');
INSERT INTO `t_setting` VALUES ('6AFE727EF2D34377AB25CF7AED5A960D18', 'jianyuekefu57714', 'jianyuekefu57714', '', 'accessKey', 'JIANYUE', '1');
INSERT INTO `t_setting` VALUES ('6D4DEDCEE2064BC7B210EEB2F122C91F15', 'jianyue', 'jianyue', 'Q453781573', 'visitorQueueTip', '您目前正在排队，会话被接入后坐席将回答您的问题，感谢您的支持', '2');
INSERT INTO `t_setting` VALUES ('6E98322D47BA45928D10816D100C257084', 'jianyue', 'jianyue', 'Q453781573', 'enterFirstTip', '你好，我是小猪，很高兴为您服务', '2');
INSERT INTO `t_setting` VALUES ('7', 'defaultorg', 'defaultorg', null, 'offdutyReceiptTip', '您好，当前是下班时间，客服人员较少，请你谅解', '0');
INSERT INTO `t_setting` VALUES ('70D0DF441C864A2B9AA2C7E97659F83401', 'ceshigongsi57403', 'ceshigongsi57403', '13631484269', 'chatMaxLimit', '0', '4');
INSERT INTO `t_setting` VALUES ('7112EFC308FD463BA131DAA0D2DE4D0812', 'jianyuekefu57714', 'jianyuekefu57714', '', 'historyMessagePolicy', '1', '1');
INSERT INTO `t_setting` VALUES ('73D82A3AB5D345A899CFC8BED3C4F06372', 'jianyue', 'jianyue', '', 'enterFirstTip', '你好，我是小猪，很高兴为您服务', '1');
INSERT INTO `t_setting` VALUES ('77284550BADD46A8BFFF3EA8A47B3BBF22', 'jianyue', 'jianyue', 'Q453781573', 'silentEndTip', '也许您已经休息了，那我就不再打扰您了，如有任何疑惑请随时联系我们', '2');
INSERT INTO `t_setting` VALUES ('792C338BD2844C1883C40F5B62DEF43788', 'jianyue', 'jianyue', 'Q33845916', 'enterFirstTip', '你好，我是小猪，很高兴为您服务', '2');
INSERT INTO `t_setting` VALUES ('7B917C2BBAEC4E65B3E5D7DFCA7DD2E947', 'jianyuekefu57714', 'jianyuekefu57714', '13631484267', 'chatMaxLimit', '7', '4');
INSERT INTO `t_setting` VALUES ('7D71DB5789F94B149C0833B2C52B6F4C40', 'jianyuekefu57714', 'jianyuekefu57714', '', 'loginDefaultStatus', 'ONLINE', '1');
INSERT INTO `t_setting` VALUES ('8', 'defaultorg', 'defaultorg', null, 'ondutyReceiptTip', '您好，非常欢迎您的咨询', '0');
INSERT INTO `t_setting` VALUES ('801F09A7B5BE4721B57F27C15D599CD402', 'jianyue', 'jianyue', 'Q33845916', 'offdutyRejectTip', '当前是下班时间，暂无客服为您服务，请在工作时间周一至周五（上午09:00-12:00,下午14:00-18:00）联系我们！，<a href=\"javascript:toggleLw()\">我要留言</a>', '2');
INSERT INTO `t_setting` VALUES ('85ED34271BB7483DA104E4CD02287E9D96', 'jianyuekefu57714', 'jianyuekefu57714', '', 'selfName', '小猪机器人', '1');
INSERT INTO `t_setting` VALUES ('88EBC37E5E3C4EB9AFE75290130A137536', 'jianyue', 'jianyue', 'Q33845916', 'overtimeEndSessionTip', '会话已经超过时间，如果有问题请继续联系客服咨询', '2');
INSERT INTO `t_setting` VALUES ('89AECE2E4B924264B97E899410B692D876', 'jianyue', 'jianyue', 'Q453781573', 'agentLeftTip', '坐席暂时离开，请稍等', '2');
INSERT INTO `t_setting` VALUES ('8AE99F98DCDD4C9CB6171314C26AC7CC64', 'jianyue', 'jianyue', 'Q33845916', 'ondutyReceiptTip', '您好，非常欢迎您的咨询', '2');
INSERT INTO `t_setting` VALUES ('9', 'defaultorg', 'defaultorg', null, 'agentEndSessionTip', '我们的会话已经结束，感谢您的咨询', '0');
INSERT INTO `t_setting` VALUES ('91200D3FCDD84C46BD98E0175A69464273', 'jianyuekefu57714', 'jianyuekefu57714', '', 'satisfyInviteTip', '尊敬的用户，请您对我的服务进行评价', '1');
INSERT INTO `t_setting` VALUES ('9405293AACF747AC970A1F6131E37D6D70', 'jianyue', 'jianyue', 'Q33845916', 'satisfyThanksTip', '感谢您的评价，我们将一如既往地努力', '2');
INSERT INTO `t_setting` VALUES ('9D14E8E1C8D442BD9EFC6296A2C6CCDC07', 'jianyuekefu57714', 'jianyuekefu57714', '', 'noOnlineAgentPolicy', '0', '1');
INSERT INTO `t_setting` VALUES ('9D523ADD9DBB4CB0B34C4FB461354DC057', 'jianyue', 'jianyue', 'Q33845916', 'visitorEndSessionTip', '您已经主动结束了会话,感谢您的支持', '2');
INSERT INTO `t_setting` VALUES ('9EA8BE3F0C7346BB9781EC787DEB966806', 'jianyuekefu57714', 'jianyuekefu57714', '', 'visitorQueuePolicy', '0', '1');
INSERT INTO `t_setting` VALUES ('A0308047BB944F4CAB6D9F8B3EA75EAA52', 'jianyuekefu57714', 'jianyuekefu57714', '', 'guidePopLater', '10', '1');
INSERT INTO `t_setting` VALUES ('A111C907CD7242959F2DD7FA286B24E943', 'jianyuekefu57714', 'jianyuekefu57714', '13631484268', 'chatMaxLimit', '16', '4');
INSERT INTO `t_setting` VALUES ('A12E14BE1DCB4CBFAF4685CC4433F23318', 'jianyue', 'jianyue', 'Q453781573', 'ondutyReceiptTip', '您好，非常欢迎您的咨询', '2');
INSERT INTO `t_setting` VALUES ('A4FCD25EBFC44D6CA0C1EE0D5337B88747', 'jianyue', 'jianyue', 'Q453781573', 'offdutyReceiptTip', '您好，当前是下班时间，客服人员较少，请你谅解', '2');
INSERT INTO `t_setting` VALUES ('AB1024764A0F44CE9CE9D8EED9F31B0588', 'jianyue', 'jianyue', 'Q33845916', 'silentNoticeTip', '您已经休息了3分钟，请不要再沉默了，来撩我们吧', '2');
INSERT INTO `t_setting` VALUES ('B18C6006EC064AB68811D88519A29B9F98', 'jianyuekefu57714', 'jianyuekefu57714', '', 'satisfyLimit', '5', '1');
INSERT INTO `t_setting` VALUES ('B645C9BDDBB7417A881A30E0A211CACE26', 'jianyue', 'jianyue', 'Q453781573', 'overtimeEndSessionTip', '会话已经超过时间，如果有问题请继续联系客服咨询', '2');
INSERT INTO `t_setting` VALUES ('B7737EF371464120B154E124CD80C7FB02', 'jianyuekefu57714', 'jianyuekefu57714', '', 'guideButtonText', '', '1');
INSERT INTO `t_setting` VALUES ('BA344E7332094452A15A170D9FA4050652', 'jianyuekefu57714', 'jianyuekefu57714', '', 'satisfyThanksTip', '感谢您的评价，我们将一如既往地努力', '1');
INSERT INTO `t_setting` VALUES ('BA5CDAA9F59D46C6A4C3D19499C96BAC22', 'jianyuekefu57714', 'jianyuekefu57714', '', 'windowBackgroundColor', '00A4F5', '1');
INSERT INTO `t_setting` VALUES ('BEA9BB55767D4FA3819CA7951B9A200747', 'jianyuekefu57714', 'jianyuekefu57714', '', 'guidePopTip', '欢迎您的光临，如需更多服务请联系我们', '1');
INSERT INTO `t_setting` VALUES ('C0062B349B1B41C2B9248AB013052BDD51', 'jianyue', 'jianyue', 'Q453781573', 'selfUnknowTip', '我不太理解您的意思，请换个问法，或者回复66咨询人工坐席', '2');
INSERT INTO `t_setting` VALUES ('C1967E09A15249E384AFFF9449199B2240', 'jianyuekefu57714', 'jianyuekefu57714', '', 'firstMenuTag', '常见问题', '1');
INSERT INTO `t_setting` VALUES ('C3210F5F64A541B081D34B423ABBE4B021', 'jianyuekefu57714', 'jianyuekefu57714', '', 'satisfyInput', '1', '1');
INSERT INTO `t_setting` VALUES ('C3BA28268B2C4F2E88B6F9323201773D96', 'jianyue', 'jianyue', 'Q33845916', 'agentEndSessionTip', '我们的会话已经结束，感谢您的咨询', '2');
INSERT INTO `t_setting` VALUES ('C6B5970022854AF0B9D1EA06AB175CBA97', 'jianyuekefu57714', 'jianyuekefu57714', '', 'selfSendQuestion', '', '1');
INSERT INTO `t_setting` VALUES ('CF460D10A09447178B6872F365DCF3CB98', 'jianyue', 'jianyue', '', 'robotType', '1', '1');
INSERT INTO `t_setting` VALUES ('D4D51519535A4B0A88C71F37E09392F383', 'jianyuekefu57714', 'jianyuekefu57714', '', 'keyWordToManual', '转人工,人工坐席,有意见', '1');
INSERT INTO `t_setting` VALUES ('D6C6B8A29ED14E43A2A47F61F55CB5C207', 'jianyue', 'jianyue', 'Q33845916', 'visitorQueueTip', '您目前正在排队，会话被接入后坐席将回答您的问题，感谢您的支持', '2');
INSERT INTO `t_setting` VALUES ('D7F49D2524A042C4B0A4D02EEE0F868066', 'jianyuekefu57714', 'jianyuekefu57714', '', 'enterFirstTip', '你好，我是小猪，很高兴为您服务', '1');
INSERT INTO `t_setting` VALUES ('DA0C3CED6EA94B16BA2F80250029D03801', 'jianyuekefu57714', 'jianyuekefu57714', '', 'agentLogout', '0', '1');
INSERT INTO `t_setting` VALUES ('DA806FCB16BC444A98FB1599B6F7AF5311', 'jianyue', 'jianyue', 'Q453781573', 'satisfyInviteTip', '尊敬的用户，请您对我的服务进行评价', '2');
INSERT INTO `t_setting` VALUES ('DAB8B02365894CC7A2A7B0105570645558', 'jianyuekefu57714', 'jianyuekefu57714', '', 'chatMaxType', '0', '1');
INSERT INTO `t_setting` VALUES ('DD2EDB3197D14408BB04C47B4252C30E86', 'jianyue', 'jianyue', '', 'selfPolicy', '1', '1');
INSERT INTO `t_setting` VALUES ('E57370D2878040FAAD3AD896B51B2AB582', 'jianyue', 'jianyue', 'Q33845916', 'selfUnknowTip', '我不太理解您的意思，请换个问法，或者回复66咨询人工坐席', '2');
INSERT INTO `t_setting` VALUES ('E9C13511B8FF432AAE5DD424B8FB0C5B64', 'jianyue', 'jianyue', '', 'selfAutoToManualTip', '现在系统为您转接人工坐席', '1');
INSERT INTO `t_setting` VALUES ('EBD8AAD448E2463081E7DED8B3E8BD7784', 'jianyue', 'jianyue', 'Q33845916', 'offdutyReceiptTip', '您好，当前是下班时间，客服人员较少，请你谅解', '2');
INSERT INTO `t_setting` VALUES ('F0EAA18004504F79A1F0561213B4DA7583', 'jianyue', 'jianyue', 'Q453781573', 'noAgentOnlineTip', '当前无客服在线，请稍后访问我们，感谢您的支持', '2');
INSERT INTO `t_setting` VALUES ('F37F5794BEE44CCC8BE2C4EE1C5C9FC848', 'jianyuekefu57714', 'jianyuekefu57714', '', 'supportHolder', '简约客服 13631484267', '1');
INSERT INTO `t_setting` VALUES ('F5085B0251F4417EBEA0E6FA5671710144', 'jianyue', 'jianyue', 'Q453781573', 'guidePopTip', '欢迎您的光临，如需更多服务请联系我们', '2');
INSERT INTO `t_setting` VALUES ('F8BE5F6EC214487AA5996B2E67D8D70A50', 'jianyuekefu57714', 'jianyuekefu57714', '', 'buttonTag', '快速访问', '1');
INSERT INTO `t_setting` VALUES ('FB097BF4E18742558F39161BD8802BE979', 'jianyue', 'jianyue', 'Q33845916', 'agentLeftTip', '坐席暂时离开，请稍等', '2');

-- ----------------------------
-- Table structure for `t_student`
-- ----------------------------
DROP TABLE IF EXISTS `t_student`;
CREATE TABLE `t_student` (
  `id` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `age` int(11) DEFAULT NULL,
  `birthday` varchar(100) DEFAULT NULL,
  `level` varchar(10) DEFAULT NULL,
  `sex` varchar(10) DEFAULT NULL,
  `t` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=gb2312;

-- ----------------------------
-- Records of t_student
-- ----------------------------
INSERT INTO `t_student` VALUES ('07841F7F5FF84D69B80A4DA273A34C2038', '1', '1', '2018-08-09', '2', '女', '22');
INSERT INTO `t_student` VALUES ('1729560D3F0A462E9B5D6CE5C713DE5920', 'OK', '12', '', '2', '女', '22');
INSERT INTO `t_student` VALUES ('21B1D62E4B764A4DB8327C2EF8DFD3A517', '435', '34543', '', '2', '女', '22');
INSERT INTO `t_student` VALUES ('34D3E8F7E7024D1EB548214955AFCC2159', '900', '900', '', '3', '女', '22');
INSERT INTO `t_student` VALUES ('37DAE187895F4606AD2CBFD930A7E70E59', '32432444', '234244', '', '2', '女', '22');
INSERT INTO `t_student` VALUES ('3AE1914006B74006A8F731D57AE3049A71', '2', '2', '', '2', '女', '22');
INSERT INTO `t_student` VALUES ('47F33608E3534BB183103F2A3447DE8C12', '测试', '123', '', '1', '女', '22');
INSERT INTO `t_student` VALUES ('48FBA62F2F214FC8814F8DF91B07A8AB45', '32432432', '432432', '2018-08-02', '2', '女', '22');
INSERT INTO `t_student` VALUES ('4F76DF86AF1548C68E20F086D247F69F13', '60', '698', '2018-07-09', '3', '男', '01');
INSERT INTO `t_student` VALUES ('5415656970EC4EA19F72BE21A820E2FB21', '12123', '123', '', '2', '女', '22');
INSERT INTO `t_student` VALUES ('5EB782C95A0948F7B261D348778A705390', '12', '22', '2018-08-03', '2', '女', '01,03,05,22');
INSERT INTO `t_student` VALUES ('643C09D4687C489E96275E7C6AAD457D57', '90033', '33', '2018-07-10', '2', '女', '22');
INSERT INTO `t_student` VALUES ('77970590551A424688E96A7EF1D539A080', '郑余杰', '2', '2018-08-08', '2', '男', '01');
INSERT INTO `t_student` VALUES ('77D0496D00EE4FF6B14489B41758ECD609', '1', '1', '', '2', '男', '22');
INSERT INTO `t_student` VALUES ('78C27DDC78EC43C3B79963C272B252AF87', '4', '4', '2018-07-09', '1', '男', '01');
INSERT INTO `t_student` VALUES ('853326BB15A04AA1B2729E1EBC6FF78359', '32432', '2342', '', '2', '女', '22');
INSERT INTO `t_student` VALUES ('9692B116DA64443989F343A4F2871E7487', '23234', '444', '2018-08-10', '2', '女', '22');
INSERT INTO `t_student` VALUES ('9C27EB0FDAD84FF6B815A870FB92F27E19', '4354', '34543', '', '2', '女', '22');
INSERT INTO `t_student` VALUES ('AE17CC14A706414AAD7B8794F77D7D8E29', '1234123', '213', '2018-07-26', '2', '女', '22');
INSERT INTO `t_student` VALUES ('E5E9DCD978B6462AB5D9178C886BF17569', '23432432', '423432432', '2018-08-10', '2', '女', '22');
INSERT INTO `t_student` VALUES ('E60E04F22CE048BA9CA9B66C3139DB4553', '3243432', '432432', '2018-08-02', '2', '女', '22');

-- ----------------------------
-- Table structure for `t_tag`
-- ----------------------------
DROP TABLE IF EXISTS `t_tag`;
CREATE TABLE `t_tag` (
  `id` varchar(100) NOT NULL,
  `org` varchar(100) NOT NULL,
  `no` int(11) NOT NULL,
  `tag_txt` varchar(100) NOT NULL,
  `father_id` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=gbk;

-- ----------------------------
-- Records of t_tag
-- ----------------------------

-- ----------------------------
-- Table structure for `t_trace`
-- ----------------------------
DROP TABLE IF EXISTS `t_trace`;
CREATE TABLE `t_trace` (
  `id` varchar(100) NOT NULL,
  `org` varchar(100) NOT NULL,
  `pid` varchar(100) NOT NULL,
  `crm` varchar(100) DEFAULT NULL,
  `title` varchar(1000) DEFAULT NULL,
  `url` varchar(100) DEFAULT NULL,
  `logon` varchar(1) DEFAULT NULL,
  `create_time` bigint(20) NOT NULL,
  `request_agent` varchar(1) DEFAULT NULL,
  `http_session_id` varchar(100) NOT NULL,
  `ip` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=gb2312;

-- ----------------------------
-- Records of t_trace
-- ----------------------------

-- ----------------------------
-- Table structure for `t_user`
-- ----------------------------
DROP TABLE IF EXISTS `t_user`;
CREATE TABLE `t_user` (
  `username` varchar(80) NOT NULL,
  `org` varchar(100) NOT NULL,
  `pid` varchar(100) NOT NULL,
  `type` varchar(1) NOT NULL,
  `password` varchar(80) NOT NULL,
  `level` int(11) NOT NULL DEFAULT '1',
  `no` varchar(80) DEFAULT NULL,
  `nick_name` varchar(80) DEFAULT NULL,
  `name` varchar(80) NOT NULL,
  `phone` varchar(80) DEFAULT NULL,
  `email` varchar(80) DEFAULT NULL,
  `insert_time` datetime NOT NULL,
  `status` varchar(1) NOT NULL,
  `tmp` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`username`),
  UNIQUE KEY `IDX_phone` (`phone`)
) ENGINE=InnoDB DEFAULT CHARSET=gbk;

-- ----------------------------
-- Records of t_user
-- ----------------------------
INSERT INTO `t_user` VALUES ('13631484266', 'jianyue', 'jianyue', 'm', '123456', '1', '10002', null, '10002', '13631484266', null, '2018-02-25 14:16:12', 'Y', null);
INSERT INTO `t_user` VALUES ('13631484267', 'jianyue', 'jianyue', 'm', '123456', '1', '10001', 'jianyue', '管理员', '13631484267', '', '2018-01-14 14:53:52', 'Y', null);
INSERT INTO `t_user` VALUES ('13922739111', 'guangzhouqili04644', 'guangzhouqili04644', 'm', 'sales727', '1', '10001', '广州奇利', '管理员', '13922739111', '', '2018-01-15 10:47:31', 'Y', null);
INSERT INTO `t_user` VALUES ('zhengyujie', 'jianyue', 'jianyue', 'm', '123456', '1', '10003', '一头猪', '郑余杰', 'zhengyujie', null, '2018-06-08 19:11:48', 'Y', null);

-- ----------------------------
-- Table structure for `t_user_copy`
-- ----------------------------
DROP TABLE IF EXISTS `t_user_copy`;
CREATE TABLE `t_user_copy` (
  `username` varchar(80) NOT NULL,
  `org` varchar(100) NOT NULL,
  `pid` varchar(100) NOT NULL,
  `type` varchar(1) NOT NULL,
  `password` varchar(80) NOT NULL,
  `level` int(11) NOT NULL DEFAULT '1',
  `no` varchar(80) DEFAULT NULL,
  `nick_name` varchar(80) DEFAULT NULL,
  `name` varchar(80) NOT NULL,
  `phone` varchar(80) DEFAULT NULL,
  `email` varchar(80) DEFAULT NULL,
  `insert_time` datetime NOT NULL,
  `status` varchar(1) NOT NULL,
  `tmp` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`username`),
  UNIQUE KEY `IDX_phone` (`phone`)
) ENGINE=InnoDB DEFAULT CHARSET=gbk;

-- ----------------------------
-- Records of t_user_copy
-- ----------------------------
INSERT INTO `t_user_copy` VALUES ('13631484266', 'jianyue', 'jianyue', 'm', '123456', '1', '10002', null, '10002', '13631484266', null, '2018-02-25 14:16:12', 'Y', null);
INSERT INTO `t_user_copy` VALUES ('13631484267', 'jianyue', 'jianyue', 'm', '123456', '1', '10001', 'jianyue', '管理员', '13631484267', '', '2018-01-14 14:53:52', 'Y', null);
INSERT INTO `t_user_copy` VALUES ('13922739111', 'guangzhouqili04644', 'guangzhouqili04644', 'm', 'sales727', '1', '10001', '广州奇利', '管理员', '13922739111', '', '2018-01-15 10:47:31', 'Y', null);
INSERT INTO `t_user_copy` VALUES ('zhengyujie', 'jianyue', 'jianyue', 'm', '123456', '1', '10003', '一头猪', '郑余杰', 'zhengyujie', null, '2018-06-08 19:11:48', 'Y', null);

-- ----------------------------
-- Table structure for `t_user_copy1`
-- ----------------------------
DROP TABLE IF EXISTS `t_user_copy1`;
CREATE TABLE `t_user_copy1` (
  `username` varchar(80) NOT NULL,
  `org` varchar(100) NOT NULL,
  `pid` varchar(100) NOT NULL,
  `type` varchar(1) NOT NULL,
  `password` varchar(80) NOT NULL,
  `level` int(11) NOT NULL DEFAULT '1',
  `no` varchar(80) DEFAULT NULL,
  `nick_name` varchar(80) DEFAULT NULL,
  `name` varchar(80) NOT NULL,
  `phone` varchar(80) DEFAULT NULL,
  `email` varchar(80) DEFAULT NULL,
  `insert_time` datetime NOT NULL,
  `status` varchar(1) NOT NULL,
  `tmp` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`username`),
  UNIQUE KEY `IDX_phone` (`phone`)
) ENGINE=InnoDB DEFAULT CHARSET=gbk;

-- ----------------------------
-- Records of t_user_copy1
-- ----------------------------
INSERT INTO `t_user_copy1` VALUES ('13631484266', 'jianyue', 'jianyue', 'm', '123456', '1', '10002', null, '10002', '13631484266', null, '2018-02-25 14:16:12', 'Y', null);
INSERT INTO `t_user_copy1` VALUES ('13631484267', 'jianyue', 'jianyue', 'm', '123456', '1', '10001', 'jianyue', '管理员', '13631484267', '', '2018-01-14 14:53:52', 'Y', null);
INSERT INTO `t_user_copy1` VALUES ('13922739111', 'guangzhouqili04644', 'guangzhouqili04644', 'm', 'sales727', '1', '10001', '广州奇利', '管理员', '13922739111', '', '2018-01-15 10:47:31', 'Y', null);
INSERT INTO `t_user_copy1` VALUES ('zhengyujie', 'jianyue', 'jianyue', 'm', '123456', '1', '10003', '一头猪', '郑余杰', 'zhengyujie', null, '2018-06-08 19:11:48', 'Y', null);

-- ----------------------------
-- Table structure for `t_worder`
-- ----------------------------
DROP TABLE IF EXISTS `t_worder`;
CREATE TABLE `t_worder` (
  `id` varchar(100) NOT NULL,
  `org` varchar(100) NOT NULL,
  `pid` varchar(100) NOT NULL,
  `sn` varchar(100) NOT NULL,
  `status` varchar(10) NOT NULL,
  `level` int(11) DEFAULT NULL,
  `crm` varchar(100) NOT NULL,
  `fagent` varchar(100) DEFAULT NULL,
  `to_agent` varchar(100) DEFAULT NULL,
  `subject` text,
  `context` text,
  `create_source` varchar(10) DEFAULT NULL,
  `create_object` varchar(100) DEFAULT NULL,
  `create_time` bigint(20) NOT NULL,
  `tag1` varchar(100) DEFAULT NULL,
  `tag2` varchar(100) DEFAULT NULL,
  `tag3` varchar(100) DEFAULT NULL,
  `tag4` varchar(100) DEFAULT NULL,
  `extend1` varchar(1000) DEFAULT NULL,
  `extend2` varchar(1000) DEFAULT NULL,
  `extend3` varchar(1000) DEFAULT NULL,
  `extend4` varchar(1000) DEFAULT NULL,
  `extend5` varchar(1000) DEFAULT NULL,
  `extend6` varchar(1000) DEFAULT NULL,
  `extend7` varchar(1000) DEFAULT NULL,
  `extend8` varchar(1000) DEFAULT NULL,
  `extend9` varchar(1000) DEFAULT NULL,
  `extend10` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=gbk;

-- ----------------------------
-- Records of t_worder
-- ----------------------------

-- ----------------------------
-- Table structure for `t_worder_history`
-- ----------------------------
DROP TABLE IF EXISTS `t_worder_history`;
CREATE TABLE `t_worder_history` (
  `id` varchar(100) NOT NULL,
  `order_id` varchar(100) NOT NULL,
  `crm` varchar(100) NOT NULL,
  `oper_user` varchar(100) DEFAULT NULL,
  `oper_type` varchar(10) NOT NULL,
  `oper_content` text NOT NULL,
  `oper_time` varchar(19) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_worder_history
-- ----------------------------
