var editorTempRemark;
function loadTinymce(div) {
	tinymce.init({
		selector: '#' + div,
		menubar: false,
		theme: 'modern',
		visual_table_style: 'border: 0px dashed #BBBBBB',
		height: '170',
		width : '800px',
		plugins: [
			'advlist autolink lists link image charmap print preview hr anchor pagebreak',
			'searchreplace visualblocks visualchars code fullscreen',
			'insertdatetime media nonbreaking save table contextmenu directionality',
			'emoticons template paste textcolor colorpicker textpattern imagetools '
		],//wordcount 
		toolbar1: 'mybutton | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | forecolor backcolor size fontsize | formatselect ',
		paste_data_images: true,
		setup: function(editor) {
			editorTempRemark = editor;
			editor.addButton('mybutton', {
				text: '图片/文件',
				icon: false,
				onclick: function() {
					layer.open({
						type: 2,
						title: '选择图片或者文件',
						shadeClose: true,
						shade: 0.8,
						area: ['80', '20'],
						content: '../../self/UploadFile.jsp'
					});
				}
			});
		},
		language: "zh_CN"
	});
}

function insertImage(url, width, height) {
	if(editorTempRemark) {
		editorTempRemark.insertContent("<a href='" + url + "' target='_blank'><img src=" + url + " width='" + width + "' height='" + height + "'></a>");
	}
}

function insertFile(url, name) {
	if(editorTempRemark) {
		editorTempRemark.insertContent("<a href='" + url + "' target='_blank'>点击下载文件[" + name + "]</a>");
	}
}