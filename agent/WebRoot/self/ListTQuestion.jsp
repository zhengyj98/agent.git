<%@ page contentType="text/html; charset=UTF-8" language="java"  errorPage="" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.cellcom.com.cn/myjsp" prefix="myjsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>自助聊天管理</title>
	<link rel="stylesheet" href="../themes/skyblue/skyblueMain.css" type="text/css" title="styles1" />    
	
	
	<!--请在下面增加js-->
   	<script type="text/javascript" src="../js/mycommon.jquery.js"></script>
	<script type="text/javascript" src="../plugins/datepicker/WdatePicker.js"></script>
	<script type="text/javascript" src='../plugins/jquery-x.x.js'> </script>	

</head>

<body>
	<html:form action="/self/TQuestionDpAct?method=list" styleId="TQuestionForm">
		<html:hidden property='type' styleId="type"/>
		<html:hidden property='level' styleId="level"/>
		<html:hidden property="subject" styleId="subject"/>		
		<div id="main">
			<div id="tab-top">
				<div id="lift"></div>
				<div id="pt">自助聊天管理</div>
				<div id="right"></div>
			</div>		
			
			<div id="table">
				<div id="ptk"> <div id="tabtop-l"> </div>
				<div id="tabtop-z">输入查询条件</div>
				<div id="tabtop-r1"></div></div>
			</div>
		
		
			<div id="main-tab">
				<div id="info-4">           
					<li>问题号：<html:text property='no' styleClass='s-input-2' /></li>
					<li>问题：<html:text property='question' styleClass='s-input-2' /></li>
					<li>答案：<html:text property='answer' styleClass='s-input-2' /></li>
					<li style="width:80px"><html:submit styleClass="search-2" value="查询" /></li>	
					<li style="width:80px"></li>			
				</div>          
			</div>
		
			<div id="table">
				<div id="ptk"> 
					<div id="tabtop-l"> </div>
					<div id="tabtop-z">当前查询结果</div>
					<li><input class='search-2' type='button' value='新增' onClick='window.location="../self/TQuestionDpAct.do?method=add&type=${TQuestionForm.type}"'/></li>
					<li><input class='search-2' type='button' value='删除选中' onClick='return deleteSelected("../self/TQuestionDpAct.do?method=doDelete")'/></li>
					<li></li>					
				</div>					
			</div>
		
			<div id="fh-flie-2"> </div>
			<div id="main-tablist">
				<table width='100%' border='0' cellpadding='0' cellspacing='0' id='showData'>
					<tr id='minpt-tab'>
						<td>选择</td>
						<td>问题号</td>
						<td>问题</td>
						<td>答案</td>
						<td>插入</td>
						<c:if test="${TQuestionForm.type == 'F'}">
							<td>提问次数</td>
						</c:if>
						<td>操作</td>
					</tr>

					<c:forEach var="aRecord" items="${result}">
						<tr class='out' onMouseOver='this.className="over"' onMouseOut='this.className="out"'>
							<td class='zw-txt'><html:checkbox property='ids' value='${aRecord.id}' styleId="${aRecord.id}"/></td>
							<td class='zw-txt'>${aRecord.no}</td>
							<td class='zw-txt' width="25%">${aRecord.question}</td>
							<td class='zw-txt' width="50%">${aRecord.answer}</td>
							<td class='zw-txt'>${aRecord.insertTime}</td>
							<c:if test="${TQuestionForm.type == 'F'}">
								<td class='zw-txt'>${aRecord.times}</td>
							</c:if>	
							<td class='zw-txt'><a href='../self/TQuestionDpAct.do?method=modify&id=${aRecord.id} '>修改</a></td>
						</tr>
					</c:forEach>
				</table>
				
				<div id="info-pz"> 
					${link}
				</div>
			</div>
		</div>
	</html:form>
</body>
</html>
