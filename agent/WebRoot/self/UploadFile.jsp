<%@ page contentType="text/html; charset=UTF-8" language="java"  errorPage="" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.cellcom.com.cn/myjsp" prefix="myjsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>请选择文件</title>
	
	<!--请在下面增加js-->
	<script type="text/javascript" src='../plugins/jquery-x.x.js'> </script>	
	<script type="text/javascript" src='../plugins/jquery.form.js'> </script>
   	<script type="text/javascript" src="../js/mycommon.jquery.js"></script>
   	<script type="text/javascript" src="../js/my.upload.js"></script>
	<script type="text/javascript" src="../plugins/datepicker/WdatePicker.js"></script>
   	<script type="text/javascript" src="../js/online/constant.js"></script>	
   	<script type="text/javascript" src="../js/online/all-show.js"></script>	
	<script>
		function uploadFile(token) {
			var params = {
				token : token,
				uuid : "uuid-" + (new Date().getTime()),
				bizType : "QUESTION",
				multType : "IMG",
				uploadUrl : UPLOAD_URL
			};
			MyUpload.uploadFile(params, $("#CommonForm"), function(resp) {
				if(resp.code == 0) {
					var f = QUESTION_FILE_PATH + "&pid=${attribute.pid}&name=" + resp.oldName + "&fileName=" + resp.newName;
					if(resp.newName.toLowerCase().endsWith(".png") || resp.newName.toLowerCase().endsWith(".jpg") || resp.newName.toLowerCase().endsWith(".gif")) {
						//$("#showFile").attr("src", f);
						var arr = adapterSize(resp, SHOW_PIC.w, SHOW_PIC.h);
						parent.insertImage(f, arr[0], arr[1]);
					} else {
						parent.insertFile(f, resp.oldName);
					}
					parent.layer.closeAll();
				} else {
					parent.layer.msg(TEXT.DO_ERROR);
				}
			});
		}
	</script>

</head>

<body>
	<form action="/base/Nothing" id="CommonForm" enctype="multipart/form-data">
		<table width='100%' border='0' cellpadding='0' cellspacing='0' id='showData'>
				<tr>
					<td class='zw-txt' align='left'>
						<input id="file" name="file" type="file" value="" onchange="uploadFile('${token}')" style="width:69px"/>
					</td>
				</tr>				
				<!-- 
				<tr>
					<td class='zw-txt' align='left'>
						<img src="" id="showFile" width="50px" height="50px"/>
					</td>
				</tr>
				 -->				
		</table>
	</form>
</body>
</html>
