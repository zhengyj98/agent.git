<%@ page contentType="text/html; charset=GBK" language="java"  errorPage="" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.cellcom.com.cn/myjsp" prefix="myjsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=GBK" />
	<title>导入信息</title>
	<link rel="stylesheet" href="../themes/skyblue/skyblueMain.css" type="text/css" title="styles1" />    
	
	<!--请在下面增加js-->
   	<script type="text/javascript" src="../js/mycommon.js"></script>
	<script type="text/javascript" src="../plugins/datepicker/WdatePicker.js"></script>
	<script type="text/javascript" src='../plugins/jquery-x.x.js'> </script>
	<script type="text/javascript" src="../js/jquery.validate.js"></script>
	<script type="text/javascript" src="../js/jquery.mine.js"></script>
	<script type="text/javascript" src="../js/#{filename}.js"></script>	
</head>

<body>
<div id="main">
	<div id="tab-top">
		<div id="ptk1">
			<div id="lift"></div>
			<div id="pt">导入数据，请按模板输入信息</div>
			<div id="right"></div>
		</div>
	</div>
	<div id="table">
		<div id="ptk">
			<div id="tabtop-l"></div>
			<div id="tabtop-z">导入信息</div>
			<div id="tabtop-r1"></div>
		</div>
	</div>
	<div id="main-tab">
		<html:form action="/#{prefix}/#{filename}DpAct?method=doImporter" method="post" enctype="multipart/form-data" styleId="#{pojo}Form">
			<p align="center">${result}</p>		
				<table width="70%" height="100" border="0" align="center" cellpadding="0" cellspacing="0" class="tab-sy">
					<tr>
						<td>模板：</td>
						<td><a href="../#{prefix}/#{filename}DpAct.do?method=template">导入必须按照模板格式输入，点击下载模板</a></td>
					</tr>
					<tr>
						<td width="30%">选择：</td>
						<td>
						#{importerrows}						
						</td>						
					</tr>					
					<tr>
						<td width="30%">选择文件：</td>
						<td><input name="importerfile" type="file" style="width:400px"/></td>						
					</tr>
				</table>	
			<li style="width:300px">&nbsp;</li>
			<li><input type="button" value="返 回" onclick="history.back()" class="search-2" tyle="cursor:pointer"/></li>
			<li style="width:20px">&nbsp;</li>
			<li><input type="submit" value="导 入" class="search-2" tyle="cursor:pointer"/></li>				
		</html:form>
	</div>

</div>
</body>
</html>
