package ${args.packageOfAction};

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

import cn.cellcom.jar.biz.AbstractBiz;
import cn.cellcom.jar.biz.AbstractBiz.LOGTYPE;
import cn.cellcom.jar.logon.LogonSession;
import cn.cellcom.jar.dao.IBaseDao;
import cn.cellcom.jar.env.Env;
import cn.cellcom.jar.file.FU;
import cn.cellcom.jar.pagination.AbstractPaginationBiz;
import cn.cellcom.jar.pagination.IPaginationBiz;
#if(${table.export} or ${table.importer})
import cn.cellcom.jar.file.FileLoad;
import cn.cellcom.jar.file.FileProcessor;
import cn.cellcom.jar.file.IFileLoad;
import cn.cellcom.jar.file.IFileProcessor;

import cn.cellcom.jar.port.ExportTemplate;
import cn.cellcom.jar.port.IDataPort;
import cn.cellcom.jar.port.ImportTemplate;
import cn.cellcom.jar.port.JxlPort;
#end
#if(${table.mpojo})	
import cn.cellcom.jar.util.APojo;
import cn.cellcom.jar.util.PojoUtil;
#end	
import cn.cellcom.jar.util.BeanUtil;
import cn.cellcom.jar.util.CU;
import cn.cellcom.jar.util.LogUtil;
import cn.cellcom.jar.util.ConversionUtils;
import cn.cellcom.jar.util.DT;
import cn.cellcom.jar.util.LogUtil;
import cn.cellcom.jar.util.MyException;
import ${args.packageOfHql}.${table.filename}Hql;
import ${args.packageOfPojo}.${table.pojo};
#if(${extend.keySetterForDetail})
import ${args.packageOfPojo}.${extend.keyPojo};
#end
import ${args.packageOfForm}.${table.filename}Form;

public class ${table.filename}DpAct extends DispatchAction {

	private IPaginationBiz pbiz;

	private AbstractBiz cbiz;

	private Log log = LogFactory.getLog(this.getClass());

	/**
	 * 查询记录
	 * 
	 * @param mapping
	 * @param fm
	 * @param req
	 * @param rep
	 * @return
	 */
	public ActionForward list(ActionMapping mapping, ActionForm form,
			HttpServletRequest req, HttpServletResponse rep) {
		form = AbstractPaginationBiz.getPaginationActionForm(req,form);
		List list = null;
		try {
			log.info("查询的语句：${table.filename}Hql()");
			list = pbiz.pagination(mapping, form, req, new ${table.filename}Hql()).getDataResult();

			#if(${table.mpojo})		
				APojo[] pojos = new APojo[] {
				#foreach($field in ${extend.forpojolist})
					#if(${velocityCount} == ${extend.forpojolist.size()})
						new APojo(${field.sn}, "${field.field}", null)
					#else
						new APojo(${field.sn}, "${field.field}", null),
					#end
				#end
				};
				list = PojoUtil.arraylist2pojolist(list, pojos);			
			#end
			req.setAttribute(Env.DATA_NAME, list);
		} catch (MyException e) {
			return LogUtil.e(this.clazz, "操作出现错误", e, mapping, req);
		}
		#foreach($readbean in ${extend.readBeanUtilForList})
			##req.setAttribute("${readbean.field}", req.getParameter("${readbean.field}"));
			req.setAttribute("${readbean.field}list",  BeanUtil.startWith("${readbean.pojo}-${readbean.field}"));
		#end
		#foreach($readbean in ${extend.readContextUtilForList})
			##req.setAttribute("${readbean.field}", req.getParameter("${readbean.field}"));
			//${readbean.field}list是从context中读取的，这里无需有读取代码
		#end
		//存在读取数据库的配置
		#if(${extend.readDbForList.size()}>0)
			try {
				#foreach($readdb in ${extend.readDbForList})
					#if(${readdb.specialValuesSpiltBySemicolon.size()} <3)
						//${readdb.field}配置查询关联表的特殊处理值参数错误，应该填写：pojo;value;showname
					#else			
						LogonSession ls = new LogonSession(req);
						#if(${readdb.specialValuesSpiltBySemicolon.size()}>3)
							String hql = "from ${readdb.specialValuesSpiltBySemicolon.get(0)} where ${readdb.specialValuesSpiltBySemicolon.get(3)}=?";
							#if(${readdb.specialValuesSpiltBySemicolon.size()}>4)
								Object[] obj${velocityCount} = new Object[]{${readdb.specialValuesSpiltBySemicolon.get(4)}};
							#end
						#else
							Object[] obj${velocityCount} = null;
							String hql = "from ${readdb.specialValuesSpiltBySemicolon.get(0)}";					
						#end
						List list${velocityCount} = cbiz.getDao().myList(hql, obj${velocityCount});					
						req.setAttribute("${readdb.field}list", BeanUtil.toABeanList(list${velocityCount}, "${readdb.specialValuesSpiltBySemicolon.get(1)}", "${readdb.specialValuesSpiltBySemicolon.get(2)}"));
					#end
				#end						
			} catch (MyException e) {
				return LogUtil.e(this.clazz, "读取相关数据出现错误", e, mapping, req);
			}
		#end
				
		#if(${table.getList().getAjax()})
		//采用ajax返回		
			rep.setContentType("text/xml;charset=UTF-8");
			try {
			StringBuffer sb = new StringBuffer("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			sb.append("<${table.pojo}>");	
				for (int i = 0; list != null && i < list.size(); i++) {
					${table.pojo} info = (${table.pojo}) list.get(i);
					String val = "";
					#foreach(${arecord} in ${extend.propertiesForForm})
						val = String.valueOf(info.${arecord.getter}());
						sb.append("<${arecord.property}>").append(val).append("</${arecord.property}>");
					#end
				}
				sb.append("</${table.pojo}>");
				rep.getOutputStream().write(sb.toString().getBytes());
			} catch (IOException e) {
				return LogUtil.e(this.clazz, "操作出现错误", e, mapping, req);
			}
		#else
			return mapping.findForward("list");		
		#end			
	}

	#if(${table.add})
	/**
	 * 准备记录添加
	 * 
	 * @param mapping
	 * @param fm
	 * @param req
	 * @param rep
	 * @return
	 */	public ActionForward add(ActionMapping mapping, ActionForm form,
			HttpServletRequest req, HttpServletResponse rep) {
		#foreach($readbean in ${extend.readBeanUtilForAdd})
			req.setAttribute("${readbean.field}list",  BeanUtil.startWith("${readbean.pojo}-${readbean.field}"));
		#end
		#foreach($readbean in ${extend.readContextUtilForAdd})
			//${readbean.field}list是从context中读取的，这里无需有读取代码
		#end
		
		#if(${extend.readDbForAdd.size()}>0)
		LogonSession ls = new LogonSession(req);
		try {
			#foreach($readdb in ${extend.readDbForAdd})
				#if(${readdb.specialValuesSpiltBySemicolon.size()} <3)				
					//${readdb.field}配置查询关联表的特殊处理值参数错误，应该填写：pojo;value;showname
				#else								
					#if(${readdb.specialValuesSpiltBySemicolon.size()}>3)
						String hql${velocityCount} = "from ${readdb.specialValuesSpiltBySemicolon.get(0)} where ${readdb.specialValuesSpiltBySemicolon.get(3)}=?";
						#if(${readdb.specialValuesSpiltBySemicolon.size()}>4)
							Object[] obj${velocityCount} = new Object[]{${readdb.specialValuesSpiltBySemicolon.get(4)}};
						#end
					#else
						Object[] obj${velocityCount} = null;
						String hql${velocityCount} = "from ${readdb.specialValuesSpiltBySemicolon.get(0)}";					
					#end
					List list${velocityCount} = cbiz.getDao().myList(hql${velocityCount}, obj${velocityCount});					
					req.setAttribute("${readdb.field}list", BeanUtil.toABeanList(list${velocityCount}, "${readdb.specialValuesSpiltBySemicolon.get(1)}", "${readdb.specialValuesSpiltBySemicolon.get(2)}"));
				#end
			#end
			
		} catch (MyException e) {
			return LogUtil.e(this.clazz, "读取相关数据出现错误", e, mapping, req);			
		}
		#end
		return mapping.findForward("add");
	}


	/**
	 * 执行记录添加
	 * 
	 * @param mapping
	 * @param fm
	 * @param req
	 * @param rep
	 * @return
	 */
	public ActionForward doAdd(ActionMapping mapping, ActionForm form,
			HttpServletRequest req, HttpServletResponse rep) {
		${table.filename}Form fm = (${table.filename}Form) form;
		Object obj = null;
		try {
			obj = cbiz.add(req, fm, null);
		} catch (MyException e) {
			LogUtil.e(this.clazz, "新增数据出现异常", e);
		}
		log.info("新增记录结果：" + obj);
		String message;
		if (obj == null || !(obj instanceof ${table.pojo})) {
			req.setAttribute(Env.DATA_NAME, "操作失败，" + obj);
			return mapping.findForward("error");
		} else {
			message = "记录添加成功";
		}
		
		add(mapping, fm, req, rep);
		//req.setAttribute("success", fm);
		req.setAttribute(Env.DATA_NAME, message);
		return mapping.findForward("add");
	}
	#end
	
	#if(${table.modify})
	/**
	 * 准备修改记录
	 * @param mapping
	 * @param fm
	 * @param req
	 * @param rep
	 * @return
	 */
	public ActionForward modify(ActionMapping mapping, ActionForm form,
			HttpServletRequest req, HttpServletResponse rep) {
		${table.filename}Form fm = (${table.filename}Form) form;
		try {
			${extend.keyDefinedForModify}
			#if(${extend.keySetterForModify})						
				${extend.keySetterForModify}
			#end
			${table.pojo} rf = (${table.pojo}) cbiz.getDao().myGet(${table.pojo}.class, key);
			if(rf == null) {
				req.setAttribute(Env.DATA_NAME, "读取数据错误");
				return mapping.findForward("error");
			}
			req.setAttribute(Env.DATA_NAME, rf);
		} catch (MyException e) {
			return LogUtil.e(this.clazz, "读取数据错误", e, mapping, req);					
		}
		#foreach($readbean in ${extend.readBeanUtilForModify})
			req.setAttribute("${readbean.field}list",  BeanUtil.startWith("${readbean.pojo}-${readbean.field}"));
		#end
		#foreach($readbean in ${extend.readContextUtilForModify})
			//${readbean.field}list是从context中读取的，这里无需有读取代码
		#end		
		#if(${extend.readDbForModify.size()}>0)
		
		try {
			#foreach($readdb in ${extend.readDbForModify})
				#if(${readdb.specialValuesSpiltBySemicolon.size()} <3)				
					//${readdb.field}配置查询关联表的特殊处理值参数错误，应该填写：pojo;value;showname
				#else			
					LogonSession ls = new LogonSession(req);
					#if(${readdb.specialValuesSpiltBySemicolon.size()}>3)
						String hql = "from ${readdb.specialValuesSpiltBySemicolon.get(0)} where ${readdb.specialValuesSpiltBySemicolon.get(3)}=?";
						#if(${readdb.specialValuesSpiltBySemicolon.size()}>4)
							Object[] obj${velocityCount} = new Object[]{${readdb.specialValuesSpiltBySemicolon.get(4)}};
						#end
					#else
						Object[] obj${velocityCount} = null;
						String hql = "from ${readdb.specialValuesSpiltBySemicolon.get(0)}";					
					#end
					List list${velocityCount} = cbiz.getDao().myList(hql, obj${velocityCount});					
					req.setAttribute("${readdb.field}list", BeanUtil.toABeanList(list${velocityCount}, "${readdb.specialValuesSpiltBySemicolon.get(1)}", "${readdb.specialValuesSpiltBySemicolon.get(2)}"));
				#end
			#end			
		} catch (MyException e) {
			return LogUtil.e(this.clazz, "读取数据错误", e, mapping, req);					
		}
		#end
		return mapping.findForward("modify");
	}

	/**
	 * 执行记录修改
	 * @param mapping
	 * @param fm
	 * @param req
	 * @param rep
	 * @return
	 */
	public ActionForward doModify(ActionMapping mapping, ActionForm form,
			HttpServletRequest req, HttpServletResponse rep) {
		${table.filename}Form fm = (${table.filename}Form) form;
		Object obj = null;
		try {
			obj = cbiz.modify(req, fm, null);
		} catch (MyException e) {
			LogUtil.e(this.clazz, "修改数据出现异常", e);
		}
		log.info("修改记录结果：" + obj);
		if(obj == null || !(obj instanceof ${table.pojo})) {
			req.setAttribute(Env.DATA_NAME, "操作失败，" +  obj);
			return mapping.findForward("error");
		}
		req.setAttribute(Env.DATA_NAME, "记录修改成功");
		return mapping.findForward("success");
	}
	#end
	
	#if(${table.detail})
	/**
	 * 查询记录详细信息
	 * @param mapping
	 * @param fm
	 * @param req
	 * @param rep
	 * @return
	 */
	public ActionForward detail(ActionMapping mapping, ActionForm form,
			HttpServletRequest req, HttpServletResponse rep) {

		${table.filename}Form fm = (${table.filename}Form) form;
		IBaseDao dao = cbiz.getDao();
		try {		
			${extend.keyDefinedForDetail}
			#if(${extend.keySetterForDetail})				
				${extend.keySetterForDetail}
			#end
			${table.pojo} pojo = (${table.pojo}) dao.myGet(${table.pojo}.class, key);
			if(pojo == null) {
				req.setAttribute(Env.DATA_NAME, "查看的信息不存，有疑问请联系管理员");
				return mapping.findForward("error");
			}
			req.setAttribute(Env.DATA_NAME, pojo);
		} catch (MyException e) {
			return LogUtil.e(this.clazz, "操作失败，未能读取相关信息的详细资料", e, mapping, req);					
		}
		#foreach($readbean in ${extend.readBeanUtilForDetail})
			req.setAttribute("${readbean.field}list",  BeanUtil.startWith("${readbean.pojo}-${readbean.field}"));
		#end
		#foreach($readbean in ${extend.readContextUtilForDetail})
			//${readbean.field}list是从context中读取的，这里无需有读取代码
		#end
		
		#if(${extend.readDbForDetail.size()}>0)
		
		try {
			#foreach($readdb in ${extend.readDbForDetail})
				#if(${readdb.specialValuesSpiltBySemicolon.size()} <3)				
					//${readdb.field}配置查询关联表的特殊处理值参数错误，应该填写：pojo;value;showname
				#else			
					LogonSession ls = new LogonSession(req);
					#if(${readdb.specialValuesSpiltBySemicolon.size()}>3)
						String hql = "from ${readdb.specialValuesSpiltBySemicolon.get(0)} where ${readdb.specialValuesSpiltBySemicolon.get(3)}=?";
						#if(${readdb.specialValuesSpiltBySemicolon.size()}>4)
							Object[] obj${velocityCount} = new Object[]{${readdb.specialValuesSpiltBySemicolon.get(4)}};
						#end
					#else
						Object[] obj${velocityCount} = null;
						String hql = "from ${readdb.specialValuesSpiltBySemicolon.get(0)}";					
					#end
					List list${velocityCount} = cbiz.getDao().myList(hql, obj${velocityCount});					
					req.setAttribute("${readdb.field}list", BeanUtil.toABeanList(list${velocityCount}, "${readdb.specialValuesSpiltBySemicolon.get(1)}", "${readdb.specialValuesSpiltBySemicolon.get(2)}"));
				#end
			#end
			
		} catch (MyException e) {
			return LogUtil.e(this.clazz, "读取相关数据出现错误", e, mapping, req);				
		}
		#end	
		return mapping.findForward("detail");
	}
	#end
	
	#if(${table.delete})
	/**
	 * 执行删除操作
	 * @param mapping
	 * @param fm
	 * @param req
	 * @param rep
	 * @return
	 */
	public ActionForward doDelete(ActionMapping mapping, ActionForm form,
			HttpServletRequest req, HttpServletResponse rep) {
		${table.filename}Form fm = (${table.filename}Form) form;
		List res = cbiz.delete(req, fm, null);
		if(res == null || !(res instanceof List)) {
			return LogUtil.e(this.clazz, "操作失败，执行删除记录失败", null, mapping, req);
		} else {
			req.setAttribute(Env.DATA_NAME, "操作完成，删除记录数量：" + res.size());
			return mapping.findForward("success");
		}
	}
	#end
	
	
	
	#if(${table.export})
	/**
	 * 导出数据
	 * @param mapping
	 * @param form
	 * @param req
	 * @param rep
	 * @return
	 */
	public ActionForward doExport(ActionMapping mapping, ActionForm form, HttpServletRequest req,
			HttpServletResponse rep) {

		try {
			${table.filename}Hql hql0 = new ${table.filename}Hql();
			List list = this.cbiz.getDao().myList(hql0.getHql(req, form), hql0.getParaValue(req, form));
			if(list == null || list.size() < 1) {
				req.setAttribute(Env.DATA_NAME, "本次查询没有数据，所以不做导出");
				return mapping.findForward("success");
			}
			#foreach($readbean in ${extend.readBeanUtilForExport})
				final List ${readbean.field}list = BeanUtil.startWith("${readbean.pojo}-${readbean.field}");
			#end
			
			#foreach($readdb in ${extend.readDbForExport})
				#if(${readdb.specialValuesSpiltBySemicolon.size()} <3)				
					//${readdb.field}配置查询关联表的特殊处理值参数错误，应该填写：pojo;value;showname
				#else			
					LogonSession ls = new LogonSession(req);
					#if(${readdb.specialValuesSpiltBySemicolon.size()}>3)
						String hql = "from ${readdb.specialValuesSpiltBySemicolon.get(0)} where ${readdb.specialValuesSpiltBySemicolon.get(3)}=?";
						#if(${readdb.specialValuesSpiltBySemicolon.size()}>4)
							Object[] obj${velocityCount} = new Object[]{${readdb.specialValuesSpiltBySemicolon.get(4)}};
						#end
					#else
						Object[] obj${velocityCount} = null;
						String hql = "from ${readdb.specialValuesSpiltBySemicolon.get(0)}";					
					#end
					List list${velocityCount} = cbiz.getDao().myList(hql, obj${velocityCount});					
					final List ${readdb.field}list = BeanUtil.toABeanList(list${velocityCount}, "${readdb.specialValuesSpiltBySemicolon.get(1)}", "${readdb.specialValuesSpiltBySemicolon.get(2)}");
				#end
			#end			
			
			ExportTemplate template = new ExportTemplate(){
				private static final long serialVersionUID = 1L;
	
				@Override
				public void setRow(DataRow row, Object obj) {
					${table.pojo} pojo = (${table.pojo}) obj;
					${extend.rowForExport}
				}
			};		
			String root3path3file = "${table.export.path3root}/" + CU.getSerial(".xls");
			template.setRoot3path3file(root3path3file);
			template.setContentTitle(new String[]{${extend.contentTitleForExport}});
			
			IDataPort port = new JxlPort();			
			template.setDataList(list);
			port.export(template);
			
			#if(${table.export.downloadExport})
				IFileLoad fl = new FileLoad();
				fl.download(req, rep, this.servlet.getServletConfig(), root3path3file);
				
				#if(${table.export.deleteAfterDownload})				
					IFileProcessor fp = new FileProcessor();
					fp.delete(root3path3file);
				#end
			#end
			
			#if(${table.export.log})
				this.cbiz.log(req, "", "导出记录，文件为：" + root3path3file, LOGTYPE.EXPORT);
			#end	
		} catch (MyException e) {
			return LogUtil.e(this.clazz, "操作出现错误", e, mapping, req);
		}
		
		#if(${table.export.downloadExport})
			return null;
		#else
			return LogUtil.e(this.clazz, "数据已经成功导出", null, mapping, req);
		#end
	}
	#end
	
	#if(${table.importer})
	/**
	 * 准备导入
	 * 
	 * @param mapping
	 * @param fm
	 * @param req
	 * @param rep
	 * @return
	 */
	public ActionForward importer(ActionMapping mapping, ActionForm form,
			HttpServletRequest req, HttpServletResponse rep) {		
		#if(${extend.readDbForImporter.size()}>0)
		try {
			#foreach($readdb in ${extend.readDbForImporter})
				#if(${readdb.specialValuesSpiltBySemicolon.size()} <3)				
					//${readdb.field}配置查询关联表的特殊处理值参数错误，应该填写：pojo;value;showname
				#else			
					LogonSession ls = new LogonSession(req);
					#if(${readdb.specialValuesSpiltBySemicolon.size()}>3)
						String hql = "from ${readdb.specialValuesSpiltBySemicolon.get(0)} where ${readdb.specialValuesSpiltBySemicolon.get(3)}=?";
						#if(${readdb.specialValuesSpiltBySemicolon.size()}>4)
							Object[] obj${velocityCount} = new Object[]{${readdb.specialValuesSpiltBySemicolon.get(4)}};
						#end
					#else
						Object[] obj${velocityCount} = null;
						String hql = "from ${readdb.specialValuesSpiltBySemicolon.get(0)}";					
					#end
					List list${velocityCount} = cbiz.getDao().myList(hql, obj${velocityCount});					
					req.setAttribute("${readdb.field}list", BeanUtil.toABeanList(list${velocityCount}, "${readdb.specialValuesSpiltBySemicolon.get(1)}", "${readdb.specialValuesSpiltBySemicolon.get(2)}"));
				#end
			#end
			
		} catch (MyException e) {
			return LogUtil.e(this.clazz, "读取相关数据出现错误", e, mapping, req);
		}
		#end
		return mapping.findForward("importer");
	}
		
	/**
	 * 导出数据
	 * @param mapping
	 * @param form
	 * @param req
	 * @param rep
	 * @return
	 */		 
	public ActionForward doImporter(ActionMapping mapping, ActionForm form, HttpServletRequest req, HttpServletResponse rep) {
		IFileLoad fl = new FileLoad();
		List<String> importerfiles;
		try {
			importerfiles = fl.uploadToPath(form, "${table.importer.path3root}/", true);
		} catch (MyException e) {
			return LogUtil.e(this.clazz, "文件上传失败，不能进行导入", e, mapping, req);
		}
		
		#foreach($readbean in ${extend.readBeanUtilForDoImporter})
			final List ${readbean.field}list = BeanUtil.startWith("${readbean.pojo}-${readbean.field}");
		#end
		final LogonSession ls = new LogonSession(req);
		final HttpServletRequest req0 = req;		
		ImportTemplate template = new ImportTemplate() {
			@Override
			public Object setObject(DataRow row, Object other) throws MyException {
				${table.pojo} pojo = new ${table.pojo}();
				#set($temp=0)
				#foreach($field in ${extend.fieldsForImporter})
					#if(${field.special}=="#")
						pojo.${field.setter}(ConversionUtils.convert(req0.getParameter("${field.field}"), ${field.fieldClassString}.class));
					#elseif(${field.special}=="~")
						pojo.${field.setter}(ConversionUtils.convert(BeanUtil.getFirstValueFromObject(row.get($temp), ${field.field}list), ${field.fieldClassString}.class));
						#set($temp=$temp+1)
					#elseif(${field.special}=="@")
						pojo.${field.setter}(ConversionUtils.convert(ls.getAttribute("${field.specialValue}"), ${field.fieldClassString}.class));
					#elseif(${field.special}=="=" || ${field.specialValue}=="^")
						pojo.${field.setter}(DT.getNow());
					#elseif(${field.fieldClassString}=="String")					
						pojo.${field.setter}(row.get($temp));
						#set($temp=$temp+1)						
					#else
						pojo.${field.setter}(ConversionUtils.convert(row.get($temp), ${field.fieldClassString}.class));
						#set($temp=$temp+1)												
					#end
				#end
				return pojo;
			}
		};
		template.setErrorInputAllowed(false);
		template.setErrorInsertAllowed(false);
		template.setRoot3path3file(importerfiles.get(0));
		template.setBaseDao(cbiz.getDao());
		template.setStart(1);
		IDataPort port = new JxlPort();
		try {
			port.importer(template);
		} catch (MyException e) {
			return LogUtil.e(this.clazz, "数据导入失败", e, mapping, req);			
		}
		int success = template.getSuccess();
		String show = "数据已经成功导入，导入数量有：" + success;
		int error = template.getError();
		if (error > 0)
			show = show + "，有" + error + "条记录不能正常导入";
		req.setAttribute(Env.DATA_NAME, show);
		
		#if(${table.importer.log})
			try {
				this.cbiz.log(req, "", "导入记录，结果为：" + show, LOGTYPE.IMPORTER);
			} catch (MyException e) {
				log.info("导入写log表失败", e.getException());
			}
		#end
		return mapping.findForward("success");
	}
	
	/**
	 * 下载模板
	 * @param mapping
	 * @param form
	 * @param req
	 * @param rep
	 * @return
	 */
	public ActionForward template(ActionMapping mapping, ActionForm form, HttpServletRequest req, HttpServletResponse rep) {
		#if(${table.export})
			String path = "${table.export.path3root}/";
		#else
			String path = "${table.importer.path3root}/";		
		#end
		try {
			if(!FU.isExist(path + "/template.xls", false)) {
				ExportTemplate template = new ExportTemplate() {
					private static final long serialVersionUID = 1L;
					@Override
					public void setRow(DataRow row, Object obj) {
					}
				};
				template.setRoot3path3file(path + "/template.xls");
				template.setContentTitle(new String[]{${extend.contentTitleForImporter}});
				template.setDataList(new ArrayList());
				IDataPort port = new JxlPort();
				port.export(template);
			}		
			IFileLoad fl = new FileLoad();
			fl.download(req, rep, this.servlet.getServletConfig(), path + "/template.xls");
		} catch (MyException e) {
			return LogUtil.e(this.clazz, "获取模板失败", e, mapping, req);				
		}
		return null;
	}
	#end	

	public void setCbiz(AbstractBiz cbiz) {
		this.cbiz = cbiz;
	}
		
	public void setPbiz(IPaginationBiz pbiz) {
		this.pbiz = pbiz;
	}
}
