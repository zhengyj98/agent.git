$(
	function checkform() {
		$('#TExtendForm').validate(
			{
				rules: {
					name:{
						required:true
					},
					url:{
						required:true
					},
					type:{
						required:true
					}
				}
			}
		);
	}
);

function addParam(){
	var content = $("#url").val();
	var param = $("#param").val();
	var paramName = param.replace(".", "");
	paramName = paramName.replace("{{", "");
	paramName = paramName.replace("}}", "");
	var split = content.indexOf("?") >= 0 ? "&" : "?";
	$("#url").val(content + split + paramName + "=" + param);
}

