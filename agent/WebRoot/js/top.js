﻿var handle;
var error = 0;
function getNewNotice(){
		$.ajax({
			type:"POST",
			url:"notice/NoticeDpAct.do?method=getNewNotice",
			async: true,	
			dataType: "xml", 
			error: function(){
				error++;				
				if(error > 4) {
					$("#notices").html("您已经超时，请重新登录");
					clearInterval(handle);
				}
			},		
			success:function(data){	
				$("#notices").empty();
				$(data).find("id").each( function(i) {   
					var URL ="<a href='javascript:detail(\"" + $(this).text() + "\")'>" + $(this).next().text()+ "</a>";
					var span = "<span id=" + $(this).text() + ">【" + URL+ "】</span>";
					var exist = $("span[id="+$(this).text()+"]");
					if(exist.length==0) {
						$("#notices").prepend(span);
					}
				});						
			}
		});
}
getNewNotice();
handle = setInterval("getNewNotice()", 60000);
function detail(id){
	//$("#" + id).remove();	
	parent.document.frames["mainFrame"].location="notice/NoticeDpAct.do?method=detail&id="+id;
}
