//全局变量，表’选择全部‘的控件的名字
var selectAllCheckbox;

/*
改变其他checkbox的状态与this一样
*/
function fSelectAll(myName){
	var ck = $("#"+myName).attr("checked");
	$("input[type=checkbox]").attr("checked", ck);
	selectAllCheckbox = myName;
}

//对指定的id的checkbox设置值,参数为："#id1,#id2,#id3,#id4"
function fSelectAll2(myName, othersName){
	var ck = $("#"+myName).attr("checked");
	$(othersName).attr("checked", ck);	
	selectAllCheckbox = myName;
}

//限制某些控件的参数，参数必须是2的倍数，如：checkNull('username','姓名')将提示：姓名不能为空
function checkNull(){
	if(arguments.length % 2!=0){
		alert("参数格式不符合要求");
		return false;
	}
	for(var i=0;i<arguments.length;i+=2) {
		var obj = $("#" + arguments[i]);
		if($.trim(obj.val()).length == 0){
			alert(arguments[i + 1] + "必须填写");
			obj.focus();
			return false;
		}
	}
	return true;
}

function checkNumber(){
	if(arguments.length % 2!=0){
		alert("参数格式不符合要求");
		return false;
	}
	for(var i=0;i<arguments.length;i+=2) {
		var obj = $("#" + arguments[i]);
		var regNum =/^\d*$/;
		if(!regNum.test($.trim(obj.val()))){
			alert(arguments[i + 1] + "必须位数字，请修正");
			obj.focus();
			return false;
		}
	}
	return true;
}

//读取指定的form，如果找不到指定的则返回第一个
function getMyFormById(theFormId){
	var thf = $("#" + theFormId);
	if(thf && thf.length > 0)
		return thf.get(0);
	var forms = $("form");
	if(forms.length==0)//无form存在
		return null;
	else
		return forms.get(0);
}

/**
删除记录时候的form提交，
判断页面上的checkbox对象是否有被选中的，如果没有，则不予提交，如果有 ，则要其确认。
**/
function deleteSelected(actTo, theFormId, showText){
	if(!showText || showText == "") {
		showText = "数据删除将无法恢复，是否确定删除?";
	}
	var b = false;
	$("input[type=checkbox]").each(function(i){
		if($(this).attr("id") != selectAllCheckbox && $(this).attr("checked")=="checked") {
   			b = true;   			
   		}
 	});	
	if(!b) {
		alert('您没有选择记录!');
		return false;
	} else {
		var theForm = getMyFormById(theFormId);
		if(!confirm(showText) || !theForm) 
			return false;
		theForm.action = actTo;	
		theForm.submit();
	}
}

//获取一个本地时间
function getTimeString(d){
	var now = new Date();
	if(d) {
		now = new Date(d);
	}
	var month = now.getMonth() + 1;
	var date = now.getDate();
	var hours = now.getHours();
	var mins = now.getMinutes();
	var secs = now.getSeconds();
	if(month<10)
		month = "0" + month;
	if(date<10)
		date = "0" + date;
	if(hours<10)
		hours = "0" + hours;
	if(mins<10)
		mins = "0" + mins;
	if(secs<10)
		secs = "0" + secs;
	var timeVal = "";
	timeVal = month + "-" + date + " " + hours + ":" + mins + ":" +  secs;
	return timeVal;
}
//获取一个本地日期
function getDateString(){
	var now = new Date();
	var year = now.getYear();
	var month = now.getMonth() + 1;
	var date = now.getDate();
	if(month<10)
		month = "0" + month;
	if(date<10)
		date = "0" + date;
	var timeVal = "";
	timeVal = year + "-" + month + "-" + date ;
	return timeVal;
}


function exportList(actTo, theFormId, showText){
	if(!showText || showText == "") {
		showText = "确认导出查询的信息吗？";
	}
	if(!confirm(showText)) 
		return false;
	var theForm = getMyFormById(theFormId);
	var tmp = theForm.action;
	theForm.action = actTo;	
	theForm.submit();
	theForm.action = tmp;
}



function vl(value) {
    value = $.trim(value);
    return value != null && value != 'undefined' && value !== '' && !(typeof value == 'undefined') && value !== " ";
}

function timeToString(ccTimeIndex, type) {
	var hour = parseInt(ccTimeIndex / 3600);
	var minutes = parseInt((ccTimeIndex % 3600) / 60);
	var seconds = parseInt(ccTimeIndex % 60);
	hour = hour < 10 ? "0" + hour : hour;
	minutes = minutes < 10 ? "0" + minutes : minutes;
	seconds = seconds < 10 ? "0" + seconds : seconds;
	if(type == 1) {
		return minutes + ":" + seconds;
	}
	return hour + ":" + minutes + ":" + seconds;
}

function getQueryString(name) {
	var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
	var r = window.location.search.substr(1).match(reg);
	if (r != null) {
		return unescape(r[2]);
	}
	return null;
}

function commit(url, param, callback) {
	$.ajax({
		type:"POST", 
		dataType:"json",
		async:true,
		url: url, 
		data:param,
		success:function (resp) {
			if(callback) {
				callback(resp);
			}
		}, error:function (resp) {
			if(callback) {
				resp.result = false;
				callback(resp);
			}
		}
	});
}
/**
 * 检测是否支持html5api
 * @return {[type]} [description]
 */
function checkSupportFormData() {
	try {
		return !!FormData;
	} catch (e) {
		return false;
	}
}

var JIANYUE_TERMINAL_TYPE = {PC:0, MOBILE:1};
function jianyueGetCurrentDevice() {
	if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
		return JIANYUE_TERMINAL_TYPE.MOBILE;
	} else {
		return JIANYUE_TERMINAL_TYPE.PC;
	}
}
function jianyueMobileClient() {
//return true;
	return jianyueGetCurrentDevice() == JIANYUE_TERMINAL_TYPE.MOBILE;
}
function getLocalTime(nS) {     
   return getTimeString(parseInt(nS) * 1000);     
}