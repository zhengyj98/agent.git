
var INTERVAL_WHEN_END = 8000;
var interval = null;
function startInterval(time) {
	if (!time) {
		time = 3000;
	}
	if (interval) {
		clearTimeout(interval);
	}
	interval = setInterval(function () {
		get();
	}, time);
}
var loadedNiceScroll = false;
var success = 0;
var error = 0;
var disconnection = false;
function get(event, data, async0, callback) {
	var callurl = "../visitor/VisitorDpAct.do?method=processor";
	//指定类型获取
	if (vl(event)) {
		callurl = callurl + "&event=" + event;
	}
	//默认情况都是异步获取
	var async = true;
	if (vl(async0)) {
		async = async0;
	}
	if (data) {
		data.cid = crm.id;
	} else {
		data = {cid:crm.id};
	}
	//每次刷新界面都会产生一个新的序列号
	data.sequence = sequence;
	if (!vl(data.id)) {
		var time = new Date().getTime();
		data.id = "chat-" + crm.id + "-" + time;
	}
	$.ajax({type:"POST", dataType:"json", async:async, url:callurl, data:data, success:function (resp) {
		if (callback) {
			callback(resp);
			return;
		}
		//以下信息处理是针对BizMessage进行的
		if (resp.code == 1) {
			if(resp.msg == "20001" || resp.msg == "20002") {
				var text = "";
				if(resp.msg == "20001") {
					text = TEXT.REFRESH_TO_LOAD;
				} else if(resp.msg == "20002") {
					text = TEXT.LOAD_ON_OTHER_PAGE;
				} 
				clearTimeout(interval);//不再提取服务器消息
				disconnection = true;//已经断开
				
				//提醒
				showLayer(text);
				blockInput(text, "window.location.reload()");
				//setInterval(function () {//间隔提醒
				//	showLayer(TEXT.REFRESH_TO_LOAD);
				//}, 10000);
			} else {
				showLayer(TEXT.DO_ERROR);
			}
			return;
		}
		for (var v = 0; v < resp.length; v++) {
			var message = resp[v];
			console.log(message);
			if (message.event == MESSAGE_EVENT.QUESTION_LIST) {
				showQuestionList(message.object);
				toBottom();
			} else {
				if (message.event == MESSAGE_EVENT.SESSION) {
					var session = message.object;
					sessions[session.pid] = session;
					//LOGIN\SELF\QUEUE\AGENT
					showSession(session);	
			
					//处于LOGIN状态，则获取技能组列表
					if (session.step == CLIENT_SESSION_STEP.LOGIN) {
						getGroupList();
					}
				} else {
					if (message.event == MESSAGE_EVENT.NOTIFY) {
						handlerNotify(message);
					} else {
						if (message.event == MESSAGE_EVENT.CHAT) {
							postMessage({event : "message", pid : message.pid, text : message.text});
							showChatMessge(message);
							toBottom();
							flashTitle();
							showNotify(TEXT.VISITOR_HAVE_NEW_MESSAGE);
						}
					}
				}
			}
		}
		if ($("#msgWrapper")[0].scrollTop > 150 && loadedNiceScroll == false) {
			loadedNiceScroll = true;
			resetNiceScroll();
		}
		success++;
	}, error:function (data) {
		error++;
	}});
}
function send(data, callback) {
	get("", data, true, callback);
}

function postMessage(json) {
    try {
    	if(windowOpenType == "D") {//DIV模式才需要把消息传递给嵌入页面
        	window.parent.postMessage(JSON.stringify(json), quoterUrl);
        }
    } catch (e) {
    }
}