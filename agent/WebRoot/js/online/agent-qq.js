var gettingResult;
var serial;

function showQrcode() {
	serial = (Math.random() + "").replace("0.", "");
	layer.open({
		type : 1,
		closeBtn : 1,
		title : "请扫码登陆",
		content : "<img src='../base/TChannelDpAct.do?method=qqLogin&serial=" +  serial + "' width='100%' height='100%' id='qrcode'>",
		move : false,
		maxmin : false,
		area : [ '400px', '400px' ],
		shadeClose : true,
		cancel: function(){ 
		    //右上角关闭回调
		    layer.closeAll();
		    clearInterval(gettingResult);
		    commit("../base/TChannelDpAct.do?method=qqLogin", {action:"cancel", serial:serial});
		}
	});
	
	gettingResult = setInterval(function () {
	    commit("../base/TChannelDpAct.do?method=qqLogin", {action:"result", serial:serial}, function(data){
	    	if(data.msg.indexOf('SUCCESS') >=0) {
	    		layer.closeAll();
	    		showLayer("登陆成功");
	    		clearInterval(gettingResult);
	    		qqLoginSuccess(data.msg.substring(7), serial);
	    	} else if(data.msg=='ERROR') {
	    		commit("../base/TChannelDpAct.do?method=qqLogin", {action:"cancel", serial:serial});
	    		$("#qrcode").attr("alt", "二维码已经失效"); 
	    		clearInterval(gettingResult);
	    	} else if(data.msg=='WAITING') {
	    		$("#qrcode").attr("src", "../base/TChannelDpAct.do?method=qqLogin&serial=" +  serial + "&t=" + Math.random());
	    	}
	    });
	}, 3000);
	return false;
}

function qqLogout(serial2, number) {
	layer.confirm("是否确认退出" + number, {title:TEXT.CONFIRM_TITLE, shadeClose:true, btn:[TEXT.CONFIRM, TEXT.CANCEL]}, function () {
		commit("../base/TChannelDpAct.do?method=qqLogin&serial=" +  serial2, {action:"cancel"}, function(){
	    	$("#QQ" + serial2).remove();
			layer.closeAll();
	    });
	});
}

function qqLoginSuccess(number, serial2) {
	var firstLevel = "";
	firstLevel += "<div class=\"first-level\" id=\"QQ" + serial2 + "\">";
	firstLevel += "	<span id=\"Span-QQ-" + number + "\" class=\"click-icon fold\" onclick=\"toggle('QQ-" + number + "')\"></span><span onclick=\"toggle('QQ-"
	+ number + "')\" class=\"hand\">" + number + "</span>[<a href=\"javascript:qqLogout('" +serial2+ "', '" + number + "')\">退出</a>]";
	firstLevel += "	<div id=\"QQ-" + number + "\" style='display:none'>";
	firstLevel += "	</div>";
	firstLevel += "</div>";
	$("#tab-left-div-3").append(firstLevel);
	
	commit("../base/TChannelDpAct.do?method=qqLogin", {action:"getCategory", serial:serial2},function(data){
		for(var v=0;v < data.length;v++) {
			var one = data[v];
			var gid = "G" + one.index;
			addCategory(number, gid, one.name);
			var friends = one.friends;
			for(var j=0;j<friends.length;j++) {
				var f = friends[j];
				addFriend(number, gid, f.userId, f.nickname, f.markname);	
			}
		}
	});
}



function addCategory(qqNumber, qqGroup, groupName) {
	var category = "";
	category += "<div class=\"second-level\">";
	category += "	<span id=\"Span-" + qqGroup + "\" class=\"click-icon fold\" onclick=\"toggle('" + qqGroup + "')\"></span><span onclick=\"toggle('" + qqGroup + "')\" class=\"hand\">" + groupName + "</span>";
	category += "	<div id=\"" + qqGroup + "\" style='display:none'>";
	category += "		<ul id=\"" + qqGroup + "UL\">";
	category += "		</ul>";
	category += "	</div>";
	category += "</div>";
	$("#QQ-" + qqNumber).append(category);
}

function addFriend(qqNumber, qqGroup, qqFriend, friendName, markname) {
	if(!vl(markname)) {
		markname = friendName;
	}
	var li = "";
	li = li + "<li id='Friend-" + qqFriend +  "'>";
	li = li + "<b class='web-offline'></b>";
	li = li + "<span class='session-visitor-name'>" + markname + "[" + friendName + "]</span>";	
	li = li + "<span class='session-visitor-name session-visitor-channel'></span>";	
	li = li + "</li>";
	//var li = "<li id='Friend-" + qqFriend +  "' class='session-visitor-name'>" + friendName + "</li>";
	$("#" + qqGroup + "UL").append(li);
}

$(function(){
	commit("../base/TChannelDpAct.do?method=qqLogin", {action:"list"},function(data){
		for(var v=0;v < data.length;v++) {
			var one = data[v];
			qqLoginSuccess(one.number, one.serial);
		}
		
	});
	//qqLoginSuccess('33845916');
	//addCategory('33845916','haoyou','好友');
	//addCategory('33845916','wangyou','网页');
	//addFriend('33845916','wangyou','100','郑余杰', 'xxx');
	//addFriend('33845916','wangyou','101','郑余杰2', 'xxx333333333333333333333333');
});