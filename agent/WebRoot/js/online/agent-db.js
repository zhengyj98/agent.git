//每次刷新界面都会产生一个新的序列号
var sequence = "";

//crm的字段类型
var crmField;

//是否隐藏消息的昵称
var hideName = false;

//记录点击的访客ID
var currentVisitorId;

//当前点击的排队用户
var currentQueueVisitorId;

//标记最后点击的类型，决定是否可以发送消息
var lastClickVisitorType = "";

//标记最后等待协助的访客
var currentWaitingVisitorId = "";

//坐席本人信息
var agentSelfInfo = null;

//技能组的成员
var allGroup = {};

//发送出去的请求，收到回执则提醒
var waitingReceipt = {
	id : "提示内容"
};

//排队信息{sessionId:xxx,visitorId:yyy,object:{member:object1, crm:object2, channel:zzz}}
var theQueue = {};

//接收的会话 {sessionId:xxx,visitorId:yyy, visitorStatus:online,unreadCount:0, object:{member:object1, crm:object2, channel:zzz}}
var talkingVisitor = {};

//历史会话
var historyVisitor = {};

//等待协助的会话
var waitingVisitor = {};

//记录每个访客的历史消息页码
var pgData = new Array();