
MyUpload = new function () {
	var BIZ_TYPE = {SETTING_FILE:0, CHAT_FILE:1};
	var MULTI_TYPE = {IMG:"IMG", FILE:"FILE"};
	var MAX_File_Size = 5 * 1024 * 1024;//5M
	var responseParams = {};
	var isUploadFinish={};
	
	//params:uuid biztype token
	this.uploadFile = function (params, uploadTarget, callbackFunction) {
		var checkResult = checkFileSize(params, uploadTarget, callbackFunction);
		if (checkResult) {
			try {
				jianyueUploadCommon(params, uploadTarget, callbackFunction);
			} catch (e) {
				responseParams[params.uuid].code = 20000;
				responseParams[params.uuid].msg = e;
				callbackFunction(responseParams[params.uuid]);
				return false;
			}
		}
	};
	
	function checkFileSize(params, uploadTarget, callbackFunction) {
		responseParams[params.uuid] = {};
		responseParams[params.uuid].uuid = params.uuid;
		try {
			var objFile = null;
			uploadTarget.find("input").each(function () {
				var type = $(this).attr("type") || $(this)[0].tagName;
				if (type == "file") {
					objFile = $(this)[0];
				}
			});
			if (!objFile) {
				responseParams[params.uuid].code = 20002;
				responseParams[params.uuid].msg = "file form is null";
				callbackFunction(responseParams[params.uuid]);
				return false;
			}
			var filePath = objFile.value;
			if (filePath == "") {
				responseParams[params.uuid].code = 20001;
				responseParams[params.uuid].msg = "file is null";
				callbackFunction(responseParams[params.uuid]);
				return false;
			}
			
		 	var fileNameStart = filePath.lastIndexOf("\\");
		 	var fileName = filePath.substring(fileNameStart+1, filePath.length);
		 	if(fileName){
		 		responseParams[params.uuid].fileName=fileName;
		 	}
		 	
			var extStart = filePath.lastIndexOf(".");
		 	var ext = filePath.substring(extStart, filePath.length).toUpperCase();
		 	if(params.multType == MULTI_TYPE.IMG){
				if (ext.length > 0 && !(ext == ".BMP" ||ext == ".PNG" || ext  ==  ".GIF" || ext  ==  ".JPG" || ext  ==  ".JPEG")
				 && !(ext == ".RAR" ||ext == ".ZIP" || ext  ==  ".PPT" || ext  ==  ".XLS"|| ext  ==  ".DOC" || ext  ==  ".DOCX" || ext  ==  ".XLSX")) {
					responseParams[params.uuid].code=20003;
			 		responseParams[params.uuid].msg='ÎÄ¼þ¸ñÊ½²»ÕýÈ·';
			 		callbackFunction(responseParams[params.uuid]);
			 		return false;
				} 
		 	}

			var currentfileSize = "";
			if (objFile && objFile.files && objFile.files[0] && objFile.files[0].size) {
				currentfileSize = objFile.files[0].size;
				if (currentfileSize > MAX_File_Size) {
					responseParams[params.uuid].code = 20004;
					responseParams[params.uuid].msg = "ÎÄ¼þ´óÐ¡²»ÄÜ³¬¹ý5M";
					return false;
				}
			}
		}
		catch (e) {
			responseParams[params.uuid].code = 20000;
			responseParams[params.uuid].msg = e;
			callbackFunction(responseParams[params.uuid]);
			return false;
		}
		return true;
	}
	
	function jianyueUploadCommon(params, uploadTarget, callbackFunction) {
		if (uploadTarget == null) {
			throw "dom :" + uploadTarget + " is not exisit";
		}
		if (!params) {
			throw "params mimeType is null";
		}
		if (!params.bizType) {
			throw "params biz type is null";
		}
		//../file/load.do?method=upload
		var options = {
			type:"post", 
			url:params.uploadUrl, 
			timeout:1200000, 
			contentType:"application/x-www-form-urlencoded; charset=utf-8", 
			data:params, 
			dataType:"json", 
			beforeSend:
			function (XMLHttpRequest) {
			}, success:function (data) {
				isUploadFinish[params.uuid] = true;
				callbackFunction(data);
			}, error:function (data) {
				isUploadFinish[params.uuid] = true;
				responseParams[params.uuid].code = 22000;
				responseParams[params.uuid].msg = "upload error";
				responseParams[params.uuid].errorData = data;
				callbackFunction(responseParams[params.uuid]);
			}
		};
		isUploadFinish[params.uuid] = {};
		isUploadFinish[params.uuid] = false;
		isUploadFinish[params.uuid + "index"] = 1;
		uploadTarget.ajaxSubmit(options);
		if (params.percent == true) {
			setTimeout(function () {
				getUploadPercent(params, uploadTarget, callbackFunction);
			}, 2000);
		}
	}
	function getUploadPercent(params, uploadTarget, callbackFunction) {
		if (!isUploadFinish[params.uuid]) {
			if (isUploadFinish[params.uuid + "index"] <= 600) {
				isUploadFinish[params.uuid + "index"]++;
				var url = params.getPercentUrl + "?uuid=" + params.uuid;//../file/load.do?method=percent
				$.post(url, null, function (response) {
					callbackFunction(response);
					if (response.code == 30000 && !isUploadFinish[params.uuid]) {
						setTimeout(function () {
							getUploadPercent(params, uploadTarget, callbackFunction);
						}, 1000);
					}
				}, "json");
			} else {
				responseParams[params.uuid].code = 24000;
				responseParams[params.uuid].msg = "maybe server down";
				callbackFunction(responseParams[params.uuid]);
			}
		}
	}
};

