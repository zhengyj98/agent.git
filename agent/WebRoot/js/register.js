function showRegister() {
	layer.open({
		type : 2,
		closeBtn : 1,
		title : "",
		content : "register.html",
		move : false,
		area : [ '80%', '480px'],
		shadeClose : true,
		cancel: function(){ 
		    //右上角关闭回调
		    layer.closeAll();
		}
	});
}	

function doRegister() {
	if(!$("#org").val()) {
		layer.msg("请输入贵企业名称");
		return false;
	}
	if(!$("#phone").val() || $("#phone").val().length != 11) {
		layer.msg("请输入正确的手机号码");
		return false;
	}				
	if(!$("#password").val()) {
		layer.msg("请输入登陆密码");
		return false;
	}				
	if(!$("#validate").val()) {
		layer.msg("请输入验证码");
		return false;
	}
	var options = {
		url: "Logon/LogonDpAct.do?method=register",
		dataType:"json",
		success: function (e) {
			if(e.code !=0) {
				layer.msg(e.msg);	
			} else {
				layer.confirm("注册成功，马上登陆体验吧", {title:"确认", shadeClose:true, btn:["登陆", "暂不"]}, function () {
					window.location = "logon.html";
				});
			}
		},error: function (e) {
			layer.msg("注册失败");
		}
	};
	$("#LogonForm").ajaxSubmit(options);
}

function showLogon() {
	layer.open({
		type : 2,
		closeBtn : 1,
		title : "",
		content : "logon.html",
		move : false,
		area : [ '80%', '380px'],
		shadeClose : true,
		cancel: function(){ 
		    //右上角关闭回调
		    layer.closeAll();
		}
	});
}	


function doLogon() {
	if(!$("#username").val()) {
		layer.msg("请输入您的手机号码/账号");
		return false;
	}				
	if(!$("#password").val()) {
		layer.msg("请输入登陆密码");
		return false;
	}				
	if(!$("#validate").val()) {
		layer.msg("请输入验证码");
		return false;
	}
	var options = {
		url: "Logon/LogonDpAct.do?method=logon",
		dataType:"json",
		success: function (e) {
			if(e.code !=0) {
				layer.msg(e.msg);	
			} else {
				parent.window.location = "Logon/index.jsp";
			}
		},error: function (e) {
			layer.msg("登陆失败，请输入正确的验证码");
		}
	};
	$("#LogonForm").ajaxSubmit(options);
}