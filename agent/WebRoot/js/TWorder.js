$(
	function checkform() {
		$('#TWorderForm').validate(
			{
				rules: {
					status:{
						required:true
					},
					subject:{
						required:true
					},
					crm:{
						required:true
					},
					toAgent:{
						required:true
					},
					level:{
						required:true
					}
				}
			}
		);
	}
);


function showWorderDetail(id) {
	layer.open({
		type : 2,
		closeBtn : 1,
		title : "工单详情",
		content : "../biz/TWorderDpAct.do?method=detail&id=" + id,
		move : false,
		maxmin : true,
		area : [ '80%', '90%' ],
		shadeClose : true,
		cancel: function(){ 
		    layer.closeAll();
		}
	});
}

function showAddCrm() {
	layer.open({
		type : 2,
		closeBtn : 1,
		title : "新增客户",
		content : "../crm/TCrmDpAct.do?method=add",
		move : false,
		maxmin : true,
		area : [ '80%', '90%' ],
		shadeClose : true,
		cancel: function(){ 
		    layer.closeAll();
		}
	});
}