<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://www.cellcom.com.cn/myjsp" prefix="myjsp"%>
<html>
	<head>
		<title>${orgName}[在线咨询]</title>
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
		<meta http-equiv="description" content="This is my page">
		<meta content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0,user-scalable=no" name="viewport" id="viewport" />
		
		<link rel="stylesheet" type="text/css" href="../themes/online/big.css" />
		<link rel="stylesheet" type="text/css" href="../themes/online/lw.css" />
		<link rel="stylesheet" type="text/css" href="../plugins/wechat-emotion/jquery-wechatEmotion-2.1.0.css" />
		<link rel="shortcut icon" type="image/x-icon" href="../file/load.do?method=get&bizType=SETTING&fileName=${orgHeader}" />


		<script type="text/javascript" src="../js/mycommon.jquery.js"></script>
		<script type="text/javascript" src="../js/my.upload.js"></script>
		<script type="text/javascript" src="../plugins/jquery-x.x.js"></script>
		<script type="text/javascript" src="../plugins/jquery.form.js"></script>
		<script type="text/javascript" src="../js/jquery.validate.js"></script>

		<script type="text/javascript" src="../plugins/jquery.blockUI.js"></script>
		<script type="text/javascript" src="../plugins/jquery.nicescroll.min.js"></script>
		<script type="text/javascript" src="../plugins/layer/layer.js"></script>
		<script type="text/javascript" src="../plugins/keditor/kindeditor-min.js"></script>
		<script type="text/javascript" src="../plugins/keditor/lang/zh_CN.js"></script>
		<script type="text/javascript" src="../plugins/keditor/keditor-extend.js"></script>
		<script type="text/javascript" src="../plugins/wechat-emotion/jquery-wechatEmotion-2.1.0.js"></script>
		<script type="text/javascript" src="../plugins/recorder/bundle.js"></script>
		<script type="text/javascript" src="../plugins/recorder/recorder-extend.js"></script>

		<script type="text/javascript" src="../js/online/constant.js"></script>
		<script type="text/javascript" src="../js/online/all-show.js"></script>
		<script type="text/javascript" src="../js/online/visitor-show.js"></script>
		<script type="text/javascript" src="../js/online/visitor-db.js"></script>
		<script type="text/javascript" src="../js/online/visitor-postman.js"></script>
		<script type="text/javascript" src="../js/online/visitor-logic.js"></script>
		<script type="text/javascript" src="../js/online/visitor-satisfy.js"></script>
		<script type="text/javascript" src="../js/online/all-tip.js"></script>

		<script>
		var bg = '${bg}';
		var advertHeight = '${advertHeight}';
		document.write('<style>');
		document.write('.light-btn {background-color: #' + bg + ' !important;}');
		document.write('#support-holder {color: #' + bg + ' !important;}');
		document.write('#header {background-color: #' + bg + ' !important;}');
		document.write('.msg-client .bubble {background-color: #' + bg + ' !important;}');
		//document.write('.notifyClass {color: #' + bg + ' !important;}');
		
		if(advertHeight > 0) {
			var top = advertHeight + 60;
			document.write('#pageChat {top: ' + advertHeight + 'px !important;}');
			document.write('#body-right {top: ' + top + 'px !important;}');
			document.write('#header {top: ' + top + 'px !important;}');
			document.write('#msgWrapper {top: ' + top + 'px !important;}');
			document.write('#alerting {top: ' + top + 'px !important;}');
		}
		document.write('</style>');
		
		
		$(function(){
			sequence = "${sequence}";
			crm = {id:"${CRM_ID_IN_SESSION}"};
			pid = getQueryString("pid");
			getCrm();
			getSetting(pid);
			getLog(pid);
			getFaq(pid);
			getButton(pid);
			get();
			startInterval(3000);
			initEvents();
			quoterUrl = getQueryString("quoter");
			windowShowType = getQueryString("windowShowType");
			windowOpenType = getQueryString("windowOpenType");
			initShow(windowOpenType);
			if(advertHeight > 0) {
				$("#advert").show();
			}
			
			var ty = getQueryString("ty");
			if(ty == "TY") {
				$(".showGuide").show();
			}
		});
		</script>
	</head>

	<body>
		<div id="advert">
			<!-- <img src="../file/load.do?method=get&bizType=SETTING&fileName=advert_70.png"> -->
		</div>
		<div id="pageChat">
			<div id="header">
				<div class="header-left" onclick="hideHeader()">
					<img id="agent-avatar" src="" alt="你好"><img src="../themes/online/how.png" id="themes-guide" class="showGuide" onMouseOver="vTitleTip('themes-guide')"/>
					<div id="agent-info">
						<div id="agent-name"></div>
						<div id="agent-signature"></div>
					</div>
				</div>
				<div class="header-right">
					<em id="toLeaveword"><img class="topButton" src="../themes/online/lw.png" onclick="toggleLw()"></em>						
					<em id="minChat" style="display:none"><img class="topButton" src="../themes/online/common_min.png" onclick="minWidowOnTop()"></em>						
					<em id="closeChat"><img class="topButton" src="../themes/online/common_close.png" onclick="endSessionOnTop()"></em>						
				</div>
			</div>

			<div id="chat-panel">
				<div id="alerting">
					<div id="alertInside">
						this is a tip
					</div>
				</div>
				<div id="msgWrapper">
					<div id="msgHolder">
					</div>
				</div>


				<div id="audioRecording">
				    <div class="luyin-content">
				        <p><span style="font-size: 12px;">正在录音...(已录音<span id="audioTime">0</span>/60秒)</span>
				            <button class="a_talking_button light-btn" id="cancelRecord">取消</button>
				            <button class="a_talking_button light-btn" id="sendRecord">发送</button>
				            <button class="a_talking_button light-btn" id="stopRecord">停止</button>
				        </p>
				    </div>
				</div>	
				<div id="footer">
					<div class="functions">
						<span id="emojiBtn"><i class="chat-icon icon-emoji" title="表情"></i>
						</span>
						<form method="post" enctype="multipart/form-data" class="hideForm" style="display: block; opacity: 10;" id="sendFileForm">
							<a class="chat-icon icon-file" title="发送文件" alt="发送文件"> <input id="sendFile" name="file" type="file" class="hideFile" value="" onchange="uploadFile()"> </a>
						</form>
						<span id="satisfyBtn"><i class="chat-icon icon-satisfy" title="满意度评价"></i> </span>
						<span id="cutBtn"><i class="chat-icon icon-cut" title="截图"></i></span>
						<span id="audioBtn" class="icon-audio" title="发送语音"></span>
						<span id="faqBtn" style="display:none"><i class="chat-icon icon-faq" title="常见问题"></i></span>
						<span id="endBtn" class='functions-img'><img src='../themes/online/end.png' onclick="endSession()"/></span>
						<span id="guideBtn" class='functions-img'><img src="../themes/online/how.png" class="showGuide" onMouseOver="vGuideTip('guideBtn')"/></span>	
					</div>
					<div id="forPc">
						<textarea id="textarea" name="content" style="width: 100%; height: 60px; visibility: hidden;" placeholder="请输入..."></textarea>
					</div>
					<div id="forMobile">
						<table border="0" cellpadding="0" width="100%">
						<tr >
							<td width="80%"><input type="text" id="textarea" name="content" style="width: 100%; height: 30px;" placeholder="请输入..."></input></td>
							<td align="center"><span id="btnSend"><img src='../themes/online/send.png' width='25px' height='25px'  onclick="sendChatMessage()"/></span></td>
							<td align="center"><span id="btnManual"><img src='../themes/online/manual.png' width='25px' height='25px' onclick="transferManual()"/></span></td>
						</tr>
						</table>
					</div>
					<div id="talking_buttons">
						<div class="feature-right">
							<input id="btnEnd" type="button" value="结束会话" class="a_talking_button light-btn" onclick="endSession()" sstyle="display:none">
							<input id="btnManual" type="button" value="转人工" class="a_talking_button light-btn" onclick="transferManual()" style="display: none"><img src="../themes/online/how.png" id="btnManual-guide" class="showGuide" onMouseOver="vBtnMualTip('btnManual-guide')"/>
							<input id="btnSend" type="button" value="发送消息" class="a_talking_button light-btn" onclick="sendChatMessage()" style="display: none">
						</div>
					</div>
				</div>


				<div class="liuyan-body">
					<div id="liuyanForm">
						<div class="liuyan">
							<div class="form-group" id="showLiuyan">
								<p>
									<span>姓名</span>
									<input type="text" id="LW_NAME" name="NAME">
									<strong style="color: red"> *</strong>
								</p>
								<p>
									<span>手机</span>
									<input type="text" id="LW_PHONE" name="PHONE">
									<strong style="color: red"> *</strong>
								</p>
								<p>
									<span>邮箱</span>
									<input type="text" id="LW_EMAIL" name="EMAIL">
									<strong> &nbsp;</strong>
								</p>
								<p>
									<span>Q&nbsp;Q</span>
									<input type="text" id="LW_QQ" name="QQ">
									<strong> &nbsp;</strong>
								</p>
								<p>
									<span>留言</span>
									<textarea rows="5" placeholder="请在此留言，我们会及时联系您!" type="text" id="LW_MEMO" name="LW_MEMO"></textarea>
									<strong style="color: red"> *</strong>
								</p>
							</div>
							<div class="liuyan_buttons">
								<input type="button" class="button light-btn" id="leaveWordBtn" onclick="submitLw()" value="提交留言" />
								<input type="button" class="button" id="returnChatBtn" onclick="toggleLw()" value="返回聊天" />
							</div>
						</div>
					</div>
				</div>
			</div>


			<div id="body-right">
				<div id="right-buttons-pannel">
					<p id="right-buttons-tag">
						快速访问
					</p>
				</div>
				<div class="service-container clearfix" id="button-list">
				</div>
				<div id="right-tab-pannel" class="right-full-div">
					<div class="tab-div">
						<ul id="menu-tab-wrapper">
							<div id="menu0" class="one-tab" onclick='showContentDiv(0)'>
								常见问题
							</div>
							<div id="menu1" class="one-tab" onclick='showContentDiv(1)' style="display: none">
								订单
							</div>
							<div id="menu2" class="one-tab" onclick='showContentDiv(2)' style="display: none">
								最近浏览
							</div>
						</ul>
					</div>
					<div id="tab-content-div-0">
					</div>
					<div id="tab-content-div-1" style="display: none">
						<iframe src="" class="right-full-div" frameborder="0" id="orderFrame" name="orderFrame"></iframe>	
					</div>
					<div id="tab-content-div-2" style="display: none">
					</div>
				</div>
			</div>


			<div id="support-holder">
				<a href="" target="_blank"></a>
			</div>
		</div>
	</body>
</html>

