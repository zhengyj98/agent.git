var urlSplitString = ",";

//确认修改的时候调用
function onsubmitCheck(){
	//先得到要修改的节点的数组
	var qb = getNewArrayFromTree('root');
	if(qb == false) {
		return false;
	}
	
	if(confirm("确定要提交吗?")){		
		makeSubmitField(qb, "forSubmitNewNodes");
		return true;
	}
	return false;
}

//点击‘修改该节点的时候’调用
function modifyTreeNode(){
	var d = new Date();
	var newName = $$('newName');
	var newDesc = $$('newDesc');
	var newIden = $$('newIden');
	
	if(checkNull('newName','名称','newDesc','描述','newIden','标识')==false){
	    return ;
	}
	
	if(/^[a-z|A-Z|0-9]+$/.test(newIden.value)==false){
		alert('请输入英文字母格式');
		newIden.focus();
	    return;
	}
	
	var theId = tree2.getSelectedItemId();
	if(!theId.match(/G_/) && !theId.match(/O_/) && !theId.match(/I_/)){
		alert('暂时修改只支持到第二,三,四层结点');
		return ;
	}
	
	//如果当前结点有子结点，展开
	//tree2.openItem(fatherItemId);
	
	tree2.setItemText(theId, newName.value);
	tree2.setUserData(theId,"desc",newDesc.value);
	tree2.setUserData(theId,"iden",newIden.value);
	
	tree2.setItemColor(theId,'#ff00ff','#ff0088');
	tree2.setCheck(theId,true);
	//设置隐藏
}


//分析得到要修改的权限节点的数组{I_999,I998,O_888,O_889}等等
function getNewArrayFromTree(treeRootId){
	//先确定要修改的权限结点	
    var idsStr = tree2.getAllChecked();
    if(!idsStr || idsStr.length <= 0){
    	alert('必须至少选中一个结点修改');
    	return false;
    } else {//得到要修改的权限节点存放的数组
		var arr = idsStr.split(",");		
		var newArr = new Array();
		var k = 0;
		for(var i=0; i<arr.length; i++) {
			newArr[k] = new Array(arr[i],tree2.getItemText(arr[i]),tree2.getUserData(arr[i],"desc"),tree2.getUserData(arr[i],"iden"),tree2.getUserData(arr[i],"father"));//id,name,desc,iden
			k++;
		}
		return newArr;	
	}
}
