<%@ page contentType="text/html; charset=gbk" language="java" import="java.sql.*" errorPage="" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gbk" />
<title>url关联修改</title>
<script language="javascript" src="./js/url2.js"></script>
<script type="text/javascript" src="../themes/js/jquery.js"></script>
	
<script language="javascript" src="../js/mycommon.js"></script>

<!--界面颜色变化-->
<link rel="stylesheet" href="../themes/skyblue/skyblueMain.css" type="text/css" title="styles1" />
<script language="javascript" src="./js/tree/dhtmlXCommon.js"></script>
<script language="javascript" src="./js/tree/dhtmlXTree.js"></script>	
</head>

<body>
<div id="main">
  	<div id="tab-top">
  		<div id="ptk1">
	  		<div id="lift"></div>
	  		<div id="pt">url关联</div>
	  		<div id="right"></div>
  		</div>
 	</div>
<html:form action="/auth/TAuthUrlDpAct?method=modifyAssociation">
<html:hidden property="id"/>
<html:hidden property="oldItems" />
<html:hidden property="newItems"/>
<html:hidden property="urlType" />
	<div id="table">
   		<div id="ptk">
   			<div id="tabtop-l"></div>
    		<div id="tabtop-z">url关联</div>
    		<div id="tabtop-r1"></div>
    	</div>
  	</div> 
	<div id="main-tablist">			
	<table width="100%">
	  <tr>
	    <td><html:text property="actionUrl" size="120" readonly="true"/></td>
	  </tr>	
      <tr>
        <td valign="top" width="30%"><div id="treeboxbox_tree2" style="width:350; height:218;background-color:#f5f5f5;border :1px solid Silver;; overflow:auto;"></div></td>
        </tr>
	  <tr>
	    <td id="stateBar"></td>
	  </tr>
	  <tr>
		<td>
			<li><html:button styleClass="ny-2-1" property="return" value=" 返回 " onclick="javascript:history.back();"/></li>
			<logic:equal property="auth.url.modify.value" name="root" value="true" scope="session">	
				<li><html:button styleClass="ny-2-1" property="add" value="确定修改" onclick="javascript:onsubmitCheck();"/></li>
			</logic:equal>
	  	</td>
	  </tr>
    </table>    
    </div>	
</html:form>
	<script language="javascript">
			var tree2=new dhtmlXTreeObject("treeboxbox_tree2","100%","100%",0);
			tree2.setImagePath("./imgs/");
			tree2.enableCheckBoxes(1);
			tree2.enableThreeStateCheckboxes(true);
			tree2.loadXML("authTree.xml");
			tree2.setOnClickHandler(doOnClick);
	
	        function doOnClick(nodeId){
			    var name = tree2.getItemText(nodeId);
				var desc = tree2.getUserData(nodeId,"desc");
				var iden = tree2.getUserData(nodeId,"iden");			
				var str = ' id：'+ nodeId +'\t\n 名称：'+ name +'\t\n 描述：'+ desc +'\t\n 标识：'+ iden;
				var stateBar = document.getElementById('stateBar');
				stateBar.innerHTML=str;
            }
			
			function initTreeForSelected(idsStr){
			    if(idsStr && idsStr.length >0){
				    var idsArray = idsStr.split(",");
					alert('此url已关联到 '+idsArray.length+' 项权限!');
					for(var i = 0; i<idsArray.length; i++){
					    tree2.setCheck(idsArray[i],true);
					}
				}
			}
			initTreeForSelected('${idsStr}');			
	</script>
</div>
</body>
</html>
<%@ include file="Dialog.jsp" %>
