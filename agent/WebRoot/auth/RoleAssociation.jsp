<%@ page contentType="text/html; charset=gbk" language="java" import="java.sql.*" errorPage="" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gbk" />
<title>角色关联的帐号管理</title>
<script language="javascript" src="../js/mycommon.js"></script>

<!--界面颜色变化-->
<link rel="stylesheet" href="../themes/skyblue/skyblueMain.css" type="text/css" title="styles1" />
<script language="javascript" src="./js/tree/dhtmlXCommon.js"></script>
<script language="javascript" src="./js/tree/dhtmlXTree.js"></script>	
</head>

<body>
<div id="main">
  	<div id="tab-top">
  		<div id="ptk1">
	  		<div id="lift"></div>
	  		<div id="pt"><span class="STYLE1">角色关联的帐号管理</span></div>
	  		<div id="right"></div>
  		</div>
 	</div>
	<html:form action="/auth/TRoleDpAct?method=modifyAssociation" onsubmit="if(confirm('确定修改该角色的关联帐号吗？')){$$('dialogWindow').style.display='';}else{return false;}" >
	<html:hidden property="id" />
		<div id="table">
			<div id="ptk">
				<div id="tabtop-l"></div>
				<div id="tabtop-z"><span class="STYLE1">角色关联的帐号</span></div>
				<div id="tabtop-r1"></div>
			</div>
		</div> 
		<div id="main-tablist">			
		<table width="95%" align="center" cellpadding="5" cellspacing="1" class="table_query">
		  <tr class="table_header"> 
			<th colspan="15" align="left">
				<logic:equal property="auth.role.association.value" name="root" value="true" scope="session">
					<html:submit property="search" value=" 更改关联 "/>
				</logic:equal>
				<html:button property="back" value=" 返 回 " onclick="javascript:history.back();" />
			</th>
		  </tr>
		  <tr class="table_header">
			<th colspan="15" align="left">全选<input type="checkbox" name="seleceAll" onclick="fSelectAll(this.name);" /> (按工号排序) 已关联的帐号数量: <span class="red">${resultNum}</span></th>
		  </tr>
		  <tr class="table_header">
			<th align="left"><div align="center">工号</div></th>
			<th align="left"><div align="center">工号</div></th>
			<th align="left"><div align="center">工号</div></th>
		  </tr>
		  <logic:present name="result" scope="request">
			<logic:notEmpty name="result">
			  <logic:iterate id="threeRecord" name="result" scope="request">
				<tr height="25">
					<logic:iterate id="aRecord" name="threeRecord" indexId="point" offset="0" length="1">
						<logic:present name="aRecord">
							<td width="33%" align="center" class="bottom">
								<html:multibox property="userIds" value="${aRecord.id}"/>
								${aRecord.userNetId}					
							</td>
						</logic:present>				
					</logic:iterate>
					<logic:iterate id="aRecord" name="threeRecord" indexId="point" offset="1" length="1">
						<logic:present name="aRecord">
						  <td width="33%" align="center" class="bottom">
								<html:multibox property="userIds" value="${aRecord.id}"/>							  ${aRecord.userNetId}							</td>
						</logic:present>							
					</logic:iterate>
					<logic:iterate id="aRecord" name="threeRecord" indexId="point" offset="2" length="1">
						<logic:present name="aRecord">
						  <td width="33%" align="center" class="bottom">
								<html:multibox property="userIds" value="${aRecord.id}"/>							  ${aRecord.userNetId}							</td>
						</logic:present>	
					</logic:iterate>
				</tr>
			  </logic:iterate>
			</logic:notEmpty>
		  </logic:present>
		</table>
	 
		</div>	
	</html:form>
</div>
</body>
</html>
<%@ include file="Dialog.jsp" %>
