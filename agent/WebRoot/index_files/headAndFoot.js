$.ajax({
	url : "header.html",
	success : function(data) {
		$('#header').append(data);
	},
	dataType : 'html'
});
$.ajax({
	url : "footer.html",
	success : function(data) {
		$('#footer').append(data);
	},
	dataType : 'html'
});

function test() {
	var email = $("#email").val();
	if(email == '' || email.indexOf('@') < 0) {
		alert('请输入email');
		$("#email").focus();
	} else {
		$("#submitButton").attr('disabled','disabled'); 
		$.ajax({
			url : "test.html",
			method:"post",
			data : {email:email},
			success : function(data) {
				if(data.result) {
					$('#p_go_to_test').html("邮件已经发送，请登录邮箱进入使用");
					$('#p_go_to_test').show();
				} else {
					alert('邮件发送失败，请电话联系我们13570254864');
				}
				$("#submitButton").attr('disabled', false); 
			},
			error: function(data) {				
				$("#submitButton").attr('disabled', false); 
				alert('邮件发送失败，请电话联系我们13570254864');
			},
			dataType : 'json'
		});
	}
}