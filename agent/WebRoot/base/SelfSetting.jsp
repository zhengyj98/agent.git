<%@ page contentType="text/html; charset=UTF-8" language="java"  errorPage="" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.cellcom.com.cn/myjsp" prefix="myjsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>自助配置</title>
	<link rel="stylesheet" href="../themes/skyblue/skyblueMain.css" type="text/css" title="styles1" />    
	
	
	<!--请在下面增加js-->
   	<script type="text/javascript" src="../js/mycommon.jquery.js"></script>
	<script type="text/javascript" src="../plugins/datepicker/WdatePicker.js"></script>
	<script type="text/javascript" src='../plugins/jquery-x.x.js'> </script>	

	<script>
		function show() {
			var v = $("#chatMaxType").val();
			if(v == 1) {
				$("#chatMaxLimitTr").show();
			} else {
				$("#chatMaxLimitTr").hide();
			}
		}
	</script>
</head>

<body onload="show()">
	<html:form action="/base/TSettingDpAct?method=doModifyOrgSetting" styleId="CommonForm">
		<html:hidden property="tag"/>
		<html:hidden property="level"/>
		<html:hidden property="subject"/>
		<div id="main">
			<div id="tab-top">
				<div id="lift"></div>
				<div id="pt">自助配置</div>
				<div id="right"></div>
			</div>
		
			<div id="fh-flie-2"> </div>
			<div id="main-tablist">
				<table width='100%' border='0' cellpadding='0' cellspacing='0' id='showData'>	
						<tr class='out-70' onMouseOver='this.className="over-70"' onMouseOut='this.className="out-70"'>
							<td class='zw-txt'>机器人类型：</td>
							<td class='zw-txt' align='left'>
								<myjsp:select added='class="s-select"' name='robotType' id='robotType' listname='robotTypeList' value='${result.robotType}'/>
							</td>
						</tr>
						<tr class='out-70' onMouseOver='this.className="over-70"' onMouseOut='this.className="out-70"'>
							<td class='zw-txt'>图灵KEY：</td>
							<td class='zw-txt' align='left'>
								<myjsp:sinput name='tulingKey' id='tulingKey' value='${result.tulingKey}' add="style='width:400px'" line='2'/>
							</td>
						</tr>
						<tr class='out-70' onMouseOver='this.className="over-70"' onMouseOut='this.className="out-70"'>
							<td class='zw-txt'>关键词转人工：</td>
							<td class='zw-txt' align='left'>
								<myjsp:sinput name='keyWordToManual' id='keyWordToManual' value='${result.keyWordToManual}' add="style='width:400px'" line='2'/>
							</td>
						</tr>
						<tr class='out-70' onMouseOver='this.className="over-70"' onMouseOut='this.className="out-70"'>
							<td class='zw-txt'>错误X次转人工：</td>
							<td class='zw-txt' align='left'>
								<myjsp:sinput name='selfErrorLimit' id='selfErrorLimit' value='${result.selfErrorLimit}' add="style='width:30px;height:20px'" line='1'/>[上班时间且组内有坐席在线]
							</td>
						</tr>
						<tr class='out-70' onMouseOver='this.className="over-70"' onMouseOut='this.className="out-70"'>
							<td class='zw-txt'>自动转人工提示：</td>
							<td class='zw-txt' align='left'>
								<myjsp:sinput name='selfAutoToManualTip' id='selfAutoToManualTip' value='${result.selfAutoToManualTip}' add="style='width:400px'" line='2'/>
							</td>
						</tr>
						<tr class='out-70' onMouseOver='this.className="over-70"' onMouseOut='this.className="out-70"'>
							<td class='zw-txt'>机器人名称：</td>
							<td class='zw-txt' align='left'>
								<myjsp:sinput name='selfName' id='selfName' value='${result.selfName}' add="style='width:400px'" line='2'/>
							</td>
						</tr>
						<tr class='out-70' onMouseOver='this.className="over-70"' onMouseOut='this.className="out-70"'>
							<td class='zw-txt'>不懂回答时提示：</td>
							<td class='zw-txt' align='left'>
								<myjsp:sinput name='selfUnknowTip' id='selfUnknowTip' value='${result.selfUnknowTip}' add="style='width:400px'" line='2'/>
							</td>
						</tr>
						<tr class='out-70' onMouseOver='this.className="over-70"' onMouseOut='this.className="out-70"'>
							<td class='zw-txt'>沟通策略：</td>
							<td class='zw-txt' align='left'>
								<myjsp:select added='class="s-select"' name='selfPolicy' id='selfPolicy' listname='selfPolicyList' value='${result.selfPolicy}'/>
							</td>
						</tr>	
						<tr class='out' onMouseOver='this.className="over"' onMouseOut='this.className="out"'>
							<td class='zw-txt' width='250px'>自助提示语：</td>
							<td class='zw-txt' align='left'>
								<myjsp:sinput name='enterFirstTip' id='enterFirstTip' value='${result.enterFirstTip}' add="style='width:400px'" line='2'/>
							</td>
						</tr>
						<tr class='out-70' onMouseOver='this.className="over-70"' onMouseOut='this.className="out-70"'>
							<td class='zw-txt'>自助推送问题：</td>
							<td class='zw-txt' align='left'>
								<myjsp:sinput name='selfSendQuestion' id='selfSendQuestion' value='${result.selfSendQuestion}' add="style='width:400px'" line='2'/>
								<br/>请输入需要自动推送的问题的序号，多个请采用英文都好分割，如：10001,10002
							</td>
						</tr>						
				</table>
				
				<div align="center"> 
					<input type="submit" name="aa" value="确认保存" class='search-2'>&nbsp<input type="button" name="bb" value="返回" class='search-2' onclick="history.back();">
				</div>
			</div>
		</div>
	</html:form>
</body>
</html>
