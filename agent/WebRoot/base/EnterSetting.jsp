<%@ page contentType="text/html; charset=UTF-8" language="java"  errorPage="" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.cellcom.com.cn/myjsp" prefix="myjsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>接入配置</title>
	<link rel="stylesheet" href="../themes/skyblue/skyblueMain.css" type="text/css" title="styles1" />    
	
	
	<!--请在下面增加js-->
   	<script type="text/javascript" src="../js/mycommon.jquery.js"></script>
   	<script type="text/javascript" src="../js/my.upload.js"></script>
	<script type="text/javascript" src="../plugins/datepicker/WdatePicker.js"></script>
	<script type="text/javascript" src='../plugins/jquery-x.x.js'> </script>	
	<script type="text/javascript" src='../plugins/jquery.form.js'> </script>	

</head>

<body>
	<html:form action="/base/TSettingDpAct?method=doModifyOrgSetting" styleId="CommonForm" enctype="multipart/form-data">
		<html:hidden property="tag" styleId="tag"/>
		<html:hidden property="level" styleId="level"/>
		<html:hidden property="subject" styleId="subject"/>
		<div id="main">
			<div id="tab-top">
				<div id="lift"></div>
				<div id="pt">基础配置</div>
				<div id="right"></div>
			</div>
		
			<div id="fh-flie-2"> </div>
			<div id="main-tablist">
				<table width='100%' border='0' cellpadding='0' cellspacing='0' id='showData'>
						<tr class='out-70' onMouseOver='this.className="over-70"' onMouseOut='this.className="out-70"'>
							<td class='zw-txt'>排队发言策略：</td>
							<td class='zw-txt' align='left'>
								<myjsp:select added='class="s-select"' name='queueTalkingPolicy' id='queueTalkingPolicy' listname='queueTalkingPolicyList' value='${result.queueTalkingPolicy}'/>
							</td>
						</tr>
						<tr class='out-70' onMouseOver='this.className="over-70"' onMouseOut='this.className="out-70"'>
							<td class='zw-txt'>排队固定回复语：</td>
							<td class='zw-txt' align='left'>
								<myjsp:sinput name='visitorQueueTip' id='visitorQueueTip' value='${result.visitorQueueTip}' add="style='width:400px'" line='3'/>
							</td>
						</tr>						
						<tr class='out-70' onMouseOver='this.className="over-70"' onMouseOut='this.className="out-70"'>
							<td class='zw-txt'>无坐席登陆时提示：</td>
							<td class='zw-txt' align='left'>
								<myjsp:select added='class="s-select"' name='noOnlineAgentPolicy' id='noOnlineAgentPolicy' listname='noOnlineAgentPolicyList' value='${result.noOnlineAgentPolicy}'/>
							</td>
						</tr>
						<tr class='out-70' onMouseOver='this.className="over-70"' onMouseOut='this.className="out-70"'>
							<td class='zw-txt' width='250px'>按钮引导策略：</td>
							<td class='zw-txt' align='left'>
								<myjsp:select added='class="s-select"' name='guidePolicy' id='guidePolicy' listname='guidePolicyList' value='${result.guidePolicy}'/>
							</td>
						</tr>
						<tr class='out-70' onMouseOver='this.className="over-70"' onMouseOut='this.className="out-70"'>
							<td class='zw-txt'>延迟时间弹框：</td>
							<td class='zw-txt' align='left'>
								<myjsp:sinput name='guidePopLater' id='guidePopLater' value='${result.guidePopLater}' add="style='width:30px;height:20px'" line='1'/>秒
							</td>
						</tr>	
						<tr class='out-70' onMouseOver='this.className="over-70"' onMouseOut='this.className="out-70"'>
							<td class='zw-txt'>弹框引导提示语：</td>
							<td class='zw-txt' align='left'>
								<myjsp:sinput name='guidePopTip' id='guidePopTip' value='${result.guidePopTip}' add="style='width:400px'" line='3'/>
							</td>
						</tr>	
						<tr class='out-70' onMouseOver='this.className="over-70"' onMouseOut='this.className="out-70"'>
							<td class='zw-txt'>沟通策略：</td>
							<td class='zw-txt' align='left'>
								<myjsp:select added='class="s-select"' name='selfPolicy' id='selfPolicy' listname='selfPolicyList' value='${result.selfPolicy}'/>
							</td>
						</tr>				
						<tr class='out-70' onMouseOver='this.className="over-70"' onMouseOut='this.className="out-70"'>
							<td class='zw-txt'>自助引导：</td>
							<td class='zw-txt' align='left'>
								<myjsp:sinput name='enterFirstTip' id='enterFirstTip' value='${result.enterFirstTip}' add="style='width:400px'" line='3'/>
							</td>
						</tr>	
						<tr class='out-70' onMouseOver='this.className="over-70"' onMouseOut='this.className="out-70"'>
							<td class='zw-txt'>选择组的提示：</td>
							<td class='zw-txt' align='left'>
								<myjsp:sinput name='beforeGroupList' id='beforeGroupList' value='${result.beforeGroupList}' add="style='width:400px'" line='3'/>[可以改为推广词]
							</td>
						</tr>	
						<tr class='out-70' onMouseOver='this.className="over-70"' onMouseOut='this.className="out-70"'>
							<td class='zw-txt'>排队时效：</td>
							<td class='zw-txt' align='left'>
								<myjsp:sinput name='queueLimit' id='queueLimit' value='${result.queueLimit}' add="style='width:30px;height:20px'" line='1'/>分钟，访客排队X分钟仍然无坐席接入则自动停止排队，访问失败
							</td>
						</tr>		
						<tr class='out-70' onMouseOver='this.className="over-70"' onMouseOut='this.className="out-70"'>
							<td class='zw-txt'>排队策略：</td>
							<td class='zw-txt' align='left'>
								<myjsp:select added='class="s-select"' name='visitorQueuePolicy' id='visitorQueuePolicy' listname='visitorQueuePolicyList' value='${result.visitorQueuePolicy}'/>
							</td>
						</tr>
						<tr class='out-70' onMouseOver='this.className="over-70"' onMouseOut='this.className="out-70"'>
							<td class='zw-txt'>会话分配策略：</td>
							<td class='zw-txt' align='left'>
								<myjsp:select added='class="s-select"' name='sessionDispatchPolicy' id='sessionDispatchPolicy' listname='sessionDispatchPolicyList' value='${result.sessionDispatchPolicy}' readonly='' showonly='' />				
							</td>
						</tr>	
						<tr class='out-70' onMouseOver='this.className="over-70"' onMouseOut='this.className="out-70"'>
							<td class='zw-txt'>历史会话策略：</td>
							<td class='zw-txt' align='left'>
								<myjsp:select added='class="s-select"' name='historyMessagePolicy' id='historyMessagePolicy' listname='historyMessagePolicyList' value='${result.historyMessagePolicy}'/>
							</td>
						</tr>
				</table>
				
				<div align="center"> 
					<input type="submit" name="aa" value="确认保存" class='search-2'>&nbsp<input type="button" name="bb" value="返回" class='search-2' onclick="history.back();">
				</div>
			</div>
		</div>
	</html:form>
</body>
</html>
