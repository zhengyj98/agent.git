<%@ page contentType="text/html; charset=UTF-8" language="java"  errorPage="" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.cellcom.com.cn/myjsp" prefix="myjsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>时效配置</title>
	<link rel="stylesheet" href="../themes/skyblue/skyblueMain.css" type="text/css" title="styles1" />    
	
	
	<!--请在下面增加js-->
   	<script type="text/javascript" src="../js/mycommon.jquery.js"></script>
	<script type="text/javascript" src="../plugins/datepicker/WdatePicker.js"></script>
	<script type="text/javascript" src='../plugins/jquery-x.x.js'> </script>	

</head>

<body>
	<html:form action="/base/TSettingDpAct?method=doModifyOrgSetting" styleId="CommonForm">
		<html:hidden property="tag"/>
		<html:hidden property="level"/>
		<html:hidden property="subject"/>
		<div id="main">
			<div id="tab-top">
				<div id="lift"></div>
				<div id="pt">时效配置</div>
				<div id="right"></div>
			</div>
		
			<div id="fh-flie-2"> </div>
			<div id="main-tablist">
				<table width='100%' border='0' cellpadding='0' cellspacing='0' id='showData'>
						<tr class='out-70' onMouseOver='this.className="over-70"' onMouseOut='this.className="out-70"'>
							<td class='zw-txt' width='250px'>静默提醒时长：</td>
							<td class='zw-txt' align='left'>
								<myjsp:sinput name='silentNoticeLimit' id='silentNoticeLimit' value='${result.silentNoticeLimit}' add="style='width:30px;height:20px'" line='1'/>分钟
								访客达到X分钟没有任何发言，系统将自动下发提示，如果设置为0则表示不做任何提示。
							</td>
						</tr>
						<tr class='out-70' onMouseOver='this.className="over-70"' onMouseOut='this.className="out-70"'>
							<td class='zw-txt'>静默提示语：</td>
							<td class='zw-txt' align='left'>
								<myjsp:sinput name='silentNoticeTip' id='silentNoticeTip' value='${result.silentNoticeTip}' add="style='width:400px'" line='3'/>
							</td>
						</tr>
						<tr class='out-70' onMouseOver='this.className="over-70"' onMouseOut='this.className="out-70"'>
							<td class='zw-txt'>静默结束时长：</td>
							<td class='zw-txt' align='left'>
								<myjsp:sinput name='silentEndLimit' id='silentEndLimit' value='${result.silentEndLimit}' add="style='width:30px;height:20px'" line='1'/>分钟
								访客达到X分钟没有任何发言，会话将被自动结束，如果设置为0则表示不自动结束。
							</td>
						</tr>
						<tr class='out-70' onMouseOver='this.className="over-70"' onMouseOut='this.className="out-70"'>
							<td class='zw-txt'>静默结束提示语：</td>
							<td class='zw-txt' align='left'>
								<myjsp:sinput name='silentEndTip' id='silentEndTip' value='${result.silentEndTip}' add="style='width:400px'" line='3'/>
							</td>
						</tr>
						<tr class='out-70' onMouseOver='this.className="over-70"' onMouseOut='this.className="out-70"'>
							<td class='zw-txt'>排队时效：</td>
							<td class='zw-txt' align='left'>
								<myjsp:sinput name='queueLimit' id='queueLimit' value='${result.queueLimit}' add="style='width:30px;height:20px'" line='1'/>分钟，访客排队X分钟仍然无坐席接入则自动停止排队，访问失败
							</td>
						</tr>		
				</table>
				
				<div align="center"> 
					<input type="submit" name="aa" value="确认保存" class='search-2'>&nbsp<input type="button" name="bb" value="返回" class='search-2' onclick="history.back();">
				</div>
			</div>
		</div>
	</html:form>
</body>
</html>
