<%@ page contentType="text/html; charset=UTF-8" language="java"  errorPage="" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.cellcom.com.cn/myjsp" prefix="myjsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>坐席配置</title>
	<link rel="stylesheet" href="../themes/skyblue/skyblueMain.css" type="text/css" title="styles1" />    
	
	
	<!--请在下面增加js-->
   	<script type="text/javascript" src="../js/mycommon.jquery.js"></script>
	<script type="text/javascript" src="../plugins/datepicker/WdatePicker.js"></script>
	<script type="text/javascript" src='../plugins/jquery-x.x.js'> </script>	

</head>

<body>
	<html:form action="/base/TSettingDpAct?method=doModifyOrgSetting" styleId="CommonForm">
		<html:hidden property="tag"/>
		<html:hidden property="level"/>
		<html:hidden property="subject"/>
		<div id="main">
			<div id="tab-top">
				<div id="lift"></div>
				<div id="pt">坐席配置</div>
				<div id="right"></div>
			</div>
		
			<div id="fh-flie-2"> </div>
			<div id="main-tablist">
				<table width='100%' border='0' cellpadding='0' cellspacing='0' id='showData'>	
						<tr class='out-70' onMouseOver='this.className="over-70"' onMouseOut='this.className="out-70"'>
							<td class='zw-txt' width='250px'>满意度总分：</td>
							<td class='zw-txt' align='left'>
								<myjsp:sinput name='satisfyLimit' id='satisfyLimit' value='${result.satisfyLimit}' add="style='width:30px;height:20px'" line='1'/>分，访客端将最多显示X个分数选择，最大为5个
							</td>
						</tr>
						<tr class='out-70' onMouseOver='this.className="over-70"' onMouseOut='this.className="out-70"'>
							<td class='zw-txt'>评价方式：</td>
							<td class='zw-txt' align='left'>
								<myjsp:select added='class="s-select"' name='satisfyInviteType' id='satisfyInviteType' listname='satisfyInviteTypeList' value='${result.satisfyInviteType}' radio='1'></myjsp:select>
							</td>
						</tr>
						<tr class='out-70' onMouseOver='this.className="over-70"' onMouseOut='this.className="out-70"'>
							<td class='zw-txt'>邀请文案：</td>
							<td class='zw-txt' align='left'>
								<myjsp:sinput name='satisfyInviteTip' id='satisfyInviteTip' value='${result.satisfyInviteTip}' add="style='width:400px'" line='3'/>
							</td>
						</tr>
						<tr class='out-70' onMouseOver='this.className="over-70"' onMouseOut='this.className="out-70"'>
							<td class='zw-txt'>评价感谢：</td>
							<td class='zw-txt' align='left'>
								<myjsp:sinput name='satisfyThanksTip' id='satisfyThanksTip' value='${result.satisfyThanksTip}' add="style='width:400px'" line='3'/>
							</td>
						</tr>
						<tr class='out-70' onMouseOver='this.className="over-70"' onMouseOut='this.className="out-70"'>
							<td class='zw-txt'>描述评论：</td>
							<td class='zw-txt' align='left'>
								<myjsp:select added='class="s-select"' name='satisfyInput' id='satisfyInput' listname='satisfyInputList' value='${result.satisfyInput}'/>
							</td>
						</tr>
				</table>
				
				<div align="center"> 
					<input type="submit" name="aa" value="确认保存" class='search-2'>&nbsp<input type="button" name="bb" value="返回" class='search-2' onclick="history.back();"/>
				</div>
			</div>
		</div>
	</html:form>
</body>
</html>
