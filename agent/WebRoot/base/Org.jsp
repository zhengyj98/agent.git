<%@ page contentType="text/html; charset=UTF-8" language="java"  errorPage="" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.cellcom.com.cn/myjsp" prefix="myjsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<%
	String host = request.getLocalAddr();
	int port = request.getServerPort();
	String path = request.getContextPath();
%>	
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>企业信息</title>
	<link rel="stylesheet" href="../themes/skyblue/skyblueMain.css" type="text/css" title="styles1" />    
	
	
	<!--请在下面增加js-->
   	<script type="text/javascript" src="../js/mycommon.jquery.js"></script>
	<script type="text/javascript" src="../plugins/datepicker/WdatePicker.js"></script>
	<script type="text/javascript" src='../plugins/jquery-x.x.js'> </script>
	<script type="text/javascript" src="//www.jianyuekefu.com:8080/js/online/visitor-access.js?pid=${attribute.pid}&channel=10001&ty=TY"></script>	
	<script type="text/javascript" src="//www.jianyuekefu.com:8080/plugins/layer/layer.js"></script>
	<script>
		$(document).ready(function () {
			var firstLogon = "${firstLogon}";
			if(firstLogon == "true") {
				layer.confirm("您首次登陆，请先尝试下坐席与访客聊天吧", {title:"确认", shadeClose:true, btn:["尝试", "暂不"]}, function () {
					layer.closeAll();
					window.location="../agent/AgentDpAct.do?method=online";
					jianyueKefu();
				});
			}
		});
	</script>
</head>

<body><br />
	<html:form action="/base/TSettingDpAct?method=doModifyOrgSetting" styleId="CommonForm">
		l<html:hidden property="tag"/>
		<html:hidden property="level"/>
		<html:hidden property="subject"/>
		<div id="main">
			<div id="tab-top">
				<div id="lift"></div>
				<div id="pt">企业信息</div>
				<div id="right"></div>
			</div>
		
			<div id="fh-flie-2"> </div>
			<div id="main-tablist">
				<table width='100%' border='0' cellpadding='0' cellspacing='0' id='showData'>			
						<tr class='out-70' onMouseOver='this.className="over-70"' onMouseOut='this.className="out-70"'>
							<td class='zw-txt' width='250px'>企业：</td>
							<td class='zw-txt' align='left'>
								${org.name}
							</td>
						</tr>
						<tr class='out-70' onMouseOver='this.className="over-70"' onMouseOut='this.className="out-70"'>
							<td class='zw-txt' width='250px'>企业ID：</td>
							<td class='zw-txt' align='left'>
								${org.pid}
							</td>
						</tr>	
						<tr class='out-70' onMouseOver='this.className="over-70"' onMouseOut='this.className="out-70"'>
							<td class='zw-txt' width='250px'>联系电话：</td>
							<td class='zw-txt' align='left'>
								${org.phone}
							</td>
						</tr>		
						<tr class='out-70' onMouseOver='this.className="over-70"' onMouseOut='this.className="out-70"'>
							<td class='zw-txt' width='250px'>注册时间：</td>
							<td class='zw-txt' align='left'>
								${org.insertTime}
							</td>
						</tr>	
						<tr class='out-70' onMouseOver='this.className="over-70"' onMouseOut='this.className="out-70"'>
							<td class='zw-txt' width='250px'>我是：</td>
							<td class='zw-txt' align='left'>
								【${attribute.username}】【${attribute.name}】【${attribute.nickName}】【${attribute.phone}】
							</td>
						</tr>		
						<tr class='out-70' onMouseOver='this.className="over-70"' onMouseOut='this.className="out-70"'>
							<td colspan="2">
								<a href="../agent/AgentDpAct.do?method=online" class="zw-txt-big">进入客服服务</a>
								&nbsp;&nbsp;<a href="../Logon/LogonDpAct.do?method=logout" class="zw-txt-big">退出系统</a>
							</td>
						</tr>			
						<tr class='out-70' onMouseOver='this.className="over-70"' onMouseOut='this.className="out-70"'>
							<td colspan="2" align="left">
								<span class='zw-txt' style="margin-left:100px"><img src="../themes/online/warning.png"/>我的网站如何接入：&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp[左边菜单]--基础信息--<a href='../base/TChannelDpAct.do?method=list'>渠道</a>--获取代码--代码嵌入您的网站</span><br/>
								<span class='zw-txt' style="margin-left:100px"><img src="../themes/online/warning.png"/>不同网站如何区分来源：[左边菜单]--基础信息--<a href='../base/TChannelDpAct.do?method=list'>渠道</a>--新增渠道</span>
							</td>
						</tr>			
				</table>
			</div>
		</div>
	</html:form>
</body>
</html>
