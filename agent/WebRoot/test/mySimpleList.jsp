<%@ page contentType="text/html; charset=UTF-8" language="java"  errorPage="" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.cellcom.com.cn/mytag" prefix="mytag"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript" src="js/my.simple.js"></script>
<script type="text/javascript" src="js/my.upload.js"></script>
<script type="text/javascript" src='plugins/jquery-x.x.js'> </script>
<script type="text/javascript" src='plugins/jquery.form.js'> </script>
<script type="text/javascript" src="plugins/layer/layer.js"></script>
<script type="text/javascript" src="plugins/datepicker/WdatePicker.js"></script>


<title>分页查询</title>
</head>
<body>
<form action="mySimple.so?list&entityName=student" method="POST">
	姓名：<mytag:input name="name" type="text" value="${entity.name}" showonly="${showonly}"/><p/>
	年龄：<mytag:input name="age" type="text" value="${entity.age}" showonly="${showonly}"/><p/>
	级别：<mytag:select name="level" list="1:高一;2:高二;3:高三" value="" append=":所有"/>
	<mytag:input name="" type="submit" value="查询"/>
	<mytag:input name="" type="button" value="新增" added="onclick=\"javascript:goAdd()\""/>
	<table>
	<tr>
		<td>姓名</td>
		<td>年龄</td>
		<td>性别</td>
		<td>级别</td>
		<td>操作</td>
	</tr>
	<c:forEach var="item" items="${result}">
		<tr>
			<td>${item.name}</td>
			<td>${item.age}</td>
			<td>${item.sex}</td>
			<td><mytag:select name="level" list="0:未知;1:高一;2:高二;3:高三" value="${item.level}" showonly="true" fill="0"/></td>
			<td>
			<a href="javascript:layerConfirm('是否确认删除', 'mySimple.so?delete&id=${item.id}&entityName=student', null, deleteSuccess)">删除</a>
			<mytag:input name="" type="button" value="删除" added="onclick=\"javascript:layerConfirm('是否确认删除', 'mySimple.so?delete&id=${item.id}&entityName=student', null, deleteSuccess)\""/>
			<mytag:input name="" type="button" value="编辑" added="onclick=\"javascript:goAdd('${item.id}')\""/>
			</td>
		</tr>
	</c:forEach>
		<tr>
			<td colspan="2">${link}</td>
		</tr>
	</table>
</form>
<script type="text/javascript">
	function goAdd(id) {
		layer.open({
			type : 2,
			closeBtn : 1,
			title : "添加用户",
			content : "mySimple.so?go&entityName=student&id=" + id,
			move : false,
			maxmin : true,
			area : [ '80%', '80%' ],
			shadeClose : true,
			cancel: function(){ 
			    //右上角关闭回调
			    layer.closeAll();
			}
		});
	}
	function deleteSuccess(data) {
		layer.msg(data.message);
		window.location.reload();
	}
</script>	
</body>
</html>