<%@ page contentType="text/html; charset=UTF-8" language="java"  errorPage="" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.cellcom.com.cn/mytag" prefix="mytag"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript" src="js/my.simple.js"></script>
<script type="text/javascript" src="js/my.upload.js"></script>
<script type="text/javascript" src='plugins/jquery-x.x.js'> </script>
<script type="text/javascript" src='plugins/jquery.form.js'> </script>
<script type="text/javascript" src="plugins/layer/layer.js"></script>
<script type="text/javascript" src="plugins/datepicker/WdatePicker.js"></script>


<title>控件案例</title>
</head>
<body>
<form action="mySimple.so?addOrUpdate">
	<mytag:input name="entityName" type="hidden" value="student"/>
	<mytag:input name="idSetter" type="hidden" value="id"/>
	<mytag:input name="id" type="hidden" value="${entity.id}"/>
	姓名：<mytag:input name="name" type="text" require="require" value="${entity.name}" showonly="${showonly}"/><p/>
	年龄：<mytag:input name="age" type="text" require="require;number" value="${entity.age}" showonly="${showonly}"/><p/>
	生日：<mytag:input name="birthday" type="date" require="" value="${entity.birthday}" showonly="${showonly}"/><p/>
	性别：<mytag:select name="sex" list="男;女" type="radio" value="${entity.sex}" showonly="${showonly}" default2="女"/><p/>
	年级：<mytag:select name="level" list="1:高一;2:高二;3:高三" value="${entity.level}" default2="2" showonly="${showonly}"/><p/>
	时间点：<mytag:select name="t" list="hourList23" type="select" value="${entity.t}" default2="21;22" showonly="${showonly}"/><p/>
	照片:<mytag:input name="file" type="file" url="file/load.do?method=upload" token=""/><p/>
	<c:if test="${showonly == null}">
		<mytag:input name="submitbutton" type="ajax" value="提交"/><p/>
	</c:if>
</form>
<script type="text/javascript">
	function deleteSuccess(data) {
		layer.msg(data.message);
	}
	function callbackSuccess(data) {
		layer.confirm('操作成功，是否继续添加？', {
			title : '确认操作',
			shadeClose : true,
			btn : [ '不继续', '继续' ]
		}, function() {
			parent.window.location.reload();
		}, function() {
			layer.msg("请继续填写");
		});
	}
	function callbackError(data) {
		layer.msg(data.message);
	}
	function uploadResult(data) {
		if(data.code==0) {
			layer.msg("上传文件成功");
		} else {
			layer.msg("上传文件失败");
		}
	}
</script>
</body>
</html>