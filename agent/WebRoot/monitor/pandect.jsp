<%@ page contentType="text/html; charset=UTF-8" language="java"  errorPage="" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.cellcom.com.cn/myjsp" prefix="myjsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>监控总览</title>
	<link rel="stylesheet" href="../themes/skyblue/skyblueMain.css" type="text/css" title="styles1" />    
	
	
		<!--请在下面增加js-->
		<script type="text/javascript" src='../plugins/jquery-x.x.js'> </script>
		<script type="text/javascript" src='../js/mycommon.jquery.js'> </script>
		<script type="text/javascript" src="../plugins/layer/layer.js"></script>
		<script type="text/javascript" src='../js/online/all-tip.js'> </script>
		<script>
			
			var counting = 0;
			function readMonitor(){
				var getToday = counting % 6 == 0 ? "Y" : "N";
				commit('../monitor/MonitorDpAct.do?method=pandect', {getToday : getToday}, function(resp){
					$("#agentCount").html(resp.agentCount);
					$("#agentOnlineCount").html(resp.agentOnlineCount);
					$("#agentGoawayCount").html(resp.agentGoawayCount);
					$("#sessionCount").html(resp.sessionCount);
					$("#selfTalkingCount").html(resp.selfTalkingCount);
					$("#queueingCount").html(resp.queueingCount);
					$("#agentAvailableCount-inner").html(resp.agentAvailableCount);
					$("#agentTalkingCount-inner").html(resp.agentTalkingCount);
					$("#otherCount").html(resp.otherCount);
					if(getToday == "Y") {
						$("#todaySessionCount").html(resp.todaySessionCount);
						$("#todayCrmCount").html(resp.todayCrmCount);
						$("#todayAgentCount").html(resp.todayAgentCount);
						$("#todayAgentSessionCount").html(resp.todayAgentSessionCount);
						$("#todayAgentSessionAvgCount").html(resp.todayAgentCount == 0 ? 0 : resp.todayAgentSessionCount / resp.todayAgentCount);
					}
					counting++;
					$("#counting").html(counting);
					
					setTimeout(readMonitor, 5000);			
				});
			}
			
			readMonitor();
		</script>
	<style>
		#main-tab {
			border-bottom-style: solid;
	    margin-top: 17px;
	    border-bottom-width: 1px;
		}
		.zw-txt{
			font-size: 19px;
		}
		span{
			font-size: 19px;
		}
	</style>
</head>

<body>
<div id="main">
	<div id="tab-top">
		<div id="ptk1">
			<div id="lift"></div>
			<div id="pt">监控总览[<span id="counting"></span>]</div>
			<div id="right"></div>
		</div>
	</div>
	<div id="main-tab">
			<table width="911" align="center" class="table-slyle-hs" >		
				<tr>
					<td width="20%" class="zw-txt" style="height: 80px;" onMouseOver="showFunctionTip(this.id)" id="T1">当前在线客服数：<span id="agentCount">${agentCount}</span></td>
					<td width="20%" class="zw-txt" onMouseOver="showFunctionTip(this.id)" id="T2">服务中客服数：<span id="agentOnlineCount">${agentOnlineCount}</span></td>
					<td width="20%" class="zw-txt" onMouseOver="showFunctionTip(this.id)" id="T3">暂停中客服数：<span id="agentGoawayCount">${agentGoawayCount}</span></td>
					<td width="20%" class="zw-txt"></td>
					<td width="20%" class="zw-txt"></td>
				</tr>
				<tr>
					<td width="20%" class="zw-txt" style="height: 80px;" onMouseOver="showFunctionTip(this.id)" id="T4">当前会话数：<span id="sessionCount">${sessionCount}</span></td>
					<td width="20%" class="zw-txt" onMouseOver="showFunctionTip(this.id)" id="T5">自助聊天中：<span id="selfTalkingCount">${selfTalkingCount}</span></td>
					<td width="20%" class="zw-txt" onMouseOver="showFunctionTip(this.id)" id="T6">排队中：<span id="queueingCount">${queueingCount}</span></td>
					<td width="20%" class="zw-txt"  onMouseOver="showFunctionTip(this.id)" id="T7">有效聊天中：<span id="agentAvailableCount-inner">${agentAvailableCount}</span><br/>坐席聊天中：<span id="agentTalkingCount-inner">${agentTalkingCount}</span></td>
					<td width="20%" class="zw-txt"  onMouseOver="showFunctionTip(this.id)" id="T8">其他状态：<span id="otherCount">${otherCount}</span></td>
				</tr>
				<tr>
					<td width="20%" class="zw-txt" style="height: 80px;" onMouseOver="showFunctionTip(this.id)" id="A1">今日会话数：<span id="todaySessionCount">${todaySessionCount}</span></td>
					<td width="20%" class="zw-txt" onMouseOver="showFunctionTip(this.id)" id="A2">今日访客数：<span id="todayCrmCount">${todayCrmCount}</span></td>
					<td width="20%" class="zw-txt" onMouseOver="showFunctionTip(this.id)" id="A3">今日服务坐席数：<span id="todayAgentCount">${todayAgentCount}</span></td>
					<td width="20%" class="zw-txt" onMouseOver="showFunctionTip(this.id)" id="A4">今日坐席接待数：<span id="todayAgentSessionCount">${todayAgentSessionCount}</span></td>
					<td width="20%" class="zw-txt" onMouseOver="showFunctionTip(this.id)" id="A5">今日坐席评价接待数：<span id="todayAgentSessionAvgCount">${todayAgentSessionAvgCount}</span></td>
				</tr>
			</table>
	</div>

</div>
</body>
</html>
