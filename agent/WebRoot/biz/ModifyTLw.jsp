<%@ page contentType="text/html; charset=UTF-8" language="java"  errorPage="" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.cellcom.com.cn/myjsp" prefix="myjsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>修改留言</title>
	<link rel="stylesheet" href="../themes/skyblue/skyblueMain.css" type="text/css" title="styles1" />    
	
	
	<!--请在下面增加js-->
   	<script type="text/javascript" src="../js/mycommon.js"></script>
	<script type="text/javascript" src="../plugins/datepicker/WdatePicker.js"></script>
	<script type="text/javascript" src='../plugins/jquery-x.x.js'> </script>
	<script type="text/javascript" src="../js/jquery.validate.js"></script>
	<script type="text/javascript" src="../js/jquery.mine.js"></script>	
	<script type="text/javascript" src="../js/TLw.js"></script>
</head>

<body>
<div id="main">
  	<div id="tab-top">
  		<div id="ptk1">
	  		<div id="lift"></div>
	  		<div id="pt">修改留言</div>
  		    <div id="right"></div>
  		</div>
 	</div>
	<div id="table">
	    <div id="ptk">
	   		<div id="tabtop-l"></div>
	    	<div id="tabtop-z">输入信息</div>
	    	<div id="tabtop-r1"></div>
      </div>
	</div>
	<div id="main-tab">
		<html:form action="/biz/TLwDpAct?method=doModify" styleId="TLwForm" >
			<html:hidden property='id' name='result' /><html:hidden property='qq' name='result' /><html:hidden property='memo' name='result' />
			<table width="911" align="center" class="table-slyle-hs" >
				<tr>
					<td>姓名：</td>
					<td>${result.name}*</td>
				</tr>
				<tr>
					<td>联系电话：</td>
					<td>${result.phone}</td>
				</tr>
				<tr>
					<td>Email：</td>
					<td>${result.email}</td>
				</tr>
				<tr>
					<td>状态：</td>
					<td><myjsp:select name='status' added='class="s-select"' id='status' listname='statuslist' value='${result.status}' readonly=' ' showonly=''/></td>
				</tr>
				<tr>
					<td>处理意见：</td>
					<td><html:textarea property='handleResult' styleId='handleResult' value='${result.handleResult}' rows='5' cols='70' /></td>
				</tr>				
				<tr>
					<td align="right"><input name="submit" type="submit" class="search-2" value="修改"/></td>
					<td><input name="fanhui" type="button" class="search-2" value="返回" onclick="window.location='../common/Common.do?method=goback&&action=goback&pgUri=${pgUri}'"/></td>					 
				</tr>				
			</table>
		</html:form>
	</div>

</div>
</body>
</html>
