<%@ page contentType="text/html; charset=UTF-8" language="java"  errorPage="" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.cellcom.com.cn/myjsp" prefix="myjsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>访客会话详细信息</title>
	<link rel="stylesheet" href="../themes/skyblue/skyblueMain.css" type="text/css" title="styles1" />    
	
	<!--请在下面增加js-->
   	<script type="text/javascript" src="../js/mycommon.js"></script>
	<script type="text/javascript" src='../plugins/jquery-x.x.js'> </script>
</head>

<body>
<div id="main">
	<div id="tab-top">
		<div id="ptk1">
			<div id="lift"></div>
			<div id="pt">访客会话详细信息</div>
			<div id="right"></div>
		</div>
	</div>
	
	<div id="table">
		<div id="ptk">
			<div id="tabtop-l"></div>
			<div id="tabtop-z">详细信息</div>
			<div id="tabtop-r1"></div>
		</div>
	</div>
	
	<div id="main-tab">	
		<html:form action="/biz/TSessionDpAct?method=doModify">	
		<html:hidden property='id' value='${result[0].id}' />
	
		<table width="911" align="center" class="table-slyle-hs" >	
			<tr>
				<td width="15%">会话id：</td>
				<td width="35%">${result[0].id}</td>
				<td width="15%">请求时间：</td>
				<td width="35%"><myjsp:date value='${result[0].requestTime}' format='yyyy-MM-dd HH:mm:ss' /></td>
			</tr>
			<tr>
				<td>渠道：</td>
				<td>${result[0].channel}</td>
				<td>请求组：</td>
				<td>${result[2].name}</td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td>自助聊天时间：</td>
				<td><myjsp:date value='${result[0].robotTime}' format='yyyy-MM-dd HH:mm:ss' /></td>
				<td>排队时间：</td>
				<td><myjsp:date value='${result[0].queueTime}' format='yyyy-MM-dd HH:mm:ss' /></td>
			</tr>
			<tr>
			</tr>
			<tr>
				<td>坐席接入时间：</td>
				<td><myjsp:date value='${result[0].agentTime}' format='yyyy-MM-dd HH:mm:ss' /></td>
				<td>访客结束时间：</td>
				<td><myjsp:date value='${result[0].endTime}' format='yyyy-MM-dd HH:mm:ss' /></td>
			</tr>
			<c:if test="${result[0].agentTime != null}">
				<tr>
					<td>坐席：</td>
					<td>${result[0].agent}</td>
					<td>坐席昵称：</td>
					<td>${result[0].agentName}</td>
				</tr>
				<tr>
					<td>坐席首次响应：</td>
					<td><myjsp:date value='${result[0].firstAgentResponseTime}' format='yyyy-MM-dd HH:mm:ss' /></td>
					<td>坐席消息数：</td>
					<td>${result[0].agentMessageCount}</td>
				</tr>
			</c:if>
			<tr>
				<td>访客消息数：</td>
				<td>${result[0].messageCount}</td>
				<td>系统消息：</td>
				<td>${result[0].systemMessageCount}</td>
			</tr>
			<tr>
				<td>满意度：</td>
				<td>${result[0].satisfyScore}</td>
				<td>满意度评论：</td>
				<td>${result[0].satisfyText}</td>
			</tr>
			<tr>
				<td>结束原因：</td>
				<td><myjsp:select name='endType' width='100' id='endType' listname='endTypelist' first='----' value='${result[0].endType}' readonly='' showonly='true'/></td>
				<td>最后步骤：</td>
				<td><myjsp:select name='step' width='100' id='step' listname='steplist' first='----' value='${result[0].step}' readonly='' showonly='true'/></td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td>访客姓名：</td>
				<td>${result[1].name}</td>
				<td>访客电话：</td>
				<td>${result[1].phone}</td>
			</tr>
			<tr>
				<td>ip：</td>
				<td>${result[0].ip}</td>
				<td>归属地：</td>
				<td>${result[0].province}${result[0].city}</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td><input type="button" class="search-2" value="返回" onclick="history.back()" /></td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
		</table>
		</html:form>
	</div>
</div>
</body>
</html>
