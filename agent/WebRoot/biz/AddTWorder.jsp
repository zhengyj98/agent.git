<%@ page contentType="text/html; charset=UTF-8" language="java"  errorPage="" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.cellcom.com.cn/myjsp" prefix="myjsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>创建工单</title>
	<link rel="stylesheet" href="../themes/skyblue/skyblueMain.css" type="text/css" title="styles1" />    
	<link rel="stylesheet" href="../plugins/select2/select2.min.css" type="text/css" title="styles1" />    
	
	<!--请在下面增加js-->
   	<script type="text/javascript" src="../js/mycommon.js"></script>
	<script type="text/javascript" src='../plugins/jquery-x.x.js'> </script>
	<script type="text/javascript" src='../plugins/tinymce/tinymce.min.js'> </script>
	<script type="text/javascript" src='../plugins/tinymce/jquery.tinymce.min.js'> </script>
	<script type="text/javascript" src='../plugins/tinymce/tinymce-extend.js'> </script>
	<script type="text/javascript" src="../js/jquery.validate.js"></script>
	<script type="text/javascript" src="../js/jquery.mine.js"></script>
	<script type="text/javascript" src="../js/TWorder.js"></script>	
	<script type="text/javascript" src="../js/TWorder2.js"></script>	
	<script type="text/javascript" src="../plugins/layer/layer.js"></script>
	<script type="text/javascript" src="../plugins/select2/select2.js"></script>
	<style type="">
		.addCrm {
		    width: 15px !important;
		    height: 15px !important;
		    color: #fff;
		    background-color: #039be5;
		    display: inline-block;
		    border-radius: 50% !important;
		    text-align: center;
		    line-height: 13px;
		    padding: 0;
		    margin-right: 6px;
		    cursor:pointer;
		}
		li {
		    float: inherit;
		    text-indent: 0px;
		}
	</style>
</head>

<body>
<div id="main">
	<div id="tab-top">
		<div id="ptk1">
			<div id="lift"></div>
			<div id="pt">创建工单</div>
			<div id="right"></div>
		</div>
	</div>
	<div id="table">
		<div id="ptk">
			<div id="tabtop-l"></div>
			<div id="tabtop-z">输入信息</div>
			<div id="tabtop-r1"></div>
		</div>
	</div>
	<div id="main-tab">
		<html:form action="/biz/TWorderDpAct?method=doAdd" styleId="TWorderForm" >
			<c:if test="${session != null}">
				<input type="hidden" name="createObject" value="${session.id}"/>
				<input type="hidden" name="createSource" value="IM"/>
			</c:if>
			<p align="center">${result}</p>		
			<table width="911" align="center" class="table-slyle-hs" >		
				<tr>
					<td>状态：</td>
					<td><myjsp:select name='status' added='class="s-select"' id='status' listname='statuslist' value='' readonly='' showonly='' />*</td>
					<td>等级：</td>
					<td><myjsp:select name='level' added='class="s-select"' id='level' listname='levellist' value='' readonly='' showonly='' />*</td>
				</tr>
				<tr>
					<td>客户：</td>
					<td>
						<c:if test="${crm == null}">
							<select name="crm" id="crm" class="s-select">
								<option value="">请选择客户</option>
							</select>*
						</c:if>
						<c:if test="${crm != null}">
							${crm.name}[电话：${crm.phone}]
							<input type="hidden" name="crm" value="${crm.id}"/>
						</c:if>
						<span class="addCrm" onclick="showAddCrm()">+</span>
					</td>
					<td>受理坐席：</td>
					<td>
						<select name="toAgent" id="toAgent" class="s-select">
							<option value="">请选择受理坐席</option>
						</select>*
					</td>
				</tr>
				<tr>
					<td>主题：</td>
					<td colspan="3"><html:text property='subject' value="${subject}" styleClass='s-input' style="width:800px" maxlength='100'/></td>
				</tr>
				<tr>
					<td>工单内容：</td>
					<td colspan="3">
						<html:textarea property='context' styleId='context' rows='4' cols='50' />
					</td>
				</tr>

				<tr>
					<td align="right"><input type="submit" class="search-2" value="创建"/></td>
					<td colspan="3"><input type="button" class="search-2" value="返回" onclick="window.location='../common/Common.do?method=goback&&action=goback&pgUri=${pgUri}'"/></td>
				</tr>
			</table>
		</html:form>
	</div>

</div>
</body>
</html>
