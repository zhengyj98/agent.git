<%@ page contentType="text/html; charset=UTF-8" language="java"  errorPage="" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.cellcom.com.cn/myjsp" prefix="myjsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>工单详情</title>
	<link rel="stylesheet" href="../themes/skyblue/skyblueMain.css" type="text/css" title="styles1" />    
	<link rel="stylesheet" href="../plugins/select2/select2.min.css" type="text/css" title="styles1" />    
	
	<!--请在下面增加js-->
   	<script type="text/javascript" src="../js/mycommon.js"></script>
</head>

<body>
<div id="main">
  	<div id="tab-top">
  		<div id="ptk1">
	  		<div id="lift"></div>
	  		<div id="pt">工单详情</div>
  		    <div id="right"></div>
  		</div>
 	</div>
	<div id="main-tab">
			<table width="911" align="center" class="table-slyle-hs" >
				<tr>
					<td>工单号：</td>
					<td>${result.sn}</td>
					<td>创建于：</td>
					<td><myjsp:date value='${result.createTime}' format='yyyy-MM-dd HH:mm:ss' /></td>
				</tr>
				<tr>
					<td>创建源：</td>
					<td><myjsp:select name='createSource' added="class='s-select'" id='createSource' listname='createSourcelist' value='${result.createSource}' readonly='' showonly='true'/></td>
					<td>创建对象：</td>
					<td>${result.createObject}</td>
				</tr>	
				<tr>
					<td>状态：</td>
					<td><myjsp:select name='status' added="class='s-select'" id='status' listname='statuslist' value='${result.status}' readonly='' showonly='true'/>*</td>
					<td>等级：</td>
					<td><myjsp:select name='level' added="class='s-select'" id='level' listname='levellist' value='${result.level}' readonly=' ' showonly='true'/></td>
				</tr>
				<tr>
					<td>客户：</td>
					<td>${result.crm}</td>
					<td>受理坐席：</td>
					<td>${result.toAgent}</td>
				</tr>			
				<tr>
					<td>主题：</td>
					<td colspan="3">${result.subject}</td>
				</tr>
				<tr>
					<td>工单内容：</td>
					<td colspan="3">${result.context}</td>
				</tr>	
				<tr>
					<td colspan="4"><input name="fanhui" type="button" class="search-2" value="修改" onclick="window.location='../biz/TWorderDpAct.do?method=modify&id=${result.id}'"/></td>					 
				</tr>			
			</table>
	</div>

</div>
</body>
</html>
