<%@ page contentType="text/html; charset=UTF-8" language="java"  errorPage="" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.cellcom.com.cn/myjsp" prefix="myjsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>工单管理</title>
	<link rel="stylesheet" href="../themes/skyblue/skyblueMain.css" type="text/css" title="styles1" />    
	
	<!--请在下面增加js-->
   	<script type="text/javascript" src="../js/mycommon.jquery.js"></script>
	<script type="text/javascript" src="../plugins/datepicker/WdatePicker.js"></script>
	<script type="text/javascript" src='../plugins/jquery-x.x.js'> </script>
	<script type="text/javascript" src="../js/jquery.validate.js"></script>
	<script type="text/javascript" src="../plugins/layer/layer.js"></script>	
	<script type="text/javascript" src="../js/TWorder.js"></script>	

</head>

<body>
	<html:form action="/biz/TWorderDpAct?method=list" styleId="TWorderForm">
		<div id="main">
			<div id="tab-top">
				<div id="lift"></div>
				<div id="pt">工单管理</div>
				<div id="right"></div>
			</div>
			
			<div id="table">
				<div id="ptk"> <div id="tabtop-l"> </div>
				<div id="tabtop-z">输入查询条件</div>
				<div id="tabtop-r1"></div></div>
			</div>
		
		
			<div id="main-tab">
				<div id="info-4">           
					<li>状态：<myjsp:select name='status' width='120' id='status' listname='statuslist' first='----' value='${TWorderForm.status}' readonly='' showonly=''/></li>
					<li>等级：<myjsp:select name='level' width='120' id='level' listname='levellist' first='----' value='${TWorderForm.level}' readonly='' showonly=''/></li>
					<li>创建源：<myjsp:select name='createSource' width='120' id='createSource' listname='createSourcelist' first='----' value='${TWorderForm.createSource}' readonly='' showonly=''/></li>
				</div>          
				<div id="info-4">           
					<li>工单号：<html:text property='sn' styleClass='s-input-2'/></li>
					<li>第一位坐席：<html:text property='fagent' styleClass='s-input-2'/></li>
					<li style="width:80px"><html:submit styleClass="search-2" value="查询" /></li>	
					<li style="width:80px"><input class='search-2' type='button' value='导出数据' onClick='return exportList("../biz/TWorderDpAct.do?method=doExport")'></li>			
				</div>          
			</div>
		
			<div id="table">
				<div id="ptk"> 
					<div id="tabtop-l"> </div>
					<div id="tabtop-z">当前查询结果</div>
					<li><input class='search-2' type='button' value='创建工单' onClick='window.location="../biz/TWorderDpAct.do?method=add"'></li>
					<li><input class='search-2' type='button' value='删除选中' onClick='return deleteSelected("../biz/TWorderDpAct.do?method=doDelete")'></li>
				</div>					
			</div>
		
			<div id="fh-flie-2"> </div>
			<div id="main-tablist">
				<table width='100%' border='0' cellpadding='0' cellspacing='0' id='showData'>
					<tr id='minpt-tab'>
						<td>选择</td>
						<td>工单号</td>
						<td>状态</td>
						<td>等级</td>
						<td>客户</td>
						<td>第一位坐席</td>
						<td>受理坐席</td>
						<td>主题</td>
						<td>创建源</td>
						<td>创建时间</td>
						<td>操作</td>
					</tr>

					<c:forEach var="aRecord" items="${result}">
						<tr class='out' onMouseOver='this.className="over"' onMouseOut='this.className="out"'>
							<td class='zw-txt'><html:checkbox property='ids' styleId='${aRecord[0].id}' value='${aRecord[0].id}'/></td>
							<td class='zw-txt'>${aRecord[0].sn}</td>
							<td class='zw-txt'><myjsp:select name='status' width='0' id='status' listname='statuslist' first='----' value='${aRecord[0].status}' readonly='' showonly='true'/></td>
							<td class='zw-txt'><myjsp:select name='level' width='0' id='level' listname='levellist' first='----' value='${aRecord[0].level}' readonly='' showonly='true'/></td>
							<td class='zw-txt'>${aRecord[1].name}<br/>[电话:${aRecord[1].phone}]</td>
							<td class='zw-txt'>${aRecord[0].fagent}</td>
							<td class='zw-txt'>${aRecord[0].toAgent}</td>
							<td class='zw-txt' title="${aRecord[0].subject}"><myjsp:subString length="20" value="${aRecord[0].subject}" omit=".." /></td>
							<td class='zw-txt' title="${aRecord[0].createObject}"><myjsp:select name='createSource' width='0' id='createSource' listname='createSourcelist' first='----' value='${aRecord[0].createSource}' readonly='' showonly='true'/></td>
							<td class='zw-txt'><myjsp:date value='${aRecord[0].createTime}' format='yyyy-MM-dd HH:mm:ss' /></td>
							<td class='zw-txt'><a href='../biz/TWorderDpAct.do?method=modify&id=${aRecord[0].id}'>修改</a>&nbsp;<a href='javascript:showWorderDetail("${aRecord[0].id}")'>详细</a></td>
						</tr>
					</c:forEach>
				</table>
				
				<div id="info-pz"> 
					${link}
				</div>
			</div>
		</div>
	</html:form>
</body>
</html>
