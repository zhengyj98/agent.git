<%@ page contentType="text/html; charset=UTF-8" language="java"  errorPage="" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.cellcom.com.cn/myjsp" prefix="myjsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>坐席绑定</title>
	<link rel="stylesheet" href="../themes/skyblue/skyblueMain.css" type="text/css" title="styles1" />    
	
	
	<!--请在下面增加js-->
   	<script type="text/javascript" src="../js/mycommon.jquery.js"></script>
	<script type="text/javascript" src='../plugins/jquery-x.x.js'> </script>	

</head>

<body>
	<html:form action="/user/TGroupDpAct?method=doBindAgent" styleId="TGroupForm" >
		<html:hidden property="id"/>
			
		<div id="main">
			<div id="tab-top">
				<div id="lift"></div>
				<div id="pt">坐席绑定</div>
				<div id="right"></div>
			</div>
		
			<div id="fh-flie-2"> </div>
			<div id="main-tablist">
				<table align="center" class="table-slyle-hs">
					<tr style='height:50px'>
						<td class='zw-txt' colspan="10">选择</td>
					</tr>
					<myjsp:checkbox name='agents' listname='agentlist' br='yes' before='<tr style="height:40px">' after='</tr>' beforeObject='<td class="zw-txt" width="10px">' afterObject='</td>'/>
							
				</table>
				<div align="center"> 
					<input type="submit" name="aa" value="确认保存" class='search-2'> <input type="button" name="aa" value="返回" onclick='history.back()' class='search-2'>
				</div>
				
			</div>
		</div>
	</html:form>
</body>
</html>
