<%@ page language="java" contentType="text/html;charset=utf-8"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>

		<title>操作结果</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
		<meta http-equiv="description" content="This is my page">		
	    <link rel="stylesheet" href="../themes/skyblue/skyblueMain.css" type="text/css" title="styles1" />    
		<script type="text/javascript" src="../themes/js/jquery.js"></script>
			
	</head>
	
<body>
<div id="main">
  	<div id="tab-top">
  		<div id="ptk1">
	  		<div id="lift"></div>
	  		<div id="pt">您的操作失败</div>
  		    <div id="right"></div>
  		</div>
 	</div>
	<div id="table">
	    <div id="ptk">
	   		<div id="tabtop-l"></div>
	    	<div id="tabtop-z">原因</div>
	    	<div id="tabtop-r1"></div>
      </div>
	</div>
	<div id="main-tab">
			<table width="911" height="200" align="center" class="table-slyle-hs" >
              <tr>
			  <td colspan="2">
				<center>
					<font color="red">
						${result}					</font>
				</center>			  </td>
              </tr>
              <tr>
                <td height="50" align="center">
					<div align="right">
					  <input type="button" class="search-2" value="返回列表" onClick="window.location='../common/Common.do?method=goback&&action=goback&pgUri=${pgUri}&specialUri=${specialUri}'" />
				  </div></td>
                <td align="center"><div align="left">
                  <input name="button" type="button" class="search-2" onClick="history.back()" value="后退"/>
                </div></td>
              </tr>
       	  </table>
	</div>
</div>
</body>	
</html>
