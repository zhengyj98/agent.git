<%@ page contentType="text/html; charset=UTF-8" language="java"  errorPage="" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.cellcom.com.cn/myjsp" prefix="myjsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>客户管理</title>
	<link rel="stylesheet" href="../themes/skyblue/skyblueMain.css" type="text/css" title="styles1" />    

	<!--请在下面增加js-->
   	<script type="text/javascript" src="../js/mycommon.jquery.js"></script>
	<script type="text/javascript" src="../plugins/datepicker/WdatePicker.js"></script>
	<script type="text/javascript" src='../plugins/jquery-x.x.js'> </script>	
	<script type="text/javascript" src="../plugins/layer/layer.js"></script>
	<script>
		function showLogs(cid) {
			layer.open({
				type : 2,
				closeBtn : 1,
				title : "聊天历史",
				content : "../online/agent.jsp?onlyShow=true&cid=" + cid,
				move : false,
				maxmin : true,
				area : [ '80%', '80%' ],
				shadeClose : true,
				cancel: function(){ 
				    //右上角关闭回调
				    layer.closeAll();
				}
			});
		}
	</script>

</head>

<body>
	<html:form action="/crm/TCrmDpAct?method=list" styleId="TCrmForm">
		<div id="main">
			<div id="tab-top">
				<div id="lift"></div>
				<div id="pt">客户管理</div>
				<div id="right"></div>
			</div>
			
			<div id="table">
				<div id="ptk"> <div id="tabtop-l"> </div>
				<div id="tabtop-z">输入查询条件</div>
				<div id="tabtop-r1"></div></div>
			</div>
		
		
			<div id="main-tab">
				<div id="info-4">           
					<li>姓名：<html:text property='name' styleClass='s-input-2' /></li>
					<li>电话：<html:text property='phone' styleClass='s-input-2' /></li>
					<li>TID：<html:text property='tid' styleClass='s-input-2' /></li>
				</div>   
				<div id="info-4">           
					<li>等级：<myjsp:select name='level' width='120' id='level' listname='levellist' first='----' value='${TCrmForm.level}' readonly='' showonly=''/></li>
					<li>状态：<myjsp:select name='status' width='120' id='status' listname='statuslist' first='----' value='${TCrmForm.status}' readonly='' showonly=''/></li>
					<li>类型：<myjsp:select name='type' width='120' id='type' listname='typelist' first='----' value='${TCrmForm.type}' readonly='' showonly=''/></li>
					<li style="width:80px"><html:submit styleClass="search-2" value="查询" /></li>	
					<li style="width:80px"><input class='search-2' type='button' value='导出数据' onClick='return exportList("../crm/TCrmDpAct.do?method=doExport")'></li>			
				</div>       
			</div>
		
			<div id="table">
				<div id="ptk"> 
					<div id="tabtop-l"> </div>
					<div id="tabtop-z">当前查询结果</div>
					<li><input class='search-2' type='button' value='新增客户' onClick='window.location="../crm/TCrmDpAct.do?method=add"'></li>
					<li></li>
					<li></li>					
				</div>					
			</div>
		
			<div id="fh-flie-2"> </div>
			<div id="main-tablist">
				<table width='100%' border='0' cellpadding='0' cellspacing='0' id='showData'>
					<tr id='minpt-tab'>
					<td>姓名</td>
					<td>等级</td>
					<td>状态</td>
					<td>类型</td>
					<td>电话</td>
					<td>邮箱</td>
					<td>QQ</td>
					<td>性别</td>
					<td>企业</td>
					<td>省份</td>
					<td>城市</td>
					<td>备注</td>
					<td>TID</td>
					<td>操作</td>
					</tr>

					<c:forEach var="aRecord" items="${result}">
						<tr class='out' onMouseOver='this.className="over"' onMouseOut='this.className="out"'>
							<td class='zw-txt'>${aRecord.name}</td>
							<td class='zw-txt'><myjsp:select name='level' width='0' id='level' listname='levellist' first='----' value='${aRecord.level}' readonly='' showonly='true'/></td>
							<td class='zw-txt'><myjsp:select name='status' width='0' id='status' listname='statuslist' first='----' value='${aRecord.status}' readonly='' showonly='true'/></td>
							<td class='zw-txt'><myjsp:select name='type' width='0' id='type' listname='typelist' first='----' value='${aRecord.type}' readonly='' showonly='true'/></td>
							<td class='zw-txt'>${aRecord.phone}</td>
							<td class='zw-txt'>${aRecord.email}</td>
							<td class='zw-txt'>${aRecord.qq}</td>
							<td class='zw-txt'><myjsp:select name='sex' width='0' id='sex' listname='sexlist' first='----' value='${aRecord.sex}' readonly='' showonly='true'/></td>
							<td class='zw-txt'>${aRecord.company}</td>
							<td class='zw-txt'>${aRecord.province}</td>
							<td class='zw-txt'>${aRecord.city}</td>
							<td class='zw-txt'>${aRecord.memo}</td>
							<td class='zw-txt'>${aRecord.tid}</td>
							<td class='zw-txt'><a href='../crm/TCrmDpAct.do?method=modify&id=${aRecord.id} '>修改</a>&nbsp<a href='javascript:showLogs("${aRecord.id}")'>聊天历史</a>&nbsp<a href='../crm/TCrmDpAct.do?method=modify&id=${aRecord.id} '>会话列表</a></td>
						</tr>
					</c:forEach>
				</table>
				
				<div id="info-pz"> 
					${link}
				</div>
			</div>
		</div>
	</html:form>
</body>
</html>
